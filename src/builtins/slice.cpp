/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pypp/builtins/slice.tcc>

#include <regex>
#include <limits>

namespace py
{
    inline namespace builtins
    {
        namespace internal__
        {
            slice slice::from_str(std::string properties)
            {
                std::regex regex(
                    "\\s*([+-][0-9]+|[0-9]*)\\s*:\\s*(([+-][0-9]+|[0-9]*)\\s*(|:\\s*([+-][0-9]+|[0-9]*)\\s*))"
                );
                std::smatch matches;

                if (!std::regex_match(properties, matches, regex))
                    raise(ValueError("invalid slice expression: \"" + properties + '"'));

                std::string start_ = matches[1].str();
                std::string stop_ = matches[3].str();
                std::string step_ = matches[5].str();

                if (!step_.empty() && std::stol(step_) < 0)
                {
                    if (start_.empty()) start_ = "-1";
                    if (stop_.empty())
                        stop_ = std::to_string(std::numeric_limits<long>::min());
                }

                if (start_.empty()) start_ = "0";
                if (stop_.empty()) stop_ = "-1";
                if (step_.empty()) step_ = "1";

                if (std::stol(step_) == 0)
                    raise(ValueError("slice step cannot be zero"));

                return {std::stol(start_), std::stol(stop_), std::stol(step_)};
            }
        }
    }
}
