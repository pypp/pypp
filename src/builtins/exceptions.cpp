/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pypp/builtins/exceptions.hpp>

namespace py
{
    inline namespace builtins
    {
        BaseException::BaseException()
            : what_msg()
        {}

        BaseException::BaseException(const char* what)
            : what_msg(what)
        {}

        BaseException::BaseException(const std::string& what)
            : what_msg(what)
        {}

        BaseException::BaseException(const BaseException& other) = default;

        BaseException::BaseException(BaseException&& other) = default;

        BaseException::~BaseException() = default;

        const char* BaseException::what() const noexcept
        {
            return what_msg.c_str();
        }

        BaseException& BaseException::operator = (const BaseException&) = default;

        BaseException& BaseException::operator = (BaseException&&) = default;

        Exception::Exception() = default;

        Exception::Exception(const char* what)
            : BaseException(what)
        {}

        Exception::Exception(const std::string& what)
            : BaseException(what)
        {}

        Exception::~Exception() = default;

        ArithmeticError::ArithmeticError() = default;

        ArithmeticError::ArithmeticError(const char* what)
            : Exception(what)
        {}

        ArithmeticError::ArithmeticError(const std::string& what)
            : Exception(what)
        {}

        ArithmeticError::~ArithmeticError() = default;

        FloatingPointError::FloatingPointError() = default;

        FloatingPointError::FloatingPointError(const char* what)
            : ArithmeticError(what)
        {}

        FloatingPointError::FloatingPointError(const std::string& what)
            : ArithmeticError(what)
        {}

        FloatingPointError::~FloatingPointError() = default;

        OverflowError::OverflowError() = default;

        OverflowError::OverflowError(const char* what)
            : ArithmeticError(what)
        {}

        OverflowError::OverflowError(const std::string& what)
            : ArithmeticError(what)
        {}

        OverflowError::~OverflowError() = default;

        ZeroDivisionError::ZeroDivisionError() = default;

        ZeroDivisionError::ZeroDivisionError(const char* what)
            : ArithmeticError(what)
        {}

        ZeroDivisionError::ZeroDivisionError(const std::string& what)
            : ArithmeticError(what)
        {}

        ZeroDivisionError::~ZeroDivisionError() = default;

        AssertionError::AssertionError() = default;

        AssertionError::AssertionError(const char* what)
            : Exception(what)
        {}

        AssertionError::AssertionError(const std::string& what)
            : Exception(what)
        {}

        AssertionError::~AssertionError() = default;

        AttributeError::AttributeError() = default;

        AttributeError::AttributeError(const char* what)
            : Exception(what)
        {}

        AttributeError::AttributeError(const std::string& what)
            : Exception(what)
        {}

        AttributeError::~AttributeError() = default;

        BufferError::BufferError() = default;

        BufferError::BufferError(const char* what)
            : Exception(what)
        {}

        BufferError::BufferError(const std::string& what)
            : Exception(what)
        {}

        BufferError::~BufferError() = default;

        EOFError::EOFError() = default;

        EOFError::EOFError(const char* what)
            : Exception(what)
        {}

        EOFError::EOFError(const std::string& what)
            : Exception(what)
        {}

        EOFError::~EOFError() = default;

        ImportError::ImportError() = default;

        ImportError::ImportError(const char* what)
            : Exception(what)
        {}

        ImportError::ImportError(const std::string& what)
            : Exception(what)
        {}

        ImportError::~ImportError() = default;

        ModuleNotFoundError::ModuleNotFoundError() = default;

        ModuleNotFoundError::ModuleNotFoundError(const char* what)
            : ImportError(what)
        {}

        ModuleNotFoundError::ModuleNotFoundError(const std::string& what)
            : ImportError(what)
        {}

        ModuleNotFoundError::~ModuleNotFoundError() = default;

        LookupError::LookupError() = default;

        LookupError::LookupError(const char* what)
            : Exception(what)
        {}

        LookupError::LookupError(const std::string& what)
            : Exception(what)
        {}

        LookupError::~LookupError() = default;

        IndexError::IndexError() = default;

        IndexError::IndexError(const char* what)
            : LookupError(what)
        {}

        IndexError::IndexError(const std::string& what)
            : LookupError(what)
        {}

        IndexError::~IndexError() = default;

        KeyError::KeyError() = default;

        KeyError::KeyError(const char* what)
            : LookupError(what)
        {}

        KeyError::KeyError(const std::string& what)
            : LookupError(what)
        {}

        KeyError::~KeyError() = default;

        MemoryError::MemoryError() = default;

        MemoryError::MemoryError(const char* what)
            : Exception(what)
        {}

        MemoryError::MemoryError(const std::string& what)
            : Exception(what)
        {}

        MemoryError::~MemoryError() = default;

        NameError::NameError() = default;

        NameError::NameError(const char* what)
            : Exception(what)
        {}

        NameError::NameError(const std::string& what)
            : Exception(what)
        {}

        NameError::~NameError() = default;

        UnboundLocalError::UnboundLocalError() = default;

        UnboundLocalError::UnboundLocalError(const char* what)
            : NameError(what)
        {}

        UnboundLocalError::UnboundLocalError(const std::string& what)
            : NameError(what)
        {}

        UnboundLocalError::~UnboundLocalError() = default;

        OSError::OSError() = default;

        OSError::OSError(const char* what)
            : Exception(what)
        {}

        OSError::OSError(const std::string& what)
            : Exception(what)
        {}

        OSError::~OSError() = default;

        BlockingIOError::BlockingIOError() = default;

        BlockingIOError::BlockingIOError(const char* what)
            : OSError(what)
        {}

        BlockingIOError::BlockingIOError(const std::string& what)
            : OSError(what)
        {}

        BlockingIOError::~BlockingIOError() = default;

        ChildProcessError::ChildProcessError() = default;

        ChildProcessError::ChildProcessError(const char* what)
            : OSError(what)
        {}

        ChildProcessError::ChildProcessError(const std::string& what)
            : OSError(what)
        {}

        ChildProcessError::~ChildProcessError() = default;

        ConnectionError::ConnectionError() = default;

        ConnectionError::ConnectionError(const char* what)
            : OSError(what)
        {}

        ConnectionError::ConnectionError(const std::string& what)
            : OSError(what)
        {}

        ConnectionError::~ConnectionError() = default;

        BrokenPipeError::BrokenPipeError() = default;

        BrokenPipeError::BrokenPipeError(const char* what)
            : ConnectionError(what)
        {}

        BrokenPipeError::BrokenPipeError(const std::string& what)
            : ConnectionError(what)
        {}

        BrokenPipeError::~BrokenPipeError() = default;

        ConnectionAbortedError::ConnectionAbortedError() = default;

        ConnectionAbortedError::ConnectionAbortedError(const char* what)
            : ConnectionError(what)
        {}

        ConnectionAbortedError::ConnectionAbortedError(const std::string& what)
            : ConnectionError(what)
        {}

        ConnectionAbortedError::~ConnectionAbortedError() = default;

        ConnectionRefusedError::ConnectionRefusedError() = default;

        ConnectionRefusedError::ConnectionRefusedError(const char* what)
            : ConnectionError(what)
        {}

        ConnectionRefusedError::ConnectionRefusedError(const std::string& what)
            : ConnectionError(what)
        {}

        ConnectionRefusedError::~ConnectionRefusedError() = default;

        ConnectionResetError::ConnectionResetError() = default;

        ConnectionResetError::ConnectionResetError(const char* what)
            : ConnectionError(what)
        {}

        ConnectionResetError::ConnectionResetError(const std::string& what)
            : ConnectionError(what)
        {}

        ConnectionResetError::~ConnectionResetError() = default;

        FileExistsError::FileExistsError() = default;

        FileExistsError::FileExistsError(const char* what)
            : OSError(what)
        {}

        FileExistsError::FileExistsError(const std::string& what)
            : OSError(what)
        {}

        FileExistsError::~FileExistsError() = default;

        FileNotFoundError::FileNotFoundError() = default;

        FileNotFoundError::FileNotFoundError(const char* what)
            : OSError(what)
        {}

        FileNotFoundError::FileNotFoundError(const std::string& what)
            : OSError(what)
        {}

        FileNotFoundError::~FileNotFoundError() = default;

        InterruptedError::InterruptedError() = default;

        InterruptedError::InterruptedError(const char* what)
            : OSError(what)
        {}

        InterruptedError::InterruptedError(const std::string& what)
            : OSError(what)
        {}

        InterruptedError::~InterruptedError() = default;

        IsADirectoryError::IsADirectoryError() = default;

        IsADirectoryError::IsADirectoryError(const char* what)
            : OSError(what)
        {}

        IsADirectoryError::IsADirectoryError(const std::string& what)
            : OSError(what)
        {}

        IsADirectoryError::~IsADirectoryError() = default;

        NotADirectoryError::NotADirectoryError() = default;

        NotADirectoryError::NotADirectoryError(const char* what)
            : OSError(what)
        {}

        NotADirectoryError::NotADirectoryError(const std::string& what)
            : OSError(what)
        {}

        NotADirectoryError::~NotADirectoryError() = default;

        PermissionError::PermissionError() = default;

        PermissionError::PermissionError(const char* what)
            : OSError(what)
        {}

        PermissionError::PermissionError(const std::string& what)
            : OSError(what)
        {}

        PermissionError::~PermissionError() = default;

        ProcessLookupError::ProcessLookupError() = default;

        ProcessLookupError::ProcessLookupError(const char* what)
            : OSError(what)
        {}

        ProcessLookupError::ProcessLookupError(const std::string& what)
            : OSError(what)
        {}

        ProcessLookupError::~ProcessLookupError() = default;

        TimeoutError::TimeoutError() = default;

        TimeoutError::TimeoutError(const char* what)
            : OSError(what)
        {}

        TimeoutError::TimeoutError(const std::string& what)
            : OSError(what)
        {}

        TimeoutError::~TimeoutError() = default;

        ReferenceError::ReferenceError() = default;

        ReferenceError::ReferenceError(const char* what)
            : Exception(what)
        {}

        ReferenceError::ReferenceError(const std::string& what)
            : Exception(what)
        {}

        ReferenceError::~ReferenceError() = default;

        RuntimeError::RuntimeError() = default;

        RuntimeError::RuntimeError(const char* what)
            : Exception(what)
        {}

        RuntimeError::RuntimeError(const std::string& what)
            : Exception(what)
        {}

        RuntimeError::~RuntimeError() = default;

        NotImplementedError::NotImplementedError() = default;

        NotImplementedError::NotImplementedError(const char* what)
            : RuntimeError(what)
        {}

        NotImplementedError::NotImplementedError(const std::string& what)
            : RuntimeError(what)
        {}

        NotImplementedError::~NotImplementedError() = default;

        RecursionError::RecursionError() = default;

        RecursionError::RecursionError(const char* what)
            : RuntimeError(what)
        {}

        RecursionError::RecursionError(const std::string& what)
            : RuntimeError(what)
        {}

        RecursionError::~RecursionError() = default;

        StopIteration::StopIteration() = default;

        StopIteration::StopIteration(const char* what)
            : Exception(what)
        {}

        StopIteration::StopIteration(const std::string& what)
            : Exception(what)
        {}

        StopIteration::~StopIteration() = default;

        StopAsyncIteration::StopAsyncIteration() = default;

        StopAsyncIteration::StopAsyncIteration(const char* what)
            : Exception(what)
        {}

        StopAsyncIteration::StopAsyncIteration(const std::string& what)
            : Exception(what)
        {}

        StopAsyncIteration::~StopAsyncIteration() = default;

        SyntaxError::SyntaxError() = default;

        SyntaxError::SyntaxError(const char* what)
            : Exception(what)
        {}

        SyntaxError::SyntaxError(const std::string& what)
            : Exception(what)
        {}

        SyntaxError::~SyntaxError() = default;

        IndentationError::IndentationError() = default;

        IndentationError::IndentationError(const char* what)
            : SyntaxError(what)
        {}

        IndentationError::IndentationError(const std::string& what)
            : SyntaxError(what)
        {}

        IndentationError::~IndentationError() = default;

        TabError::TabError() = default;

        TabError::TabError(const char* what)
            : IndentationError(what)
        {}

        TabError::TabError(const std::string& what)
            : IndentationError(what)
        {}

        TabError::~TabError() = default;

        SystemError::SystemError() = default;

        SystemError::SystemError(const char* what)
            : Exception(what)
        {}

        SystemError::SystemError(const std::string& what)
            : Exception(what)
        {}

        SystemError::~SystemError() = default;

        TypeError::TypeError() = default;

        TypeError::TypeError(const char* what)
            : Exception(what)
        {}

        TypeError::TypeError(const std::string& what)
            : Exception(what)
        {}

        TypeError::~TypeError() = default;

        ValueError::ValueError() = default;

        ValueError::ValueError(const char* what)
            : Exception(what)
        {}

        ValueError::ValueError(const std::string& what)
            : Exception(what)
        {}

        ValueError::~ValueError() = default;

        UnicodeError::UnicodeError() = default;

        UnicodeError::UnicodeError(const char* what)
            : ValueError(what)
        {}

        UnicodeError::UnicodeError(const std::string& what)
            : ValueError(what)
        {}

        UnicodeError::~UnicodeError() = default;

        UnicodeDecodeError::UnicodeDecodeError() = default;

        UnicodeDecodeError::UnicodeDecodeError(const char* what)
            : UnicodeError(what)
        {}

        UnicodeDecodeError::UnicodeDecodeError(const std::string& what)
            : UnicodeError(what)
        {}

        UnicodeDecodeError::~UnicodeDecodeError() = default;

        UnicodeEncodeError::UnicodeEncodeError() = default;

        UnicodeEncodeError::UnicodeEncodeError(const char* what)
            : UnicodeError(what)
        {}

        UnicodeEncodeError::UnicodeEncodeError(const std::string& what)
            : UnicodeError(what)
        {}

        UnicodeEncodeError::~UnicodeEncodeError() = default;

        UnicodeTranslateError::UnicodeTranslateError() = default;

        UnicodeTranslateError::UnicodeTranslateError(const char* what)
            : UnicodeError(what)
        {}

        UnicodeTranslateError::UnicodeTranslateError(const std::string& what)
            : UnicodeError(what)
        {}

        UnicodeTranslateError::~UnicodeTranslateError() = default;

        Warning::Warning() = default;

        Warning::Warning(const char* what)
            : Exception(what)
        {}

        Warning::Warning(const std::string& what)
            : Exception(what)
        {}

        Warning::~Warning() = default;

        BytesWarning::BytesWarning() = default;

        BytesWarning::BytesWarning(const char* what)
            : Warning(what)
        {}

        BytesWarning::BytesWarning(const std::string& what)
            : Warning(what)
        {}

        BytesWarning::~BytesWarning() = default;

        DeprecationWarning::DeprecationWarning() = default;

        DeprecationWarning::DeprecationWarning(const char* what)
            : Warning(what)
        {}

        DeprecationWarning::DeprecationWarning(const std::string& what)
            : Warning(what)
        {}

        DeprecationWarning::~DeprecationWarning() = default;

        FutureWarning::FutureWarning() = default;

        FutureWarning::FutureWarning(const char* what)
            : Warning(what)
        {}

        FutureWarning::FutureWarning(const std::string& what)
            : Warning(what)
        {}

        FutureWarning::~FutureWarning() = default;

        ImportWarning::ImportWarning() = default;

        ImportWarning::ImportWarning(const char* what)
            : Warning(what)
        {}

        ImportWarning::ImportWarning(const std::string& what)
            : Warning(what)
        {}

        ImportWarning::~ImportWarning() = default;

        PendingDeprecationWarning::PendingDeprecationWarning() = default;

        PendingDeprecationWarning::PendingDeprecationWarning(const char* what)
            : Warning(what)
        {}

        PendingDeprecationWarning::PendingDeprecationWarning(const std::string& what)
            : Warning(what)
        {}

        PendingDeprecationWarning::~PendingDeprecationWarning() = default;

        ResourceWarning::ResourceWarning() = default;

        ResourceWarning::ResourceWarning(const char* what)
            : Warning(what)
        {}

        ResourceWarning::ResourceWarning(const std::string& what)
            : Warning(what)
        {}

        ResourceWarning::~ResourceWarning() = default;

        RuntimeWarning::RuntimeWarning() = default;

        RuntimeWarning::RuntimeWarning(const char* what)
            : Warning(what)
        {}

        RuntimeWarning::RuntimeWarning(const std::string& what)
            : Warning(what)
        {}

        RuntimeWarning::~RuntimeWarning() = default;

        SyntaxWarning::SyntaxWarning() = default;

        SyntaxWarning::SyntaxWarning(const char* what)
            : Warning(what)
        {}

        SyntaxWarning::SyntaxWarning(const std::string& what)
            : Warning(what)
        {}

        SyntaxWarning::~SyntaxWarning() = default;

        UnicodeWarning::UnicodeWarning() = default;

        UnicodeWarning::UnicodeWarning(const char* what)
            : Warning(what)
        {}

        UnicodeWarning::UnicodeWarning(const std::string& what)
            : Warning(what)
        {}

        UnicodeWarning::~UnicodeWarning() = default;

        UserWarning::UserWarning() = default;

        UserWarning::UserWarning(const char* what)
            : Warning(what)
        {}

        UserWarning::UserWarning(const std::string& what)
            : Warning(what)
        {}

        UserWarning::~UserWarning() = default;

        GeneratorExit::GeneratorExit() = default;

        GeneratorExit::GeneratorExit(const char* what)
            : BaseException(what)
        {}

        GeneratorExit::GeneratorExit(const std::string& what)
            : BaseException(what)
        {}

        GeneratorExit::~GeneratorExit() = default;

        KeyboardInterrupt::KeyboardInterrupt() = default;

        KeyboardInterrupt::KeyboardInterrupt(const char* what)
            : BaseException(what)
        {}

        KeyboardInterrupt::KeyboardInterrupt(const std::string& what)
            : BaseException(what)
        {}

        KeyboardInterrupt::~KeyboardInterrupt() = default;

        SystemExit::SystemExit() = default;

        SystemExit::SystemExit(const char* what)
            : BaseException(what)
        {}

        SystemExit::SystemExit(const std::string& what)
            : BaseException(what)
        {}

        SystemExit::~SystemExit() = default;
    }
}
