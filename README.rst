PyPP: Pythonic programming in C++
=================================

.. introduction

.. image:: https://www.travis-ci.com/pypp/pypp.svg?branch=master
    :target: https://www.travis-ci.com/pypp/pypp
    :alt: Travis CI build status

.. image:: https://coveralls.io/repos/gitlab/pypp/pypp/badge.svg?branch=master
    :target: https://coveralls.io/gitlab/pypp/pypp?branch=master
    :alt: Coveralls coverage

.. image:: https://codecov.io/gl/pypp/pypp/branch/master/graph/badge.svg?token=RAAMIHLFWQ
    :target: https://codecov.io/gl/pypp/pypp
    :alt: Codecov coverage

.. image:: https://readthedocs.org/projects/pypp/badge/?version=latest
    :target: https://pypp.readthedocs.io/en/latest/
    :alt: Read the Docs Documentation build Status

.. image:: https://img.shields.io/endpoint?url=https%3A%2F%2Fpypp.readthedocs.io%2Fen%2Flatest%2F_static%2Fdocscov.json
    :target: https://pypp.readthedocs.io/en/latest/
    :alt: Documentation Coverage

PyPP is a C++ library that allows you to write **pythonic** code in C++.

PyPP is a C++ library that attempts to port the Python standard library to C++,
enabling you to use Python data structures in C++.


Getting Started
---------------

"Hello World" with PyPP is as simple as the following:

.. code-block:: cpp

    // hello-world.cpp

    #include <pypp/builtins.hpp>

    using namespace py;

    int main()
    {
        print("Hello World!");
    }

Compile it with::

    g++ hello-world.cpp -o hello-world -lpypp


License
-------

.. license-note-start

PyPP is free software: you can redistribute it and/or modify it under the terms
of the GNU Lesser General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

PyPP is distributed in the hope that it will be useful, but **WITHOUT ANY**
**WARRANTY**; without even the implied warranty of **MERCHANTABILITY** or
**FITNESS FOR A PARTICULAR PURPOSE**.  See the GNU Lesser General Public License
for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program.  If not, see `<https://www.gnu.org/licenses/>`_.
