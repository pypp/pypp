#include <pypp/builtins.hpp>

// Class convertable to number
class convertable
{
private:
    int x;

public:
    explicit convertable(int x) : x(x) {}

    int get()
    {
        return x;
    }

	convertable operator * (int y) const
	{
		return convertable(x * y);
	}

	bool operator < (int y) const
	{
		return x < y;
	}

    bool operator > (int y) const
	{
		return x > y;
	}

    operator int () const
	{
		return x;
	}
};

// Class with specialized abs
class custom_abs
{};

namespace py
{
    inline namespace builtins
    {
        int abs(const custom_abs&)
        {
            return 59;
        }
    }
}

int main()
{

    // Should work with scalar numbers
    py::assert(py::abs(-42) == 42);
    py::assert(py::abs(56) == 56);

    // Should be able to work with classes supporting operator '<', '>' and '*'
    py::assert(py::abs(convertable(-453)).get() == 453);
    py::assert(py::abs(convertable(45)).get() == 45);

    // Should be able to handle types with specialized abs
    py::assert(py::abs(custom_abs()) == 59);
}
