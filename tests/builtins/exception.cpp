#include <pypp/builtins.hpp>
#include <iostream>

template <class type>
void test_exception()
{

    // Exception test (default construction) (+ raise test (rvalue))
    try
    {
        py::raise(type());
    }
    catch (type& exception)
    {
        py::assert(std::string(exception.what()) == "");
    }

    // Exception test (construction with ``const char*``) (+ raise test (lvalue))
    try
    {
        type exception("exception");
        py::raise(exception);
    }
    catch (type& exception)
    {
        py::assert(std::string(exception.what()) == "exception");
    }

    // Exception test (construction with ``std::string``) (+ raise test (lvalue))
    try
    {
        const type exception(std::string("exception"));
        py::raise(exception);
    }
    catch (type& exception)
    {
        py::assert(std::string(exception.what()) == "exception");
    }

    // Exception test (copy construction)
    try
    {
        type exception(std::string("exception"));
        py::raise(type(exception));
    }
    catch (type& exception)
    {
        py::assert(std::string(exception.what()) == "exception");
    }

    // Exception test (construction with variadic arguments)
    try
    {
        type exception("exception", std::to_string(4));
        py::raise(exception);
    }
    catch (type& exception)
    {
        py::assert(std::string(exception.what()) == "(exception, 4)");
    }
}

int main()
{

    // BaseException test
    try
    {
        py::raise(py::BaseException("exception"));
    }
    catch (py::BaseException& exception)
    {
        py::assert(std::string(exception.what()) == "exception");
    }

    // Exception test
    try
    {
        py::raise(py::Exception("exception"));
    }
    catch (py::Exception& exception)
    {
        py::assert(std::string(exception.what()) == "exception");
    }

    // Polymorphism test
    try
    {
        py::raise(py::Exception("exception"));
    }
    catch (py::BaseException& exception)
    {
        py::assert(std::string(exception.what()) == "exception");

        // If dynamic_cast fails, exception is thrown, resulting test failure
        py::assert(std::string(dynamic_cast<py::Exception&>(exception).what()) == "exception");
    }

    // All exceptions test
    test_exception<py::BaseException>();
    test_exception<py::SystemExit>();
    test_exception<py::KeyboardInterrupt>();
    test_exception<py::GeneratorExit>();
    test_exception<py::Exception>();
    test_exception<py::StopIteration>();
    test_exception<py::StopAsyncIteration>();
    test_exception<py::ArithmeticError>();
    test_exception<py::FloatingPointError>();
    test_exception<py::OverflowError>();
    test_exception<py::ZeroDivisionError>();
    test_exception<py::AssertionError>();
    test_exception<py::AttributeError>();
    test_exception<py::BufferError>();
    test_exception<py::EOFError>();
    test_exception<py::ImportError>();
    test_exception<py::ModuleNotFoundError>();
    test_exception<py::LookupError>();
    test_exception<py::IndexError>();
    test_exception<py::KeyError>();
    test_exception<py::MemoryError>();
    test_exception<py::NameError>();
    test_exception<py::UnboundLocalError>();
    test_exception<py::OSError>();
    test_exception<py::BlockingIOError>();
    test_exception<py::ChildProcessError>();
    test_exception<py::ConnectionError>();
    test_exception<py::BrokenPipeError>();
    test_exception<py::ConnectionAbortedError>();
    test_exception<py::ConnectionRefusedError>();
    test_exception<py::ConnectionResetError>();
    test_exception<py::FileExistsError>();
    test_exception<py::FileNotFoundError>();
    test_exception<py::InterruptedError>();
    test_exception<py::IsADirectoryError>();
    test_exception<py::NotADirectoryError>();
    test_exception<py::PermissionError>();
    test_exception<py::ProcessLookupError>();
    test_exception<py::TimeoutError>();
    test_exception<py::ReferenceError>();
    test_exception<py::RuntimeError>();
    test_exception<py::NotImplementedError>();
    test_exception<py::RecursionError>();
    test_exception<py::SyntaxError>();
    test_exception<py::IndentationError>();
    test_exception<py::TabError>();
    test_exception<py::SystemError>();
    test_exception<py::TypeError>();
    test_exception<py::ValueError>();
    test_exception<py::UnicodeError>();
    test_exception<py::UnicodeDecodeError>();
    test_exception<py::UnicodeEncodeError>();
    test_exception<py::UnicodeTranslateError>();
    test_exception<py::Warning>();
    test_exception<py::DeprecationWarning>();
    test_exception<py::PendingDeprecationWarning>();
    test_exception<py::RuntimeWarning>();
    test_exception<py::SyntaxWarning>();
    test_exception<py::UserWarning>();
    test_exception<py::FutureWarning>();
    test_exception<py::ImportWarning>();
    test_exception<py::UnicodeWarning>();
    test_exception<py::BytesWarning>();
    test_exception<py::ResourceWarning>();
}
