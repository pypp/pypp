#include <pypp/builtins.hpp>
#include <utility>

template class py::range<int>;

int main()
{

    // Range should constructable from two arguments of same type
    py::range<int> r(0, 10);
    py::assert(r.size() == 10);
    {
        int i = 0, j = 10;
        py::range<int> a(i, j);
        py::assert(a.size() == 10);

        py::range<int> b(std::move(i), std::move(j));
        py::assert(b.size() == 10);
    }

    // Range should allow accessing start, stop and step
    py::assert(r == py::range<int>(r.stop()));
    py::assert(r == py::range<int>(r.start(), r.stop(), r.step()));

    // Range should be convertable to list
    py::assert(py::list<int>(r) == py::list<int>({0, 1, 2, 3, 4, 5, 6, 7, 8, 9}));

    // Range should be comparable
    py::assert(py::range<int>(10) == r);
    py::assert(!(py::range<int>(0, 10, 2) != py::range<int>(0, 9, 2)));
    py::assert(py::range<int>(10) < py::range<int>(11));
    py::assert(py::range<int>(11) < py::range<int>(1, 11));
    py::assert(!(py::range<int>(10, 11) < py::range<int>(11)));
    py::assert(py::range<int>(12) > py::range<int>(11));
    py::assert(py::range<int>(10, 15) > py::range<int>(20));
    py::assert(!(py::range<int>(15) > py::range<int>(10, 20)));
    py::assert(py::range<int>(9) <= py::range<int>(18) && py::range<int>(10) >= py::range<int>(10));

    // Negative indices should work
    py::assert(r[-1] == 9);

    // Range should be iterable
    {
        py::list<int> a;
        a.insert(0, r.begin(), r.end());
        py::assert(a == py::list<int>(r));
    }

    // Range should be reverse iterable
    {
        py::list<int> a;
        a.insert(0, r.rbegin(), r.rend());
        a.reverse();
        py::assert(a == py::list<int>(r));
    }

    // Range should have working front and back functions
    py::assert(r.front() == r[0]);
    py::assert(r.back() == r[-1]);

    // Trying to access out of range should raise exception
    {
        bool thrown = false;
        try
        {
            r[r.size()];
        }
        catch (py::IndexError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // Range should be sliceable
    py::assert(r["8::-1"] == py::range<int>(8, -1, -1));

    // Range should be assignable
    py::range<int> range(0);
    range = r;
    py::assert(range == r);
    range = std::move(r);
    py::assert(range == r);

    // count() return the count of a value in a range
    py::assert(range.count(8) == 1);
    py::assert(range.count(range.stop()) == 0);
    py::assert(py::range<int>(0, 8, 2).count(5) == 0);

    // index() return the index of a value in a range
    {
        py::assert(range.index(9) == 9);
        try
        {
            range.index(range.stop());
            py::assert(false);
        }
        catch (py::ValueError&)
        {}
    }

    // find() should return iterator to a value in a range
    py::assert(range[range.index(9)] == *range.find(9));
    py::assert(range.find(range.stop()) == range.end());

    // Range should support specialized operator ->*
    py::assert(9 ->* py::in(range));

    // std:swap should swap two ranges
    {
        py::range<int> nums1(1, 10);
        py::range<int> nums2(5);

        std::swap(nums1, nums2);
        py::assert(nums1.stop() == 5);
    }
}
