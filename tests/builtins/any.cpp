#include <pypp/builtins.hpp>
#include <vector>
#include <random>

int main()
{
    std::random_device random;
    std::default_random_engine engine(random());
    std::uniform_int_distribution<int> generator(1, 32768);
    int size = generator(engine);
    std::vector<bool> array(size, false);

    // If all value of the array is false, the returned value should be false too
    py::assert(py::any(array) == false);

    // If any value of the array is true, it should return ``true``
    array[generator(engine) % size] = true;
    py::assert(py::any(array) == true);
}
