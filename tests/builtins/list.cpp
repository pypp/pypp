#include <pypp/builtins.hpp>
#include <type_traits>
#include <functional>

template class py::list<int>;
template class py::builtins::internal__::iterator<py::list<int>, int>;

template <class... args>
constexpr void nothing(args...) {}

int main()
{

    // List should be default constructable
    {
        py::list<bool> boolean;
    }

    // List should be constructable with an initializer list
    py::list<int> a = {0, 1, 2, 3, 4};

    // List should be copy constructable
    py::list<int> b = a;

    // Copy constructed list should equal to source list
    py::assert(a == b);
    py::assert(!(a != b));

    // List should assignable
    b = {0, 1, 2, 4, 5};

    // List should be move constructable
    {
        py::list<int> list = {0, 1};
        py::assert(py::list(std::move(list)) == py::list({0, 1}));
    }

    // List should be lexicographically comparable
    py::assert(a < b);
    py::assert(a <= b);
    py::assert(b > a);
    py::assert(b >= a);

    // List should be iterable
    for (auto& element : a)
    {
        nothing(element);
    }

    // List should be accessable with indices
    for (std::size_t i = 0; i < b.size(); i++)
    {
        nothing(b[i]);
    }

    // List should be reverse iterable
    for (auto it = b.rbegin(); it != b.rend(); it++)
    {
        nothing(*it);
        nothing(it.operator->());
    }

    // List should be reverse iterable
    for (
        auto it = const_cast<const py::list<int>&>(b).rbegin();
        it != const_cast<const py::list<int>&>(b).rend();
        it++
    )
    {
        nothing(it);
    }

    // Pointer-arithmetics should be possible with list iterators
    py::assert(a.end() - a.begin() == static_cast<long>(a.size()));
    py::assert(b.begin() + 1 == 1 + b.begin());
    py::assert(!(b.begin() + 1 != 1 + b.begin()));
    py::assert(*(a.begin()++) == 0);
    py::assert(*(++a.begin()) == 1);
    py::assert(*(--a.end()) == 4);
    py::assert(a.end()-- == a.end());
    py::assert(*(a.end() - a.size()) == 0);
    py::assert(a.begin() + a.size() == a.end());
    py::assert(a.begin() < a.end() && !(a.begin() > a.end()));
    py::assert(a.begin() <= a.end() && !(a.begin() >= a.end()));
    py::assert(*((a.begin() += 1) -= 1) == 0);

    // Elements should be removable from lists
    b.pop();
    py::assert(b == py::list({0, 1, 2, 4}));

    // List should accept new elements
    b.append(5);
    b.insert(3, 3);
    {
        const int i = 0;
        b.insert(0, i);
    }
    py::assert(b == py::list({0, 0, 1, 2, 3, 4, 5}));

    // List should accept new element from iterator range
    {
        py::list<int> c = {0, 1};
        c.insert(c.size(), c.begin(), c.end());
        py::assert(c == py::list({0, 1, 0, 1}));
    }

    // remove should should remove first element with given value
    b.insert(0, 0);
    b.remove(0);
    py::assert(b == py::list({0, 0, 1, 2, 3, 4, 5}));

    // Trying to remove non-existant value should raise exception
    {
        bool thrown = false;
        try
        {
            b.remove(6);
        }
        catch (py::ValueError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // Trying to access out of range should raise exception
    {
        bool thrown = false;
        try
        {
            b[b.size()];
        }
        catch (py::IndexError&)
        {
            thrown = true;
        }
        py::assert(thrown);

        thrown = false;
        try
        {
            const_cast<const py::list<int>&>(b)[b.size()];
        }
        catch (py::IndexError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // pop from empty list should raise exception
    {
        bool thrown = false;
        try
        {
            py::list<int>().pop();
        }
        catch (py::IndexError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // sort should sort the list
    {
        py::list<int> l = {0, 2, 4, 1, 5, 3};
        l.sort();
        py::assert(l == py::list({0, 1, 2, 3, 4, 5}));
        l.sort(true);
        py::assert(l == py::list({5, 4, 3, 2, 1, 0}));
    }

    // count should return how much elements stored with given value
    py::assert(py::list({0, 1, 0}).count(0) == 2);

    // index should return the index of given element / value
    {
        py::list<int> nums = {0, 2, 1, 4, 0, 3, 2};
        py::assert(nums.index(0) == 0); // Argument is temporary reference
        py::assert(nums.index(nums[-1]) == 6); // Argument is reference to element
    }

    // List should be sliceable
    py::list<int>&& slice = b["1:5"];
    py::assert(slice == py::list({0, 1, 2, 3}));

    // Modifing slice should also modify original one
    slice = {11, 22, 33, 44, 55};
    py::assert(b == py::list({0, 11, 22, 33, 44, 55, 4, 5}));

    // Inequal slice assignment should resize original
    slice = {11, 22, 44};
    py::assert(b == py::list({0, 11, 22, 44, 55, 4, 5}));

    // Attempt to assigning to inequal slice with step != 1 should raise exception
    {
        bool thrown = false;
        try
        {
            b["::-2"] = {0, 4, 5, 11, 4, 3, 5, 3, 4, 2};
        }
        catch (py::ValueError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // Slice of slice should affect the original too
    slice["1:2"] = {222};
    py::assert(b == py::list({0, 11, 222, 44, 55, 4, 5}));

    // Slice should be at same range even after inequal assignments
    py::assert(slice == py::list({11, 222, 44, 55}));

    // Constant list should return constant slices
    static_assert(
        std::is_const<
            std::remove_reference_t<
                decltype(const_cast<const py::list<int>&>(b)["2:3"])
            >
        >::value
    );
    py::assert(const_cast<const py::list<int>&>(b)["2:3"].size() == 1);

    // Setting step to -1 should reverse the list
    {
        py::list c = b;
        c.reverse();
        py::assert(b["::-1"] == c);
    }

    // Giving invalid slice expression will cause exception
    {
        bool thrown = false;
        try
        {
            b[""];
        }
        catch (py::ValueError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // Slice step can't be zero
    {
        bool thrown = false;
        try
        {
            b["::0"];
        }
        catch (py::ValueError&)
        {
            thrown = true;
        }
        py::assert(thrown);

        thrown = false;
        try
        {
            const_cast<const py::list<int>&>(b)["::0"];
        }
        catch (py::ValueError&)
        {
            thrown = true;
        }
        py::assert(thrown);
    }

    // Slice with invalid properties should have the size of 0
    py::assert(b["0:1:-1"].size() == 0);
}
