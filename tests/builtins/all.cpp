#include <pypp/builtins.hpp>
#include <vector>
#include <random>

int main()
{
    std::random_device random;
    std::default_random_engine engine(random());
    std::uniform_int_distribution<int> generator(1, 32768);
    int size = generator(engine);
    std::vector<bool> array(size, true);

    // If all value of the array is true, the returned value should be true too
    py::assert(py::all(array) == true);

    // If any value of the array is false, it should return ``false``
    array[generator(engine) % size] = false;
    py::assert(py::all(array) == false);
}
