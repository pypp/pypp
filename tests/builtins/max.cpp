#include <pypp/builtins.hpp>
#include <vector>
#include <iostream>

int main()
{
    std::vector<int> values = {0, 4, 2, 9, 1, 7};

    // ``max`` should return a maximum value from a iterable
    py::assert(py::max(values) == 9);

    // ``max`` should return a maximum value from given arguments
    py::assert(py::max(4, 6, 1, 0, 2, 8) == 8);
}
