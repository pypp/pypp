#include <pypp/builtins.hpp>
#include <vector>

int main()
{

    // ``in`` should work with initializer lists
    py::assert(2 ->* py::in<std::initializer_list<int>>({5, 6, 2, 7}));
    py::assert(!(4 ->* py::in<std::initializer_list<int>>({5, 6, 2, 7})));

    // ``in`` should work with STL containers
    py::assert(8 ->* py::in(std::vector{6, 5, 3, 8}));
    py::assert(!(88 ->* py::in(std::vector{43, 34, 22, 76})));
}
