#include <pypp/builtins.hpp>
#include <vector>

int main()
{
    std::vector<int> values = {0, 4, 2, 9, 1, 7};

    // ``min`` should return a minimum value from a iterable
    py::assert(py::min(values) == 0);

    // ``min`` should return a minimum value from given arguments
    py::assert(py::min(4, 6, 1, 0, 2, 8) == 0);
}
