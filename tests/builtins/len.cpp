#include <pypp/builtins.hpp>

int main()
{
    py::assert(py::len(py::list<int>({4, 5, 3, 2, 8, 9, 0})) == 7);
}
