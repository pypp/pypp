Contributing Guide
==================

First of all, thanks for being interested to contribute to the project. But
before contributing, please see the `code of conduct
<https://pypp.readthedocs.io/en/latest/contributing/code-of-conduct.html>`_
to know the desired behavior of contributors. By doing public interactions (e.g
commenting on issues and merge requests), you agree to the code of conduct of
PyPP.


Contributing
------------

You can contribute to the project in the following ways:


Spreading the name
++++++++++++++++++

Yes, spreading the name helps! If you suggest the project to others, they will
use it. One day, some of them will become active contributors to the project.


Suggesting ideas
++++++++++++++++

If you think PyPP is missing something or have an idea about it, please share it
by opening a new issue at the
`tracker <https://gitlab.com/pypp/pypp/issues/new>`_.


Submitting issues
+++++++++++++++++

If you ever found issue or bug in the library please report it to the `issue
tracker <https://gitlab.com/pypp/pypp/issues>`_. If you found any mistake, typo
or missing things (like example) in the documentations, please report it too.


Writing documentations
++++++++++++++++++++++

Currently, PyPP lacks of good documentations and tutorials. We programmers often
write codes but forget to write the good documentations. This causes users to
become helpless when they don't the reference of a function or class.

See ":ref:`setting-up`" section for instructions on setting up the development
environment.

.. // TODO: Add instructions on source style


Being one of the developers
+++++++++++++++++++++++++++

PyPP is still under it's early development phase and most features aren't
implemented yet. As a developer, you can support PyPP by doing the following
things:

*   Fixing minor bugs
*   Fixing major issues
*   Adding new features
*   Fixing implementation errors

See ":ref:`setting-up`" section for instructions on setting up the development
environment before you write codes.

.. // TODO: Add instructions on code style


.. _setting-up:

Setting up a development environment
------------------------------------

To set up a development environment, please follow the following instructions.
Some of these instructions are specific for Debian-based systems, change them
accordingly for your system.


Installing required softwares
+++++++++++++++++++++++++++++

Before installing anything, update repository information and upgrade the system
by::

    sudo apt update && sudo apt upgrade

PyPP uses Git as the version control system (aka VCS). So you need to install
Git. Install it with::

    sudo apt install git

PyPP is written in C++, so you need to install a C++ complier. Supported C++
compliers are ``g++`` from GCC and ``clang++`` from Clang, and with ``g++``
being the priority. Install them by::

    sudo apt install gcc g++ clang

PyPP uses the autotools toolchain for the building process. Install it by::

    sudo apt install autoconf automake libtool

PyPP uses the following softwares in order to generate the documentations:

*   Sphinx: Building the documentations
*   Doxygen: Parsing C++ files to extract documentations
*   DoxyCast: Generating RST files contains documentations from Doxygen XML
    output
*   MyST Parser: Parsing a special Markdown variant made for writing
    documentations
*   DocsCov: Creating dynamic badge show documentation coverage
*   Sphinx Book Theme: Theme for documentations
*   ``sphinx-notfound-page``: Forcing Sphinx to use absolute addresses in 404
    page, allowing the 404 page to not break

Install the above softwares by::

    sudo apt install doxygen
    sudo pip install sphinx doxycast myst_parser docscov sphinx_book_theme \
                     sphinx-notfound-page

After you have installed the above softwares, you are done. But at the time of
writing this guide, there was a bug in Pygments (used by Sphinx for highlight
code). This causes Sphinx to fail to highlight C++ code and produce warnings.
You can remove them by installing Pygments another source with the bugfix by
executing the command below: (may be outdated)

.. code-block::

    sudo pip install git+https://github.com/AkibAzmain/pygments.git#egg=pygments

.. // TODO: update link of pygments and remove if necessary

Getting the source
++++++++++++++++++

First fork this repository to your account. Then download the source code by
executing the following::

    git clone https://gitlab.com/<CLONED-REPO-PATH>.git

Or, directly download the source code from this repository::

    git clone https://gitlab.com/pypp/pypp.git

.. warning::

    **Do not download source code from releases.** They are intended for being
    compiled and used for production, not for being modified itself.

.. // TODO: add instructions for setting up the environment
