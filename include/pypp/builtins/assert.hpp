/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_ASSERT_HPP
#define PYPP_BUILTINS_ASSERT_HPP

#include <pypp/builtins/exceptions.hpp>
#include <pypp/builtins/raise.hpp>

#include <string>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Asserts whether an expression returns ``true``
         *
         * @details @rst
         *
         * .. index:: py::builtins::assert
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-assert:
         *
         * *py::builtins::*\ assert
         * ========================
         *
         * *Defined in* ``<pypp/builtins/assert.hpp>``
         *
         * .. code-block:: cpp
         *
         *      void assert(bool expression);                               // 1
         *
         *      void assert(bool expression, std::string message);          // 2
         *
         * 1.   Assert an expression and throws on failure.
         *
         * 2.   Assert an expression and throws on failure with given message.
         *
         * ``py::builtins::assert`` asserts whether is given value is ``true``
         * (usually returned from an expression) and raises
         * :ref:`py-builtins-AssertionError` on failure.
         *
         *
         * Parameters
         * ----------
         *
         * ``expression``: The expression to assert.
         *
         * ``message``: The message of exception message.
         *
         *
         * Complexity
         * ----------
         *
         * Constant.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/assert.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::assert(true);
         *
         *          try
         *          {
         *              py::assert(false);
         *          }
         *          catch (py::AssertionError&)
         *          {
         *              py::print("assert failed: py::assert(false)");
         *          }
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      assert failed: py::assert(false)
         *
         * @endrst
         *
         * @param expression the expression to assert
         * @throw py::AssertionError on assertion failed
         */
        void assert(bool expression);

        /**
         * @brief Asserts whether an expression returns ``true``
         *
         * @details @attr
         *
         *      generate false
         *
         * @endattr
         *
         * @param expression the expression to assert
         * @param message what message of exception on failure
         * @throw py::AssertionError on assertion failed
         */
        void assert(bool expression, std::string message);
    }
}

#endif
