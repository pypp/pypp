/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_LITERALS_HPP
#define PYPP_BUILTINS_LITERALS_HPP

#include <pypp/literals.hpp>

namespace py
{
    namespace literals
    {

        /**
         * @brief Namespace containing literals for namespace builtins
         *
         * @details @rst
         *
         * .. index:: py::literals
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-literals:
         *
         * *py::literals::*\ builtins
         * ==========================
         *
         * .. code-block:: cpp
         *
         *      inline namespace builtins {}
         *
         * ``py::literals`` contains literals defined by PyPP for namespace
         * :ref:`py-builtins`.
         *
         * Literals can be accessed by inserting the statement
         * ``using namespace literal_namespace`` into a scope, where
         * ``literal_namespace`` is either ``py::literals::builtins`` or
         * ``py::literals``.
         *
         *
         * Members
         * -------
         *
         * [[[writer_eval: rst_member_tree(node)]]]
         *
         * .. toctree::
         *      :hidden:
         *
         *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
         *
         * @endrst
         *
         */
        inline namespace builtins {}
    }
}

#endif
