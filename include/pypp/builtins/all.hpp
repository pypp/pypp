/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_ALL_HPP
#define PYPP_BUILTINS_ALL_HPP

#include <algorithm>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Checks whether every element of an array is ``true``
         *
         * @details @rst
         *
         * .. index:: py::builtins::all
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-all:
         *
         * *py::builtins::*\ all
         * =====================
         *
         * *Defined at* ``<pypp/builtins/all.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool all(const type& iterable);
         *
         * Checks whether every element of an array is ``true``.
         *
         * .. note::
         *
         *      Calling this function is equivalent to the following:
         *
         *      .. code-block:: cpp
         *
         *          iterable[0] && iterable[1] && iterable[2] && ...
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of iterable object.
         *
         * .. admonition:: Requirements of ``type``
         *
         *      ``type`` must iterable with for loop and elements stored by the
         *      iterable object must be ``bool`` or implicitly convertable to
         *      ``bool``.
         *
         *
         * Parameters
         * ----------
         *
         * ``iterable``: An iterable object.
         *
         *
         * Return value
         * ------------
         *
         * Whether every element of the iterable is ``true``.
         *
         *
         * Complexity
         * ----------
         *
         * Linear in the size of ``iterable`` at most.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/all.hpp>
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<bool> a = {true, true};
         *          py::print(py::all(a));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @tparam type type of iterable array
         * @param iterable an iterable array
         * @return whether every element of an array is ``true``
         */
        template <class type>
        constexpr bool all(const type& iterable)
        {
            return std::all_of(
                iterable.begin(),
                iterable.end(),
                [](const auto& value) -> bool
                {
                    return value;
                }
            );
        }
    }
}

#endif
