/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_ITERATOR_TCC
#define PYPP_BUILTINS_ITERATOR_TCC

#include <cstddef>
#include <iterator>

namespace py
{
    inline namespace builtins
    {
        namespace internal__
        {

            /**
             * @brief Template class for iterators of lists
             *
             * @tparam array_t type of the list, maybe const-qualified
             * @tparam element_t type of element, maybe const-qualified
             */
            template <
                class array_t,
                class element_t,
                class pointer_t = element_t*,
                class reference_t = element_t&
            >
            class iterator
            {
            private:

                /**
                 * @brief Pointer to the list
                 *
                 */
                array_t* const array;

                /**
                 * @brief Index of element pointed by the iterator
                 *
                 */
                std::size_t index;

            public:

                /**
                 * @brief Defines template parameter element_t as value_type
                 *
                 */
                typedef element_t value_type;

                /**
                 * @brief Defines type of pointer to element as pointer
                 *
                 */
                typedef pointer_t pointer;

                /**
                 * @brief Defines type of constant pointer to element as
                 * const_pointer
                 *
                 */
                typedef const pointer_t const_pointer;

                /**
                 * @brief Defines reference to element as reference
                 *
                 */
                typedef reference_t reference;

                /**
                 * @brief Defines constant reference to element as
                 * const_reference
                 *
                 */
                typedef const reference_t const_reference;

                /**
                 * @brief Defines ``std::ptrdiff_t`` as difference_type
                 *
                 */
                typedef std::ptrdiff_t difference_type;

                /**
                 * @brief Defines random access iterator as iterator_category
                 *
                 */
                typedef std::random_access_iterator_tag iterator_category;

                /**
                 * @brief Constructs a new iterator
                 *
                 * @param array pointer to the list
                 * @param index initial index of the element
                 */
                constexpr iterator(array_t* array, std::size_t index)
                    : array(array),
                    index(index)
                {}

                /**
                 * @brief Returns the index of element pointed by the iterator
                 *
                 * @details @rst
                 *
                 * This function returns the index of element pointed by the
                 * iterator.
                 *
                 * .. tip::
                 *
                 *      You can use this function to determine whether the
                 *      iterator is owned by a particular list. Example: (``it``
                 *      holds the iterator and ``array`` is the candidate list)
                 *
                 *      .. code-block:: cpp
                 *
                 *          // Method 1
                 *          if (array.begin() + it.base() == it)
                 *              // Do something
                 *
                 *          // Method 2
                 *          if (array.begin() == it - it.base())
                 *              // Do something
                 *
                 * @endrst
                 *
                 * @return index of element pointed by the iterator
                 */
                constexpr std::size_t base() const
                {
                    return index;
                }

                /**
                 * @brief Dereferences the iterator
                 *
                 * @return reference to element
                 */
                constexpr reference operator * ()
                {
                    return (*array)[index];
                }

                /**
                 * @brief Returns the pointer to the element
                 *
                 * @return pointer to the element
                 */
                constexpr pointer operator -> ()
                {
                    return &(*array)[index];
                }

                /**
                 * @brief Increments the iterator
                 *
                 * @return reference to the iterator after incrementing
                 */
                constexpr iterator& operator ++ ()
                {
                    ++index;
                    return *this;
                }

                /**
                 * @brief Post-increments the iterator
                 *
                 * @return reference to the iterator before incrementing
                 */
                constexpr iterator operator ++ (int)
                {
                    iterator copy = *this;
                    index++;
                    return copy;
                }

                /**
                 * @brief Returns an iterator which is advanced by ``n``
                 * positions
                 *
                 * @param n count of elements to advance
                 * @return iterator which is advanced by ``n`` positions
                 */
                constexpr iterator operator + (difference_type n) const
                {
                    return iterator(array, index + n);
                }

                /**
                 * @brief Advances the iterator ``n`` positions
                 *
                 * @param n count of elements to advance
                 * @return reference to the iterator
                 */
                constexpr iterator& operator += (difference_type n)
                {
                    index += n;
                    return *this;
                }

                /**
                 * @brief Decrements the iterator
                 *
                 * @return reference to the iterator after decrementing
                 */
                constexpr iterator& operator -- ()
                {
                    --index;
                    return *this;
                }

                /**
                 * @brief Post-decrements the iterator
                 *
                 * @return reference to the iterator before decrementing
                 */
                constexpr iterator operator -- (int)
                {
                    iterator copy = *this;
                    index--;
                    return copy;
                }

                /**
                 * @brief Returns an iterator which is ``n`` positions behind
                 *
                 * @param n count of elements to advance
                 * @return iterator which is ``n`` positions behind
                 */
                constexpr iterator operator - (difference_type n) const
                {
                    return iterator(array, index - n);
                }

                /**
                 * @brief Returns difference between two iterators
                 *
                 * @param other another iterator
                 * @return difference between two iterators
                 */
                constexpr difference_type operator - (iterator other) const
                {
                    return base() - other.base();
                }

                /**
                 * @brief Retreats the iterator ``n`` positions
                 *
                 * @param n count of elements to retreat
                 * @return reference to the iterator
                 */
                constexpr iterator& operator -= (difference_type n)
                {
                    index -= n;
                    return *this;
                }

                /**
                 * @brief Checks whether another iterator equals to the iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator equals to the iterator
                 */
                constexpr bool operator == (const iterator& other)
                {
                    return array == other.array && base() == other.base();
                }

                /**
                 * @brief Checks whether another iterator not equals to the
                 * iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator not equals to the iterator
                 */
                constexpr bool operator != (const iterator& other)
                {
                    return !(*this == other);
                }

                /**
                 * @brief Checks whether another iterator is more advanced than
                 * the iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator is more advanced than the
                 * iterator
                 */
                constexpr bool operator > (const iterator& other)
                {
                    return array == other.array && base() > other.base();
                }

                /**
                 * @brief Checks whether another iterator is more advanced or
                 * equals to the iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator is more advanced or equals
                 * to the iterator
                 */
                constexpr bool operator >= (const iterator& other)
                {
                    return array == other.array && base() >= other.base();
                }

                /**
                 * @brief Checks whether the iterator is more advanced than
                 * another iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator is more advanced than the
                 * iterator
                 */
                constexpr bool operator < (const iterator& other)
                {
                    return array == other.array && base() < other.base();
                }

                /**
                 * @brief Checks whether the iterator is more advanced or equals
                 * to another iterator
                 *
                 * @param other another iterator to compare
                 * @return whether another iterator is more advanced or equals
                 * to the iterator
                 */
                constexpr bool operator <= (const iterator& other)
                {
                    return array == other.array && base() <= other.base();
                }
            };

            template <class array_t, class element_t>
            iterator<array_t, element_t> operator + (
                std::ptrdiff_t n,
                iterator<array_t, element_t> it
            )
            {
                return it + n;
            }
        }
    }
}

#endif
