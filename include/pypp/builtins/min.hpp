/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_MIN_HPP
#define PYPP_BUILTINS_MIN_HPP

#include <type_traits>
#include <algorithm>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Returns the minimum value of an iterable
         *
         * @details @rst
         *
         * .. index:: py::builtins::min
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-min:
         *
         * *py::builtins::*\ min
         * =====================
         *
         * *Defined in* ``<pypp/builtins/min.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class it_type>
         *      constexpr auto min(const it_type& iterable) -> decltype(*iterable.begin()); // 1
         *
         *      template <
         *          class type,
         *          class... arguments,
         *          class = std::enable_if_t<
         *              std::conjunction<
         *                  std::is_same<type, arguments>...
         *              >::value
         *          >
         *      >
         *      const type& min(const type& a, const arguments&... args);                   // 2
         *
         * 1.   Returns the minimum value in an iterable.
         *
         * 2.   Returns the minimum value in provided arguments.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``it_type``: Type of ``iterable``.
         *
         * ``type``: Type of arguments to compare.
         *
         * .. admonition:: Requirements of ``it_type``
         *
         *      ``it_type`` must be comparable.
         *
         *
         * Parameters
         * ----------
         *
         * ``iterable``: An iterable object.
         *
         * ``a``: A comparable object.
         *
         * ``args``: Variable number comparable objects.
         *
         *
         * Return value
         * ------------
         *
         * 1.   The minimum value in the iterable.
         *
         * 2.   The minimum value in provided arguments.
         *
         *
         * Complexity
         * ----------
         *
         * 1.   Linear in the size of ``iterable``.
         *
         * 2.   Linear in the count of total arguments.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/min.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::print(py::min(2, 3, 1, 4, 9));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      1
         *
         * @endrst
         *
         * @tparam it_type type of iterable
         * @param iterable an iterable array
         * @return minimum value of an iterable
         */
        template <class it_type>
        constexpr auto min(const it_type& iterable) -> decltype(*iterable.begin())
        {
            return *std::min_element(iterable.begin(), iterable.end());
        }

        #ifndef DOXYGEN_DOCUMENTATION_BUILD

        /**
         * @brief Returns the minimum value of provided arguments
         *
         * @details @rst
         *
         * This functions is recursion breaker for next function taking variable
         * number argument, see that.
         *
         * @endrst
         *
         * @tparam type type of arguments
         * @param a first argument
         * @param b first argument
         * @return minimum value of provided arguments
         */
        template <class type>
        constexpr const type& min(const type& a, const type& b)
        {
            return std::min(a, b);
        }

        #endif

        /**
         * @brief Returns the minimum value of provided arguments
         *
         * @details @attr
         *
         *      generate false
         *
         * @endattr
         *
         * @tparam type type of arguments
         * @param a first argument
         * @param args other argument of same type as ``a``
         * @return minimum value of provided arguments
         */
        template <
            class type,
            class... arguments,
            class = std::enable_if_t<
                std::conjunction<
                    std::is_same<type, arguments>...
                >::value
            >
        >
        constexpr const type& min(const type& a, const arguments&... args)
        {
            return std::min(a, min(args...));
        }
    }
}

#endif
