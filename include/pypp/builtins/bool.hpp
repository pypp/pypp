/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_BOOL_HPP
#define PYPP_BUILTINS_BOOL_HPP

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Defines ``False`` as ``false``
         *
         * @details @rst
         *
         * .. index:: py::builtins::False
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-False:
         *
         * *py::builtins::*\ False
         * =======================
         *
         * *Defined in* ``<pypp/builtins/bool.hpp>``
         *
         * .. code-block:: cpp
         *
         *      static constexpr bool False = false;
         *
         * Defines ``False`` as ``false``.
         *
         * @endrst
         *
         */
        static constexpr bool False = false;

        /**
         * @brief Defines ``True`` as ``true``
         *
         * @details @rst
         *
         * .. index:: py::builtins::True
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-True:
         *
         * *py::builtins::*\ True
         * ======================
         *
         * *Defined in* ``<pypp/builtins/bool.hpp>``
         *
         * .. code-block:: cpp
         *
         *      static constexpr bool True = true;
         *
         * Defines ``True`` as ``true``.
         *
         * @endrst
         *
         */
        static constexpr bool True = true;
    }
}

#endif
