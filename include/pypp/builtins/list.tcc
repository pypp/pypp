/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_LIST_TCC
#define PYPP_BUILTINS_LIST_TCC

#include <pypp/builtins/list.hpp>
#include <pypp/builtins/exceptions.hpp>
#include <pypp/builtins/raise.hpp>
#include <pypp/builtins/abs.hpp>

#include <algorithm>
#include <stdexcept>
#include <iterator>
#include <utility>
#include <limits>

namespace py
{
    inline namespace builtins
    {
        template <class type, class allocator>
        constexpr list<type, allocator>::list(long start, long stop, long step, list* parent)
            : array(),
            parent(parent),
            slice_prop{start, stop, step}
        {}

        template <class type, class allocator>
        list<type, allocator>::list()
            : array(),
            parent(nullptr),
            slice_prop{}
        {}

        template <class type, class allocator>
        template <class array_t>
        list<type, allocator>::list(const array_t& other)
            : array(),
            parent(nullptr),
            slice_prop()
        {
            extend(other);
        }

        template <class type, class allocator>
        list<type, allocator>::list(const std::initializer_list<type>& array)
            : array(),
            parent(nullptr),
            slice_prop{}
        {
            extend(array);
        }

        template <class type, class allocator>
        list<type, allocator>::list(const list& other)
            : array(),
            parent(nullptr),
            slice_prop()
        {
            extend(other);
        }

        template <class type, class allocator>
        list<type, allocator>::list(list&& other)
            : array(std::move(other.array)),
            parent(nullptr),
            slice_prop()
        {}

        template <class type, class allocator>
        std::size_t list<type, allocator>::validate_index(long index) const
        {

            // If index is negative, make it positive
            if (index < 0)
                index += size();

            // Make sure index isn't OUT OF RANGE
            if (index < 0 || static_cast<std::size_t>(index) >= size())
                raise(IndexError("index out of range"));

            return index;
        }

        template <class type, class allocator>
        constexpr std::size_t list<type, allocator>::parent_index(std::size_t index) const
        {
            slice_data slice_properties = slice_prop;
            if (slice_properties.start < 0)
                slice_properties.start += parent->size();

            return slice_properties.start + (slice_properties.step * index);
        }

        template <class type, class allocator>
        std::size_t list<type, allocator>::count(const_reference value) const
        {
            std::size_t i = 0;

            std::for_each(
                this->begin(),
                this->end(),
                [&](const value_type& element) -> void
                {
                    i += (element == value);
                }
            );

            return i;
        }

        template <class type, class allocator>
        std::size_t list<type, allocator>::size() const
        {

            // Check if the list is a slice
            if (parent)
            {
                slice_data slice_properties = slice_prop;

                // If slice's start and stop is negative, make them positive
                if (slice_properties.start < 0)
                    slice_properties.start += parent->size();
                if (slice_properties.stop < 0)
                {
                    if (slice_properties.stop == std::numeric_limits<long>::min())
                        slice_properties.stop = -1 * static_cast<long>(parent->size() + 1);

                    slice_properties.stop += parent->size();
                }

                // If slice's stop is never reachable with slice's start and
                // step properties, return 0
                if (
                    (slice_properties.start < slice_properties.stop && slice_properties.step < 0) ||
                    (slice_properties.start > slice_properties.stop && slice_properties.step > 0)
                )
                {
                    return 0;
                }

                // Calculate the count and return it
                return abs(slice_properties.stop - slice_properties.start) /
                    abs(slice_properties.step);
            }

            // Return array's size
            return array.size();
        }

        template <class type, class allocator>
        std::size_t list<type, allocator>::index(const_reference element) const
        {
            std::size_t i = 0;
            for (; i < size(); i++)
            {
                if (&((*this)[i]) == &element)
                    return i;
            }

            i = 0;
            for (; i < size(); i++)
                if ((*this)[i] == element)
                    return i;

            raise(ValueError("given element is not in list"));
        }

        template <class type, class allocator>
        void list<type, allocator>::append(const_reference element)
        {
            if (!parent) array.push_back(element);
        }

        template <class type, class allocator>
        void list<type, allocator>::append(value_type&& element)
        {
            if (!parent) array.push_back(element);
        }

        template <class type, class allocator>
        void list<type, allocator>::insert(long index, const_reference element)
        {
            if (!parent) array.insert(array.begin() + index, element);
        }

        template <class type, class allocator>
        void list<type, allocator>::insert(long index, value_type&& element)
        {
            if (!parent) array.insert(array.begin() + index, element);
        }

        template <class type, class allocator>
        template <class input_it>
        void list<type, allocator>::insert(long index, input_it first, input_it last)
        {
            for (input_it it = first; it != last; it++)
                insert(index++, *it);
        }

        template <class type, class allocator>
        template <class iterable_type>
        void list<type, allocator>::extend(const iterable_type& iterable)
        {
            if (!parent)
            {
                std::for_each(
                    iterable.begin(),
                    iterable.end(),
                    [&](const value_type& element) -> void
                    {
                        append(element);
                    }
                );
            }
        }

        template <class type, class allocator>
        void list<type, allocator>::clear()
        {
            if (!parent) array.clear();
        }

        template <class type, class allocator>
        type list<type, allocator>::pop(long index)
        {

            // Vaildate index (and modify if required)
            try
            {
                index = validate_index(index);
            }
            catch (IndexError& exception)
            {
                raise(IndexError(std::string("pop ") + exception.what()));
            }

            value_type removed_element = (*this)[index];

            if (!parent)
            {
                array.erase(array.begin() + index);
            }

            return removed_element;
        }

        template <class type, class allocator>
        void list<type, allocator>::remove(const_reference value)
        {
            if (!parent)
                pop(index(value));
        }

        template <class type, class allocator>
        template <class compare_func_t>
        void list<type, allocator>::sort(bool reverse, compare_func_t key)
        {
            if (!key)
                key = [](reference element) -> reference
                {
                    return element;
                };

            std::function<bool(reference, reference)> compare =
                [&](reference first, reference second)
            {
                return key(first) < key(second);
            };
            std::sort(array.begin(), array.end(), compare);

            if (reverse)
                this->reverse();
        }

        template <class type, class allocator>
        void list<type, allocator>::reverse()
        {
            if (!parent)
            {
                std::reverse(array.rbegin(), array.rend());
            }
        }

        template <class type, class allocator>
        typename list<type, allocator>::iterator list<type, allocator>::begin()
        {
            return iterator(this, 0);
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_iterator list<type, allocator>::begin() const
        {
            return cbegin();
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_iterator list<type, allocator>::cbegin() const
        {
            return const_iterator(this, 0);
        }

        template <class type, class allocator>
        typename list<type, allocator>::reverse_iterator list<type, allocator>::rbegin()
        {
            return reverse_iterator(end());
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_reverse_iterator list<type, allocator>::rbegin() const
        {
            return crbegin();
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_reverse_iterator list<type, allocator>::crbegin() const
        {
            return const_reverse_iterator(end());
        }

        template <class type, class allocator>
        typename list<type, allocator>::iterator list<type, allocator>::end()
        {
            return iterator(this, size());
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_iterator list<type, allocator>::end() const
        {
            return cend();
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_iterator list<type, allocator>::cend() const
        {
            return const_iterator(this, size());
        }

        template <class type, class allocator>
        typename list<type, allocator>::reverse_iterator list<type, allocator>::rend()
        {
            return reverse_iterator(begin());
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_reverse_iterator list<type, allocator>::rend() const
        {
            return crend();
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_reverse_iterator list<type, allocator>::crend() const
        {
            return const_reverse_iterator(begin());
        }

        template <class type, class allocator>
        typename list<type, allocator>::reference list<type, allocator>::operator [] (long index)
        {

            // Vaildate index (and modify if required)
            try
            {
                index = validate_index(index);
            }
            catch (IndexError& exception)
            {
                raise(IndexError(std::string("list ") + exception.what()));
            }

            if (parent)
                return (*parent)[parent_index(index)];

            return array[index];
        }

        template <class type, class allocator>
        typename list<type, allocator>::const_reference list<type, allocator>::operator [] (long index) const
        {
            // Vaildate index (and modify if required)
            try
            {
                index = validate_index(index);
            }
            catch (IndexError& exception)
            {
                raise(IndexError(std::string("list ") + exception.what()));
            }

            if (parent)
                return (*parent)[parent_index(index)];

            return array[index];
        }

        template <class type, class allocator>
        list<type, allocator> list<type, allocator>::operator [] (std::string properties)
        {
            slice_data data = internal__::slice::from_str(properties);
            return list(data.start, data.stop, data.step, this);
        }

        template <class type, class allocator>
        const list<type, allocator> list<type, allocator>::operator [] (std::string properties) const
        {
            slice_data data = internal__::slice::from_str(properties);
            return list(data.start, data.stop, data.step, const_cast<list*>(this));
        }

        template <class type, class allocator>
        list<type, allocator>& list<type, allocator>::operator = (const list& other)
        {
            if (parent)
            {

                // Make sure assigning is eligible (and possible)
                if (other.size() != size() && slice_prop.step != 1)
                    raise(ValueError("attempt to assign to non-continuous slice"));

                std::size_t i = 0;

                // Copy all elements
                for (; i < size() && i < other.size(); i++)
                {
                    (*this)[i] = other[i];
                }

                if (i < size())
                {

                    // Remove extra elements from the slice
                    for (std::size_t extra = size() - i; extra != 0; extra--)
                    {
                        parent->pop(parent_index(i));
                    }
                }
                else
                {

                    // Append extra element to slice from given slice
                    for (; i < other.size(); i++)
                    {
                        parent->insert(parent_index(i), other[i]);
                    }
                }
            }
            else
            {
                clear();
                extend(other);
            }

            return *this;
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator < (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            std::size_t i = 0;
            for (; i < lhs.size() && i < rhs.size(); i++)
            {
                if (lhs[i] < rhs[i])
                {
                    return true;
                }
                else if (lhs[i] > rhs[i])
                {
                    return false;
                }
            }

            return lhs.size() < rhs.size();
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator > (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            std::size_t i = 0;
            for (; i < lhs.size() && i < rhs.size(); i++)
            {
                if (lhs[i] > rhs[i])
                {
                    return true;
                }
                else if (lhs[i] < rhs[i])
                {
                    return false;
                }
            }

            return lhs.size() > rhs.size();
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator == (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            return !(lhs < rhs || lhs > rhs);
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator != (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            return !(lhs == rhs);
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator <= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            return !(lhs > rhs);
        }

        template <class type, class allocator_a, class allocator_b>
        bool operator >= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs)
        {
            return !(lhs < rhs);
        }
    }
}

#endif
