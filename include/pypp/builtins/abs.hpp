/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_ABS_HPP
#define PYPP_BUILTINS_ABS_HPP

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Returns absolute value of ``x``
         *
         * @details @rst
         *
         * .. index:: py::builtins::abs
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-abs:
         *
         * *py::builtins::*\ abs
         * =====================
         *
         * *Defined in* ``<pypp/builtins/abs.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      auto abs(const type& x) -> decltype(x * 1)
         *
         * Returns the absolute value of ``x``.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of paramter.
         *
         * .. admonition:: Requirement of ``type``
         *
         *      ``type`` must be a scalar type or any other type meeting the
         *      following requirements:
         *
         *      *   Comparable with operator ``<``
         *
         *      *   Multiplable with operator ``*``
         *
         *
         * Parameters
         * ----------
         *
         * ``x``: The value whose absolute value to return.
         *
         *
         * Return value
         * ------------
         *
         * The absolute value of ``x``. The type of return value is
         * ``decltype(x * 1)``.
         *
         *
         * Complexity
         * ----------
         *
         * Constant.
         *
         *
         * Specializing
         * ------------
         *
         * ``py::builtins::abs`` specialized as the following when the return
         * value and the parameter type is same::
         *
         *      template <>
         *      /\* type *\/ py::builtins::abs< /\* type *\/ >(const /\* type *\/ & x);
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/abs.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::print(py::abs(-5));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      5
         *
         * @endrst
         *
         * @tparam type type of ``x``
         * @param x a signed number
         * @return absolute value of ``x``
         */
        template <class type>
        auto abs(const type& x) -> decltype(x * 1)
        {
            return x * (x < 0 ? -1 : 1);
        }
    }
}

#endif
