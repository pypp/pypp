/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_IN_HPP
#define PYPP_BUILTINS_IN_HPP

#include <type_traits>
#include <algorithm>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Class for implementing python's ``in`` operator
         *
         * @details @rst
         *
         * .. index:: py::builtins::in
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-in:
         *
         * *py::builtins::*\ in
         * ====================
         *
         * *Defined in* ``<pypp/builtins/in.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      class in;
         *
         * Implements Python's ``in`` operator.
         *
         * ``py::builtins::in`` implements Python's ``in`` operator, used to
         * check whether a iterable contains a value.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of iterable.
         *
         *
         * Members
         * -------
         *
         * [[[writer_eval: rst_member_tree(node)]]]
         *
         * .. toctree::
         *      :hidden:
         *
         *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
         *
         *
         * Non-member helpers
         * ------------------
         *
         * *    :ref:`operator ->* <py-builtins-operator-pointer_to_member-in>`
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/in.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> nums = {0, 1, 2, 3, 4, 5, 6};
         *
         *          // Equivalent to ``4 in nums`` in Python
         *          py::print(4 ->* py::in(nums));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @tparam type type of container
         */
        template <class type>
        class in
        {
        private:

            /**
             * @brief The container wrapped by the in object
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             */
            const type& iterable;

        public:

            /**
             * @brief Constructs a new ``in`` object
             *
             * @details @rst
             *
             * .. index:: py::builtins::in::in
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-in-in:
             *
             * *py::builtins::in::*\ in
             * ========================
             *
             * .. code-block:: cpp
             *
             *      explicit constexpr in(const type& iterable);
             *
             * Constructs an ``py::builtins::in`` object.
             *
             *
             * Parameters
             * ----------
             *
             * ``iterable``: An iterable object.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/in.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> nums = {0, 1, 2, 3, 4, 5, 6};
             *
             *          // Equivalent to ``4 in nums`` in Python
             *          py::print(4 ->* py::in(nums));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      true
             *
             * @endrst
             *
             * @param iterable iterable array
             */
            explicit constexpr in(const type& iterable)
                : iterable(iterable)
            {}

            /**
             * @brief Return the container wrapped by the ``in`` object
             *
             * @details @rst
             *
             * .. index:: py::builtins::in::container
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-in-container:
             *
             * *py::builtins::in::*\ container
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const type& container() const;
             *
             * Returns constant reference to the iterable object referenced by
             * the ``py::builtins::in`` object.
             *
             *
             * Return value
             * ------------
             *
             * Constant reference to the iterable object referenced by the
             * ``py::builtins::in`` object.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/in.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> nums = {0, 1, 2, 3, 4, 5, 6};
             *          py::print(nums == py::in(nums).container());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      true
             *
             * @endrst
             *
             * @return container wrapped by the ``in`` object
             */
            constexpr const type& container() const
            {
                return iterable;
            }
        };

        /**
         * @brief Returns whether the iterable array contains given ``element``
         *
         * @details @rst
         *
         * .. index::  py::builtins::operator ->* [py::builtins::in]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-pointer_to_member-in:
         *
         * *py::builtins::*\ operator ->* (:ref:`py::builtins::in <py-builtins-in>`)
         * =========================================================================
         *
         * *Defined in* ``<pypp/builtins/min.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class element_t, class container_t>
         *      bool operator ->* (const element_t& element, const in<container_t>& container);
         *
         * Returns whether a iterable object contains a value.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``element_t``: Type of the value to find.
         *
         * ``container_t``: Type of the container.
         *
         *
         * Parameters
         * ----------
         *
         * ``element``: The value to find.
         *
         * ``container``: An ``py::builtins::in`` object referencing an iterable
         * object.
         *
         *
         * Return value
         * ------------
         *
         * Whether a iterable object contains a value.
         *
         *
         * Complexity
         * ----------
         *
         * Linear in the size of ``container.container()``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/in.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> nums = {0, 1, 2, 3, 4, 5, 6};
         *
         *          // Equivalent to ``4 in nums`` in Python
         *          py::print(4 ->* py::in(nums));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator ->* (py::builtins::in)
         *      member_tree_label operator ->* (py::builtins::in)
         *
         * @endattr
         *
         * @tparam element_t type of element
         * @tparam container_t type of container
         *
         * @param element the element to look for
         * @param container ``in`` object which wrapped the container
         * @return whether the iterable array contains given ``element``
         */
        template <class element_t, class container_t>
        bool operator ->* (const element_t& element, const in<container_t>& container)
        {
            return std::any_of(
                container.container().begin(),
                container.container().end(),
                [&](const element_t& candidate) -> bool
                {
                    return element == candidate;
                }
            );
        }
    }
}

#endif
