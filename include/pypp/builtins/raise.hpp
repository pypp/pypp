/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_RAISE_HPP
#define PYPP_BUILTINS_RAISE_HPP

#include <pypp/builtins/exceptions.hpp>

#include <type_traits>
#include <utility>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Raises an exception
         *
         * @details @rst
         *
         * .. index:: py::builtins::raise
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-raise:
         *
         * *py::builtins::*\ raise
         * =======================
         *
         * *Defined in* ``<pypp/builtins/raise.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      [[noreturn]] constexpr void raise(type&& exception);        // 1
         *
         *      template <class type>
         *      [[noreturn]] constexpr void raise(type& exception);         // 2
         *
         *      template <class type>
         *      [[noreturn]] constexpr void raise(const type& exception);   // 3
         *
         * Throws or raises an exception.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of exception.
         *
         *
         * Parameters
         * ----------
         *
         * ``exception``: Exception object to raise.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/raise.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          try
         *          {
         *              py::raise(py::BaseException("example exception"));
         *          }
         *          catch (py::BaseException& exception)
         *          {
         *              py::print("exception handled successfully:", exception);
         *          }
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      exception handled successfully: example exception
         *
         * @endrst
         *
         * @tparam type type of the exception
         * @param exception the exception to raise
         */
        template <class type>
        [[noreturn]] constexpr void raise(type&& exception)
        {
            throw std::move(exception);
        }

        /**
         * @brief Raises an exception
         *
         * @details @rst
         *
         * This function throws or raises an exception.
         *
         * @endrst
         *
         * @attr
         *
         *      generate false
         *
         * @endattr
         *
         * @tparam type type of the exception
         *
         * @param exception the exception to raise
         */
        template <class type>
        [[noreturn]] constexpr void raise(type& exception)
        {
            throw exception;
        }

        /**
         * @brief Raises an exception
         *
         * @details @rst
         *
         * This function throws or raises an exception.
         *
         * @endrst
         *
         * @attr
         *
         *      generate false
         *
         * @endattr
         *
         * @tparam type type of the exception
         *
         * @param exception the exception to raise
         */
        template <class type>
        [[noreturn]] constexpr void raise(const type& exception)
        {
            throw exception;
        }
    }
}

#endif
