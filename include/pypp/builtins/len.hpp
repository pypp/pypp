/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_LEN_HPP
#define PYPP_BUILTINS_LEN_HPP

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Returns the size of a container
         *
         * @details @rst
         *
         * .. index:: py::builtins::len
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-len:
         *
         * *py::builtins::*\ len
         * =====================
         *
         * .. code-block:: cpp
         *
         *      constexpr auto len(const type& container) -> decltype(container.size());
         *
         * Returns the size of a container.
         *
         * .. note::
         *
         *      This function doesn't return ``sizeof(container)``, it returns
         *      ``container.size()``.
         *
         * .. tip::
         *
         *      Equivalent to::
         *
         *          container.size();
         *
         *
         * Return value
         * ------------
         *
         * The size of a container.
         *
         *
         * Complexity
         * ----------
         *
         * Constant.
         *
         *
         * Example
         * -------
         *
         * .. code-block::
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::print(py::len(py::list<int>({4, 5, 3, 2, 8, 9, 0})));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      7
         *
         * @endrst
         *
         * @tparam type type of container
         * @param container a container with public size() function
         */
        template <class type>
        constexpr auto len(const type& container) -> decltype(container.size())
        {
            return container.size();
        }
    }
}

#endif
