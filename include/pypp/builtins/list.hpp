/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_LIST_HPP
#define PYPP_BUILTINS_LIST_HPP

#include <pypp/builtins/iterator.tcc>
#include <pypp/builtins/slice.tcc>

#include <initializer_list>
#include <functional>
#include <memory>
#include <vector>
#include <iterator>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Template class for holding a list of elements
         *
         * @details @rst
         *
         * .. index:: py::builtins::list
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-list:
         *
         * *py::builtins::*\ list
         * ======================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <
         *          class type,
         *          class allocator = std::allocator<type>
         *      >
         *      class list;
         *
         * Container for holding and randomly accessing elements of a single
         * type.
         *
         * ``py::builtins::list`` allows to hold elements of a single type and
         * access them randomly in constant time.
         *
         * This container stores element contiguously, so elements can be
         * accessed with offsets on pointers to other elements (not applicable
         * when element type is ``bool``, see :ref:`below
         * <list-uses-std-vector>`).
         *
         * .. _list-uses-std-vector:
         *
         * .. note::
         *
         *      ``py::builtins::list`` uses ``std::vector`` to hold elements, so
         *      when using this container to hold element of type ``bool``,
         *      every element usually uses a bit instead of a byte
         *      (*depends on the C++ standard library implementation*).
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of elements.
         *
         * ``allocator``: The type of allocator.
         *
         *
         * Members
         * -------
         *
         * [[[writer_eval: rst_member_tree(node)]]]
         *
         * .. toctree::
         *      :hidden:
         *
         *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
         *
         *
         * Non-member helpers
         * ------------------
         *
         * *    :ref:`operator \< <py-builtins-operator-less_than-list>`
         *
         * *    :ref:`operator > <py-builtins-operator-more_than-list>`
         *
         * *    :ref:`operator == <py-builtins-operator-equals-list>`
         *
         * *    :ref:`operator != <py-builtins-operator-not_equals-list>`
         *
         * *    :ref:`operator \<= <py-builtins-operator-less_than_or_equal-list>`
         *
         * *    :ref:`operator >= <py-builtins-operator-more_than_or_equal-list>`
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> list = {0, 1, 2, 3, 4, 5, 6};
         *
         *          py::print(list);
         *
         *          list.append(7);
         *
         *          py::print(list);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      {0, 1, 2, 3, 4, 5, 6}
         *      {0, 1, 2, 3, 4, 5, 6, 7}
         *
         * @endrst
         *
         * @tparam type type of elements
         * @tparam allocator type of allocator
         */
        template <class type, class allocator = std::allocator<type>>
        class list
        {
        private:

            /**
             * @brief The container holding actual elements
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             */
            std::vector<type, allocator> array;

            /**
             * @brief Parent of the slice, ``nullptr`` if the list isn't a slice
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             */
            list* const parent;

            /**
             * @brief Structure to hold properties of a slice
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             */
            typedef internal__::slice slice_data;

            /**
             * @brief If the list is a slice, this holds the properties of it
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             */
            const slice_data slice_prop;

            /**
             * @brief Return the index of the actual element at parent list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @return index of the actual element at parent list
             */
            constexpr std::size_t parent_index(std::size_t index) const;

            /**
             * @brief Validates the index and modifies it if required
             *
             * @details @rst
             *
             * This function takes the index and makes it positive if it's
             * negative. If this fails (fails to negative to positive) or the
             * index is out of range, an instance of py::IndexError is thrown.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param index candidate index
             * @return ``index``, may be modified
             * @throw py::IndexError if the index is out of range
             */
            std::size_t validate_index(long index) const;

            /**
             * @brief Construct a new list which is a slice of ``parent``
             *
             * @details @rst
             *
             * This contructor constructs the list so it is an slice of given
             * ``parent``. The constructed slice does not copy elements from its
             * parent, instead it works like a pipe between parent and using
             * function (or method) which passes information back and forth.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param start start of slice
             * @param stop stop of slice
             * @param step step of slice
             * @param parent parent list holding elements (can be a slice)
             */
            constexpr list(long start, long stop, long step, list* parent);

        public:

            /**
             * @brief Defines type of elements as value_type
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::value_type
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-value_type:
             *
             * *py::builtins::list::*\ value_type
             * ==================================
             *
             * .. code-block:: cpp
             *
             *      typedef type value_type;
             *
             * The type of elements.
             *
             * @endrst
             *
             */
            typedef type value_type;

            /**
             * @brief Defines allocator type as allocator_type
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::allocator_type
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-allocator_type:
             *
             * *py::builtins::list::*\ allocator_type
             * ======================================
             *
             * .. code-block:: cpp
             *
             *      typedef allocator allocator_type;
             *
             * The type of allocator.
             *
             * @endrst
             *
             */
            typedef allocator allocator_type;

            /**
             * @brief Defines size_type
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::size_type
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-size_type:
             *
             * *py::builtins::list::*\ size_type
             * =================================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::size_type size_type;
             *
             * An unsigned integer type returned by :ref:`py-builtins-list-size`.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::size_type size_type;

            /**
             * @brief Defines difference_type
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::difference_type
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-difference_type:
             *
             * *py::builtins::list::*\ difference_type
             * =======================================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::difference_type difference_type;
             *
             * A signed integer type used in pointer arithmetics.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::difference_type difference_type;

            /**
             * @brief Defines reference
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::reference
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-reference:
             *
             * *py::builtins::list::*\ reference
             * =================================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::reference reference;
             *
             * The type of reference to elements.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::reference reference;

            /**
             * @brief Defines const_reference
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::const_reference
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-const_reference:
             *
             * *py::builtins::list::*\ const_reference
             * =======================================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::const_reference const_reference;
             *
             * The type of constant reference to elements.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::const_reference const_reference;

            /**
             * @brief Defines const_pointer
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::pointer
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-pointer:
             *
             * *py::builtins::list::*\ pointer
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::pointer pointer;
             *
             * The type of pointer to elements.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::pointer pointer;

            /**
             * @brief Defines const_pointer
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::const_pointer
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-const_pointer:
             *
             * *py::builtins::list::*\ const_pointer
             * =====================================
             *
             * .. code-block:: cpp
             *
             *      typedef typename std::vector<type, allocator>::const_pointer const_pointer;
             *
             * The type of constant pointer to elements.
             *
             * @endrst
             *
             */
            typedef typename std::vector<type, allocator>::const_pointer const_pointer;

            /**
             * @brief Defines the type of iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-iterator:
             *
             * *py::builtins::list::*\ iterator
             * ================================
             *
             * .. code-block:: cpp
             *
             *      typedef /\* ... *\/ iterator;
             *
             * The type of iterator to elements.
             *
             * @endrst
             *
             */
            typedef internal__::iterator<list, value_type> iterator;

            /**
             * @brief Defines the type of constant iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::const_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-const_iterator:
             *
             * *py::builtins::list::*\ const_iterator
             * ======================================
             *
             * .. code-block:: cpp
             *
             *      typedef /\* ... *\/ const_iterator;
             *
             * Type of constant iterator to elements.
             *
             * @endrst
             *
             */
            typedef internal__::iterator<const list, const value_type> const_iterator;

            /**
             * @brief Defines the type of reverse iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::reverse_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-reverse_iterator:
             *
             * *py::builtins::list::*\ reverse_iterator
             * ========================================
             *
             * .. code-block:: cpp
             *
             *      typedef std::reverse_iterator<iterator> reverse_iterator;
             *
             * The type of reverse iterator to elements.
             *
             * @endrst
             *
             */
            typedef std::reverse_iterator<iterator> reverse_iterator;

            /**
             * @brief Defines the type of constant reverse iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::const_reverse_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-const_reverse_iterator:
             *
             * *py::builtins::list::*\ const_reverse_iterator
             * ==============================================
             *
             * .. code-block:: cpp
             *
             *      typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
             *
             * The type of constant reverse iterator to elements.
             *
             * @endrst
             *
             */
            typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

            /**
             * @brief Constructs a new empty list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::list
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-list:
             *
             * *py::builtins::list::*\ list
             * ============================
             *
             * .. code-block:: cpp
             *
             *      list();                                                 // 1
             *
             *      template <class array_t>
             *      explicit list(const array_t& array);                    // 2
             *
             *      list(const std::initializer_list<type>& array);         // 3
             *
             *      list(const list& other);                                // 4
             *
             *      list(list&& other);                                     // 5
             *
             * 1.   Constructs an empty list.
             *
             * 2.   Constructs a list from another iterable object.
             *
             * 3.   Constructs a list from a initializer list.
             *
             * 4.   Copy constructs a list.
             *
             * 5.   Move constructs a list.
             *
             *
             * Template parameters
             * -------------------
             *
             * ``array_t``: The type of another iterable object.
             *
             *
             * Parameters
             * ----------
             *
             * ``array``: (2): another iterable object, (3): an initializer
             * list.
             *
             * ``other``: Another ``py::builtins::list`` object.
             *
             *
             * Complexity
             * ----------
             *
             * 1.   Constant.
             *
             * 2.   Linear in the size of ``array``.
             *
             * 3.   Same as (2).
             *
             * 4.   Linear in the size of ``other``.
             *
             * 5.   Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *
             *          // Default constructor
             *          py::list<int> list;
             *
             *          py::print(list); // No output...
             *
             *          py::list<int> list2 = {0, 1, 2, 3, 4, 5, 6};
             *
             *          py::print(list2);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {}
             *      {0, 1, 2, 3, 4, 5, 6}
             *
             * @endrst
             *
             */
            list();

            /**
             * @brief Constructs a new list from another iterable
             *
             * @details @rst
             *
             * This constructor constructs a new list from another iterable. It
             * copies all elements, so elements must be copy constructable in
             * order to use this constructor.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @tparam type of another array
             * @param array another iterable to copy
             */
            template <class array_t>
            explicit list(const array_t& array);

            /**
             * @brief Constructs a new list from an initializer list
             *
             * @details @rst
             *
             * This constructor constructs a new list from an initializer list.
             * It copies all elements, so elements must be copy constructable in
             * order to use this constructor.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param array an initializer list
             */
            list(const std::initializer_list<type>& array);

            /**
             * @brief Constructs a new list from another list with same template
             * parameters
             *
             * @details @rst
             *
             * This constructor constructs a new list from another list with
             * same template parameters. It copies all elements, so elements
             * must be copy constructable in order to use this constructor.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param other another list to copy
             */
            list(const list& other);

            /**
             * @brief Move constructs a new list
             *
             * @details @rst
             *
             * This constructor moves everything from another list to the new
             * list. If another list is a slice, the new list is also a slice.
             *
             * @endrst
             *
             * @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param other another list
             */
            list(list&& other);

            /**
             * @brief Returns the count of elements with given value
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::count
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-count:
             *
             * *py::builtins::list::*\ count
             * =============================
             *
             * .. code-block:: cpp
             *
             *      std::size_t count(const_reference value) const;
             *
             * Returns the count of elements with given value.
             *
             *
             * Parameters
             * ----------
             *
             * ``value``: The value of look for.
             *
             *
             * Return value
             * ------------
             *
             * The count of elements with given value.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of the list.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::print(py::list<int>({0, 2, 1, 4, 0, 3, 0}).count(0));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @param value the value to find
             * @return count of elements with given value
             */
            std::size_t count(const_reference value) const;

            /**
             * @brief Returns the count of elements stored by the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::size
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-size:
             *
             * *py::builtins::list::*\ size
             * ============================
             *
             * .. code-block:: cpp
             *
             *      std::size_t size() const;
             *
             * Returns the count of elements stored by the list.
             *
             *
             * Return value
             * ------------
             *
             * The count of elements stored by the list.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block::
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::print(py::list<int>({4, 5, 3, 2, 8, 9, 0}).size());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      7
             *
             * @endrst
             *
             * @return count of elements stored
             */
            std::size_t size() const;

            /**
             * @brief Returns index of given element
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::index
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-index:
             *
             * *py::builtins::list::*\ index
             * =============================
             *
             * .. code-block:: cpp
             *
             *      std::size_t index(const_reference element) const;
             *
             * Returns the index of given element.
             *
             * ``py::builtins::list::index`` returns the index of given element.
             * It first checks if there is any ``element`` with the same address
             * as the address of given element, and return the index of that
             * element if found, otherwise ``element`` compared by value with
             * elements in the list.
             *
             * .. note::
             *
             *      When comparing by value, if the list contains more than one
             *      element with given value, index of the first one is
             *      returned.
             *
             *
             * Parameter
             * ---------
             *
             * ``element``: The element of look for.
             *
             *
             * Return value
             * ------------
             *
             * The index of given element.
             *
             *
             * Exceptions
             * ----------
             *
             * :ref:`py-builtins-ValueError` if the element can't be found.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the position of ``element`` in the list if there is any
             * element with the same address as the address of given
             * ``element``, linear in the size of the list plus linear in the
             * position of ``element`` otherwise.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> nums = {0, 2, 1, 4, 0, 3, 2};
             *
             *          py::print(nums.index(0)); // Argument is temporary reference, value 0
             *          py::print(nums.index(nums[6])); // Argument is reference to element, value 2
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *      6
             *
             * @endrst
             *
             * @param element the element whose index to return
             * @throw py::ValueError if there is no element with given
             * ``element``
             * @return index of the element
             */
            std::size_t index(const_reference element) const;

            /**
             * @brief Appends to the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::append
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-append:
             *
             * *py::builtins::list::*\ append
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      void append(const_reference element);                   // 1
             *
             *      void append(value_type&& element);                      // 2
             *
             * Appends given element to the list.
             *
             *
             * Parameters
             * ----------
             *
             * ``element``: The element to append.
             *
             *
             * Complexity
             * ----------
             *
             * Constant, plus linear in the size of the list if the insertion
             * causes the memory to be reallocated.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1};
             *
             *          py::print(list);
             *
             *          list.append(2);
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1}
             *      {0, 1, 2}
             *
             * @endrst
             *
             * @param element element to append
             */
            void append(const_reference element);

            /**
             * @brief Appends to the list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param element element to append
             */
            void append(value_type&& element);

            /**
             * @brief Inserts element at given index
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::insert
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-insert:
             *
             * *py::builtins::list::*\ insert
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      void insert(std::size_t index, const_reference element);        // 1
             *
             *      void insert(std::size_t index, value_type&& element);           // 2
             *
             *      template <class input_it>
             *      void insert(std::size_t index, input_it first, input_it last);  // 3
             *
             * 1.   Inserts given element at given index.
             *
             * 2.   Same as (1).
             *
             * 3.   Inserts given element range at given index.
             *
             *
             * Parameters
             * ----------
             *
             * ``element``: The element to insert.
             *
             * ``first``: Iterator to the first element to insert.
             *
             * ``last``: Iterator to one past the last element to insert.
             *
             *
             * Complexity
             * ----------
             *
             * 1.   Constant plus linear in the distance between ``index`` and
             *      the end if the insertion causes the memory to be
             *      reallocated, constant plus linear in the size of the list
             *      otherwise.
             *
             * 2.   Same as (1).
             *
             * 3.   Linear in the distance between ``first`` and ``last`` plus
             *      linear in the distance between ``index`` and the end.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {1, 3, 5};
             *
             *          py::print(list);
             *
             *          list.insert(0, 0);
             *          list.insert(2, 2);
             *          list.insert(-1, 4); // insert with negative index
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {1, 3, 5}
             *      {0, 1, 2, 3, 4, 5}
             *
             * @endrst
             *
             * @param index index of element
             * @param element element to insert
             */
            void insert(long index, const_reference element);

            /**
             * @brief Inserts element at given index
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param index index of element
             * @param element element to insert
             */
            void insert(long index, value_type&& element);

            /**
             * @brief Inserts given element range in given index
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @tparam input_it type of iterator
             * @param index index of new elements
             * @param first iterator to first element
             * @param last iterator to one past last element
             */
            template <class input_it>
            void insert(long index, input_it first, input_it last);

            /**
             * @brief Appends all element of another iterable to the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::extend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-extend:
             *
             * *py::builtins::list::*\ extend
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      template <class iterable_type>
             *      void extend(const iterable_type& iterable);
             *
             * Appends all element of given iterable to the list.
             *
             * .. tip::
             *
             *      Same as::
             *
             *          this->insert(-1, iterable.begin(), iterable.end());
             *
             *
             * Template parameters
             * -------------------
             *
             * ``iterable_type``: The type of iterable object.
             *
             *
             * Parameters
             * ----------
             *
             * ``iterable``: An iterable object.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of iterable plus linear in the size of the
             * list if causes reallocation, linear in the size of iterable
             * otherwise.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1};
             *
             *          py::print(list);
             *
             *          list.extend(py::list<int>(2, 3));
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1}
             *      {0, 1, 2, 3}
             *
             * @endrst
             *
             * @tparam iterable_type type of iterable
             * @param iterable another iterable whose element is going to be appended
             */
            template <class iterable_type>
            void extend(const iterable_type& iterable);

            /**
             * @brief Clears the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::clear
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-clear:
             *
             * *py::builtins::list::*\ clear
             * =============================
             *
             * .. code-block:: cpp
             *
             *      void clear();
             *
             * Clears the list.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of the list.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1};
             *          py::print(list);
             *
             *          list.clear();
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1}
             *      {}
             *
             * @endrst
             *
             */
            void clear();

            /**
             * @brief Removes the element at given index
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::pop
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-pop:
             *
             * *py::builtins::list::*\ pop
             * ===========================
             *
             * .. code-block:: cpp
             *
             *      value_type pop(long index = -1);
             *
             * Removes the element at given index (the last element by default).
             *
             *
             * Parameters
             * ----------
             *
             * ``index``: The index of the element.
             *
             *
             * Return value
             * ------------
             *
             * The removed element.
             *
             *
             * Complexity
             * ----------
             *
             * Same as the complexity of ``std::vector::erase``.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1};
             *
             *          py::print(list);
             *
             *          list.pop();
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1}
             *      {0}
             *
             * @endrst
             *
             * @param index index of element
             * @throw py::IndexError if list is empty or index is out of range
             * @return the removed element
             */
            value_type pop(long index = -1);

            /**
             * @brief Removes the element with given value
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::remove
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-remove:
             *
             * *py::builtins::list::*\ remove
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      void remove(const_reference value);
             *
             * Removes the given element.
             *
             * .. note::
             *
             *      This function removes the element at the index returned by
             *      ``this->index(value)``.
             *
             * .. tip::
             *
             *      Equivalent to: (with a difference that it doesn't return)
             *
             *      .. code-block:: cpp
             *
             *          this->pop(this->index(value));
             *
             *
             * Parameters
             * ----------
             *
             * ``value``: The element to remove.
             *
             *
             * Complexity
             * ----------
             *
             * Complexity of :ref:`py-builtins-list-index` plus complexity of
             * :ref:`py-builtins-list-pop`.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1, 3, 0};
             *
             *          py::print(list);
             *
             *          list.remove(1);
             *          list.remove(list[-1]); // Reference to element
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1, 3, 0}
             *      {0, 3}
             *
             * @endrst
             *
             * @param value value of the element to remove
             */
            void remove(const_reference value);

            /**
             * @brief Sorts the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::sort
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-sort:
             *
             * *py::builtins::list::*\ sort
             * ============================
             *
             * .. code-block:: cpp
             *
             *      template <class compare_func_t = std::function<reference(reference)>>
             *      void sort(bool reverse = false, compare_func_t key = nullptr);
             *
             * Removes the given element.
             *
             *
             * Template parameters
             * -------------------
             *
             * ``compare_func_t``: The type of compare function (``key``).
             *
             * .. admonition:: Requirements of ``compare_func_t``
             *
             *      ``compare_func_t`` must support function call operator
             *      (``operator ()``).
             *
             *
             * Parameters
             * ----------
             *
             * ``reverse``: Whether to reserve sort the list.
             *
             * ``key``: A function (can be a callable object or lambda) whose
             * return value used as the key to sort.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of the list multipled by the logarithm of the
             * size of the list, plus linear in the size of the list if reverse
             * sorted.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 1, 3, 0};
             *
             *          list.sort();
             *
             *          py::print(list);
             *
             *          list.sort(true); // Reverse
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 0, 1, 3}
             *      {3, 1, 0, 0}
             *
             * @endrst
             *
             * @tparam compare_return type returned by key
             * @param reverse whether to reverse sort the list
             * @param key function returning key for each element used to sort
             */
            template <class compare_func_t = std::function<reference(reference)>>
            void sort(bool reverse = false, compare_func_t key = nullptr);

            /**
             * @brief Reverses the list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::reverse
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-reverse:
             *
             * *py::builtins::list::*\ reverse
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      void reverse();
             *
             * Reverses the list.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of the list.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(list);
             *
             *          list.reverse(true);
             *
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 0, 1, 3}
             *      {3, 1, 0, 0}
             *
             * @endrst
             *
             */
            void reverse();

            /**
             * @brief Returns an iterator to the first element
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::begin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-begin:
             *
             * *py::builtins::list::*\ begin
             * =============================
             *
             * .. code-block:: cpp
             *
             *      iterator begin();                                       // 1
             *
             *      const_iterator begin() const;                           // 2
             *
             * 1.   Returns an iterator to the first element.
             *
             * 2.   Returns a constant iterator to the first element.
             *
             * .. tip::
             *
             *      \(2) is equivalent to :ref:`py-builtins-list-cbegin`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * 1.   An iterator to the first element.
             *
             * 2.   A constant iterator to the first element.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*list.begin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return iterator to the first element
             */
            iterator begin();

            /**
             * @brief Returns an constant iterator to the first element
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @return iterator to the first element
             */
            const_iterator begin() const;

            /**
             * @brief Returns an constant iterator to the first element
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::cbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-cbegin:
             *
             * *py::builtins::list::*\ cbegin
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      const_iterator cbegin() const;
             *
             * Returns a constant iterator to the first element.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to the first element.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*list.cbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant iterator to the first element
             */
            const_iterator cbegin() const;

            /**
             * @brief Returns a reverse iterator to first element of reversed
             * list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::rbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-rbegin:
             *
             * *py::builtins::list::*\ rbegin
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      reverse_iterator rbegin();                              // 1
             *
             *      const_reverse_iterator rbegin() const;                  // 2
             *
             * 1.   Returns a reverse iterator to the first element of the
             *      reversed list.
             *
             * 2.   Returns a constant reverse iterator to the first element of
             *      the reversed list.
             *
             * .. tip::
             *
             *      \(2) is equivalent to :ref:`py-builtins-list-crbegin`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * 1.   A reverse iterator to the first element of the reversed
             *      list.
             *
             * 2.   A constant reverse iterator to the first element of the
             *      reversed list.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*list.rbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return reverse iterator to the first element of the reversed
             * list
             */
            reverse_iterator rbegin();

            /**
             * @brief Returns a reverse iterator to first element of reversed
             * list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @return iterator to one past the last element
             */
            const_reverse_iterator rbegin() const;

            /**
             * @brief Returns a constant iterator to first element of reversed
             * list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::crbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-crbegin:
             *
             * *py::builtins::list::*\ crbegin
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      const_reverse_iterator crbegin() const;
             *
             * Returns a constant reverse iterator to first element of the
             * reversed list.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to first element of the reversed
             * list.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*list.crbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant reverse iterator to first element of reversed
             * list
             */
            const_reverse_iterator crbegin() const;

            /**
             * @brief Returns an iterator to one past the last element
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::end
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-end:
             *
             * *py::builtins::list::*\ end
             * ===========================
             *
             * .. code-block:: cpp
             *
             *      iterator end();                                         // 1
             *
             *      const_iterator end() const;                             // 2
             *
             * 1.   Returns an iterator to one past the last element.
             *
             * 2.   Returns a constant iterator one past the last element.
             *
             * .. tip::
             *
             *      \(2) is equivalent to :ref:`py-builtins-list-cend`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * 1.   An iterator to one past the last element.
             *
             * 2.   A constant iterator to one past the last element.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*(list.end() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return iterator to one past the last element
             */
            iterator end();

            /**
             * @brief Returns an constant iterator to one past the last element
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @return constant iterator to one past the last element
             */
            const_iterator end() const;

            /**
             * @brief Returns an constant iterator to one past the last element
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::cend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-cend:
             *
             * *py::builtins::list::*\ cend
             * ============================
             *
             * .. code-block:: cpp
             *
             *      const_iterator cend() const;
             *
             * Returns a constant iterator to one past the last element.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to one past the last element.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*(list.cend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant iterator to one past the last element
             */
            const_iterator cend() const;

            /**
             * @brief Returns a reverse iterator to one past the last element of
             * reversed list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::rend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-rend:
             *
             * *py::builtins::list::*\ rend
             * ============================
             *
             * .. code-block:: cpp
             *
             *      reverse_iterator rend();                                // 1
             *
             *      const_reverse_iterator rend() const;                    // 2
             *
             * 1.   Returns a reverse iterator to one past the last element of
             *      the reversed list.
             *
             * 2.   Returns a constant reverse iterator to one past the last
             *      element of the reversed list.
             *
             * .. tip::
             *
             *      \(2) is equivalent to :ref:`py-builtins-list-crend`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * 1.   An reverse iterator to one past the last element of the
             *      reversed list.
             *
             * 2.   A constant reverse iterator to one past the last element of
             *      the reversed list.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*(list.rend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return reverse iterator to one past the last element of reversed
             * list
             */
            reverse_iterator rend();

            /**
             * @brief Returns a constant reverse iterator to one past the last
             * element of reversed list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @return constant reverse iterator to one past the last element of
             * reversed list
             */
            const_reverse_iterator rend() const;

            /**
             * @brief Returns a constant reverse iterator to one past the last
             * element of reversed list
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::crend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-crend:
             *
             * *py::builtins::list::*\ crend
             * =============================
             *
             * .. code-block:: cpp
             *
             *      const_reverse_iterator crend() const;
             *
             * Returns a constant reverse iterator to one past the last element
             * of the reversed list.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to one past the last element of the
             * reversed list.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(*(list.crend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant reverse iterator to one past the last element of
             * reversed list
             */
            const_reverse_iterator crend() const;

            /**
             * @brief Returns element at given index
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::operator []
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-operator-subscirpt:
             *
             * *py::builtins::list::*\ operator []
             * ===================================
             *
             * .. code-block:: cpp
             *
             *      reference operator [] (long index);                             // 1
             *
             *      const_reference operator [] (long index) const;                 // 2
             *
             *      list operator [] (std::string slice_properties);                // 3
             *
             *      const list operator [] (std::string slice_properties) const;    // 4
             *
             * 1.   Returns reference to the element at given index.
             *
             * 2.   Returns constant reference to the element at given index.
             *
             * 3.   Returns a slice of the list with given start, stop and step.
             *
             * 4.   Returns a constant slice of the list with given start, stop
             *      and step.
             *
             * .. note::
             *
             *      Objects returned by (3) and (4) are slice and is not
             *      copyable. If copied, the copied object will copy all
             *      elements by value and will lose the special properties of
             *      slice. Use rvalues instead, i.e ``list<...>&&`` and
             *      ``const list<...>&&``.
             *
             *
             * Parameters
             * ----------
             *
             * ``index``: The index of the element.
             *
             * ``slice_properties``: The properties of slice.
             *
             * .. admonition:: Format of argument ``slice_properties``
             *
             *      The value of ``slice_properties`` must have the format::
             *
             *          "[start]:[stop][:step]"
             *
             *      Examples of valid ``slice_properties`` values::
             *
             *          "7:9" // Omitted step, automatically determined
             *          "2::2" // Omitted stop, automatically determined
             *          "2:3:4" // Specified everything
             *          "::3" // Omitted both start and stop, automatically determined
             *          ":" // Omitted everything, automatically determined
             *
             *      Examples of invalid ``slice_properties`` values::
             *
             *          "" // Error: empty string
             *          "4" // Error: string must contain a semicolor
             *          "+-4:" // Error: invalid number
             *          "long():" // Error: expressions not allowed, include the value
             *
             *
             * Return value
             * ------------
             *
             * 1.   Reference to the element at given index.
             *
             * 2.   Constant reference to the element at given index.
             *
             * 3.   A slice of the list with given start, stop and step.
             *
             * 4.   A constant slice of the list with given start, stop and
             *      step.
             *
             *
             * Complexity
             * ----------
             *
             * 1.   Constant.
             *
             * 2.   Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list = {0, 0, 1, 3};
             *          py::print(list[0]);
             *          py::print(list["1:3"]); // Slice
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *      {0, 1}
             *
             * @endrst
             *
             * @attr
             *
             *      toctree_label operator []
             *      member_tree_label operator []
             *
             * @endattr
             *
             * @param index index of element
             * @return reference to element
             */
            reference operator [] (long index);

            /**
             * @brief Returns element at given index
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param index index of element
             * @return constant reference to element
             */
            const_reference operator [] (long index) const;

            /**
             * @brief Returns a slice of the list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param properties properties of slice
             * @return slice of list
             */
            list operator [] (std::string properties);

            /**
             * @brief Returns a slice of the list
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param properties properties of slice
             * @return slice of list
             */
            const list operator [] (std::string properties) const;

            /**
             * @brief Assigns another list or slice to the list or slice
             *
             * @details @rst
             *
             * .. index:: py::builtins::list::operator =
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-list-operator-assign:
             *
             * *py::builtins::list::*\ operator =
             * ==================================
             *
             * .. code-block:: cpp
             *
             *      list& operator = (const list& other);
             *
             * Assigns another list or slice to the list.
             *
             *
             * Parameters
             * ----------
             *
             * ``other``: Another list or slice.
             *
             *
             * Return value
             * ------------
             *
             * ``*this``.
             *
             *
             * Complexity
             * ----------
             *
             * Linear in the size of ``other``.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::list<int> list{0, 9, 1, 3};
             *          list = {5}
             *          py::print(list);
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {5}
             *
             * @endrst
             *
             * @attr
             *
             *      toctree_label operator =
             *      member_tree_label operator =
             *
             * @endattr
             *
             * @param other another list (can be a slice)
             * @return the list
             */
            list& operator = (const list& other);
        };

        /**
         * @brief Checks whether a list is less than another list
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator < [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-less_than-list:
         *
         * *py::builtins::*\ operator < (:ref:`py::builtins::list <py-builtins-list>`)
         * ===========================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator < (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Checks whether a list is less than another list.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is less than ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 1, 3};
         *          py::list<int> rhs{0, 9, 1, 4};
         *
         *          py::print(lhs < rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator < (py::builtins::list)
         *      member_tree_label operator \\< (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is less than ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator < (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);

        /**
         * @brief Checks whether a list is more than another list
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator > [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-more_than-list:
         *
         * *py::builtins::*\ operator > (:ref:`py::builtins::list <py-builtins-list>`)
         * ===========================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator > (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Checks whether a list is more than another list.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is more than ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 1, 6};
         *          py::list<int> rhs{0, 9, 1, 4};
         *
         *          py::print(lhs > rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator > (py::builtins::list)
         *      member_tree_label operator > (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is more than ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator > (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);

        /**
         * @brief Compares two lists for equality
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator == [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-equals-list:
         *
         * *py::builtins::*\ operator == (:ref:`py::builtins::list <py-builtins-list>`)
         * ============================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator == (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Compares two lists for equality.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 2, 3};
         *          py::list<int> rhs{0, 9, 2, 3};
         *
         *          py::print(lhs == rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator == (py::builtins::list)
         *      member_tree_label operator == (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is equal to ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator == (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);

        /**
         * @brief Compares two lists for inequality
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator != [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-not_equals-list:
         *
         * *py::builtins::*\ operator != (:ref:`py::builtins::list <py-builtins-list>`)
         * ============================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator != (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Compares two lists for inequality.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is not equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 2, 3};
         *          py::list<int> rhs{0, 9, 2, 3};
         *
         *          py::print(lhs != rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      false
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator != (py::builtins::list)
         *      member_tree_label operator != (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is not equal to ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator != (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);

        /**
         * @brief Checks whether a list is less than or equal to another list
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator <= [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-less_than_or_equal-list:
         *
         * *py::builtins::*\ operator <= (:ref:`py::builtins::list <py-builtins-list>`)
         * ============================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator <= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Checks whether a list is less than or equal to another list.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is less than or equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 1, 3};
         *          py::list<int> rhs{0, 9, 1, 3};
         *
         *          py::print(lhs <= rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator <= (py::builtins::list)
         *      member_tree_label operator \\<= (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is less than or equal to ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator <= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);

        /**
         * @brief Checks whether a list is more than or equal to another list
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator >= [py::builtins::list]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-more_than_or_equal-list:
         *
         * *py::builtins::*\ operator >= (:ref:`py::builtins::list <py-builtins-list>`)
         * ============================================================================
         *
         * *Defined in* ``<pypp/builtins/list.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type, class allocator_a, class allocator_b>
         *      bool operator >= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
         *
         * Checks whether a list is more than or equal to another list.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A list.
         *
         * ``rhs``: Another list.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is more than or equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::list<int> lhs{0, 9, 1, 6};
         *          py::list<int> rhs{0, 9, 1, 6};
         *
         *          py::print(lhs >= rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator >= (py::builtins::list)
         *      member_tree_label operator >= (py::builtins::list)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @tparam allocator_a allocator type of first list
         * @tparam allocator_b allocator type of second list
         * @param lhs first list
         * @param rhs second list
         * @return whether ``lhs`` is more than or equal to ``rhs``
         */
        template <class type, class allocator_a, class allocator_b>
        bool operator >= (const list<type, allocator_a>& lhs, const list<type, allocator_b>& rhs);
    }
}

#include <pypp/builtins/list.tcc>

#endif
