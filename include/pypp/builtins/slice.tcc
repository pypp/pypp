/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_SLICE_TCC
#define PYPP_BUILTINS_SLICE_TCC

#include <pypp/builtins/exceptions.hpp>
#include <pypp/builtins/raise.hpp>

#include <string>

namespace py
{
    inline namespace builtins
    {
        namespace internal__
        {

            /**
             * @brief Structure to hold properties of a slice
             *
             */
            struct slice
            {
                long start = 0;
                long stop = -1;
                long step = 1;

                static slice from_str(std::string properties);
            };
        }
    }
}

#endif
