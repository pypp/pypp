/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_EXCEPTION_HPP
#define PYPP_BUILTINS_EXCEPTION_HPP

#include <exception>
#include <string>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Base class for all exception thrown by PyPP
         *
         * @details @rst
         *
         * .. index:: py::builtins::BaseException
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-BaseException:
         *
         * *py::builtins::*\ BaseException
         * ===============================
         *
         * *Defined in* ``<pypp/builtins/exceptions.hpp>``
         *
         * .. code-block:: cpp
         *
         *      class BaseException;
         *
         * ``py::builtins::BaseException`` provided a consistant interface for
         * handling exceptions. All exceptions raised (thrown) by PyPP inherit
         * from this class.
         *
         * .. graphviz::
         *      :alt: Inheritance graph
         *
         *      [[[writer_eval: textwrap.indent(inheritance_graph, " ")]]]
         *
         *
         * Members
         * -------
         *
         * [[[writer_eval: rst_member_tree(node)]]]
         *
         * .. toctree::
         *      :hidden:
         *
         *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins.hpp>
         *
         *      int main()
         *      {
         *          try
         *          {
         *              py::raise(py::BaseException("example exception"));
         *          }
         *          catch (py::BaseException& exception)
         *          {
         *              py::print("exception handled successfully:", exception);
         *          }
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      exception handled successfully: example exception
         *
         * @endrst
         *
         */
        class BaseException : public std::exception
        {
        private:

            /**
             * @brief Explanatory message about the exception
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             */
            std::string what_msg;

        public:

            /**
             * @brief Constructs a new BaseException
             *
             * @details @rst
             *
             * .. index:: py::builtins::BaseException::BaseException
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-BaseException-BaseException:
             *
             * *py::builtins::BaseException::*\ BaseException
             * ==============================================
             *
             * .. code-block:: cpp
             *
             *      BaseException();                                        // 1
             *
             *      explicit BaseException(const char* what);               // 2
             *
             *      BaseException(const std::string& what);                 // 3
             *
             *      template <class... args>
             *      BaseException(const args&... what)                      // 4
             *
             *      BaseException(const BaseException& other);              // 5
             *
             *      BaseException(BaseException&& other);                   // 6
             *
             * 1.   Constructs a BaseException with empty explanatory message.
             *
             * 2.   Constructs a BaseException with given explanatory message.
             *
             * 3.   Same as 2.
             *
             * 4.   Constructs a BaseException with given explanatory values.
             *
             * 5.   Constructs a BaseException from another BaseException.
             *
             * 6.   Same as 5.
             *
             * .. note::
             *
             *      (1), (2) and (3) are deprecated and will be removed when
             *      ``py::builtins::any_t`` is implemented. But changes should
             *      not break old codes, as after removal (4) will be candidate
             *      for all calls to them.
             *
             *
             * Template parameters
             * -------------------
             *
             * ``args``: The types of explanatory values.
             *
             *
             * Parameters
             * ----------
             *
             * ``what``: (1), (2) and (3): explanatory string, (4): explanatory
             * values.
             *
             * ``other``: Another instance of BaseException.
             *
             * @endrst
             *
             */
            BaseException();

            /**
             * @brief Constructs a new BaseException an explanatory message
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @param what explanatory string
             */
            explicit BaseException(const char* what);

            /**
             * @brief Constructs a new BaseException an explanatory message
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @param what explanatory string
             */
            BaseException(const std::string& what);

            /**
             * @brief Constructs a new Exception with an explanatory tuple
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @tparam args argument types
             * @param what explanatory values
             */
            template <class... args>
            BaseException(const args&... what)
                : what_msg("(" + (std::string(what) + ", " + ...) + ")")
            {
                what_msg.erase(what_msg.end() - 3, what_msg.end() - 1);
            }

            /**
             * @brief Copy constructs a new BaseException
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @param other another instance of BaseException or derived class
             */
            BaseException(const BaseException& other);

            /**
             * @brief Move constructs a new BaseException
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @param other another instance of BaseException or derived class
             */
            BaseException(BaseException&& other);

            /**
             * @brief Destroys the BaseException
             *
             * @details @rst
             *
             * .. index:: py::builtins::BaseException::~BaseException
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-BaseException--BaseException:
             *
             * *py::builtins::BaseException::*\ ~BaseException
             * ===============================================
             *
             * .. code-block:: cpp
             *
             *      virtual ~BaseException();
             *
             * Destoryes the BaseException object.
             *
             * @endrst
             *
             */
            virtual ~BaseException();

            /**
             * @brief Returns the explanatory message about the exception
             *
             * @details @rst
             *
             * .. index:: py::builtins::BaseException::what
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-BaseException-what:
             *
             * *py::builtins::BaseException::*\ what
             * =====================================
             *
             * .. code-block:: cpp
             *
             *      virtual const char* what() const noexcept override;     // 1
             *
             * Returns the explanatory string.
             *
             *
             * Return value
             * ------------
             *
             * The explanatory string.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins.hpp>
             *
             *      int main()
             *      {
             *          try
             *          {
             *              py::raise(py::BaseException("example exception"));
             *          }
             *          catch (py::BaseException& exception)
             *          {
             *              py::print("exception handled successfully:", exception.what());
             *          }
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      exception handled successfully: example exception
             *
             * @endrst
             *
             */
            virtual const char* what() const noexcept override;

            /**
             * @brief Assigns another BaseException to the BaseException
             *
             * @details @rst
             *
             * .. index:: py::builtins::BaseException::operator =
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-BaseException-operator-assign:
             *
             * *py::builtins::BaseException::*\ operator =
             * ===========================================
             *
             * .. code-block:: cpp
             *
             *      BaseException& operator = (const BaseException& other); // 1
             *
             *      BaseException& operator = (BaseException&& other);      // 2
             *
             * Assigns another BaseException object to the object.
             *
             *
             * Parameters
             * ----------
             *
             * ``other``: Another instance of BaseException.
             *
             *
             * Return value
             * ------------
             *
             * ``*this``.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins.hpp>
             *
             *      int main()
             *      {
             *          try
             *          {
             *              py::BaseException exception;
             *              exception = py::BaseException("example exception")
             *              py::raise(exception);
             *          }
             *          catch (py::BaseException& exception)
             *          {
             *              py::print("exception handled successfully:", exception);
             *          }
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      exception handled successfully: example exception
             *
             * @endrst
             *
             * @attr
             *
             *      toctree_label operator =
             *      member_tree_label operator =
             *
             * @endattr
             *
             * @param other another instance of BaseException or derived class
             */
            BaseException& operator = (const BaseException& other);

            /**
             * @brief Move assigns another BaseException to the BaseException
             *
             * @details @attr
             *
             *      generate false
             *
             * @endrst
             *
             * @param other another instance of BaseException or derived class
             */
            BaseException& operator = (BaseException&& other);
        };

        class Exception : public BaseException
        {
        public:

            /**
            * @brief Constructs a new Exception
            *
            */
            Exception();

            /**
            * @brief Constructs a new Exception with an explanatory message
            *
            * @param what explanatory string
            */
            explicit Exception(const char* what);

            /**
            * @brief Constructs a new Exception with an explanatory message
            *
            * @param what explanatory string
            */
            Exception(const std::string& what);

            /**
            * @brief Constructs a new Exception with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            Exception(const args&... what)
                : BaseException(what...)
            {}

            /**
            * @brief Destroys the Exception
            *
            */
           virtual ~Exception();
        };

        class ArithmeticError : public Exception
        {
        public:

            /**
            * @brief Constructs a new ArithmeticError
            *
            */
            ArithmeticError();

            /**
            * @brief Constructs a new ArithmeticError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ArithmeticError(const char* what);

            /**
            * @brief Constructs a new ArithmeticError with an explanatory message
            *
            * @param what explanatory string
            */
            ArithmeticError(const std::string& what);

            /**
            * @brief Constructs a new ArithmeticError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ArithmeticError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the ArithmeticError
            *
            */
           virtual ~ArithmeticError();
        };

        class FloatingPointError : public ArithmeticError
        {
        public:

            /**
            * @brief Constructs a new FloatingPointError
            *
            */
            FloatingPointError();

            /**
            * @brief Constructs a new FloatingPointError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit FloatingPointError(const char* what);

            /**
            * @brief Constructs a new FloatingPointError with an explanatory message
            *
            * @param what explanatory string
            */
            FloatingPointError(const std::string& what);

            /**
            * @brief Constructs a new FloatingPointError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            FloatingPointError(const args&... what)
                : ArithmeticError(what...)
            {}

            /**
            * @brief Destroys the FloatingPointError
            *
            */
           virtual ~FloatingPointError();
        };

        class OverflowError : public ArithmeticError
        {
        public:

            /**
            * @brief Constructs a new OverflowError
            *
            */
            OverflowError();

            /**
            * @brief Constructs a new OverflowError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit OverflowError(const char* what);

            /**
            * @brief Constructs a new OverflowError with an explanatory message
            *
            * @param what explanatory string
            */
            OverflowError(const std::string& what);

            /**
            * @brief Constructs a new OverflowError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            OverflowError(const args&... what)
                : ArithmeticError(what...)
            {}

            /**
            * @brief Destroys the OverflowError
            *
            */
           virtual ~OverflowError();
        };

        class ZeroDivisionError : public ArithmeticError
        {
        public:

            /**
            * @brief Constructs a new ZeroDivisionError
            *
            */
            ZeroDivisionError();

            /**
            * @brief Constructs a new ZeroDivisionError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ZeroDivisionError(const char* what);

            /**
            * @brief Constructs a new ZeroDivisionError with an explanatory message
            *
            * @param what explanatory string
            */
            ZeroDivisionError(const std::string& what);

            /**
            * @brief Constructs a new ZeroDivisionError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ZeroDivisionError(const args&... what)
                : ArithmeticError(what...)
            {}

            /**
            * @brief Destroys the ZeroDivisionError
            *
            */
           virtual ~ZeroDivisionError();
        };

        class AssertionError : public Exception
        {
        public:

            /**
            * @brief Constructs a new AssertionError
            *
            */
            AssertionError();

            /**
            * @brief Constructs a new AssertionError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit AssertionError(const char* what);

            /**
            * @brief Constructs a new AssertionError with an explanatory message
            *
            * @param what explanatory string
            */
            AssertionError(const std::string& what);

            /**
            * @brief Constructs a new AssertionError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            AssertionError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the AssertionError
            *
            */
           virtual ~AssertionError();
        };

        class AttributeError : public Exception
        {
        public:

            /**
            * @brief Constructs a new AttributeError
            *
            */
            AttributeError();

            /**
            * @brief Constructs a new AttributeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit AttributeError(const char* what);

            /**
            * @brief Constructs a new AttributeError with an explanatory message
            *
            * @param what explanatory string
            */
            AttributeError(const std::string& what);

            /**
            * @brief Constructs a new AttributeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            AttributeError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the AttributeError
            *
            */
           virtual ~AttributeError();
        };

        class BufferError : public Exception
        {
        public:

            /**
            * @brief Constructs a new BufferError
            *
            */
            BufferError();

            /**
            * @brief Constructs a new BufferError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit BufferError(const char* what);

            /**
            * @brief Constructs a new BufferError with an explanatory message
            *
            * @param what explanatory string
            */
            BufferError(const std::string& what);

            /**
            * @brief Constructs a new BufferError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            BufferError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the BufferError
            *
            */
           virtual ~BufferError();
        };

        class EOFError : public Exception
        {
        public:

            /**
            * @brief Constructs a new EOFError
            *
            */
            EOFError();

            /**
            * @brief Constructs a new EOFError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit EOFError(const char* what);

            /**
            * @brief Constructs a new EOFError with an explanatory message
            *
            * @param what explanatory string
            */
            EOFError(const std::string& what);

            /**
            * @brief Constructs a new EOFError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            EOFError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the EOFError
            *
            */
           virtual ~EOFError();
        };

        class ImportError : public Exception
        {
        public:

            /**
            * @brief Constructs a new ImportError
            *
            */
            ImportError();

            /**
            * @brief Constructs a new ImportError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ImportError(const char* what);

            /**
            * @brief Constructs a new ImportError with an explanatory message
            *
            * @param what explanatory string
            */
            ImportError(const std::string& what);

            /**
            * @brief Constructs a new ImportError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ImportError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the ImportError
            *
            */
           virtual ~ImportError();
        };

        class ModuleNotFoundError : public ImportError
        {
        public:

            /**
            * @brief Constructs a new ModuleNotFoundError
            *
            */
            ModuleNotFoundError();

            /**
            * @brief Constructs a new ModuleNotFoundError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ModuleNotFoundError(const char* what);

            /**
            * @brief Constructs a new ModuleNotFoundError with an explanatory message
            *
            * @param what explanatory string
            */
            ModuleNotFoundError(const std::string& what);

            /**
            * @brief Constructs a new ModuleNotFoundError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ModuleNotFoundError(const args&... what)
                : ImportError(what...)
            {}

            /**
            * @brief Destroys the ModuleNotFoundError
            *
            */
           virtual ~ModuleNotFoundError();
        };

        class LookupError : public Exception
        {
        public:

            /**
            * @brief Constructs a new LookupError
            *
            */
            LookupError();

            /**
            * @brief Constructs a new LookupError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit LookupError(const char* what);

            /**
            * @brief Constructs a new LookupError with an explanatory message
            *
            * @param what explanatory string
            */
            LookupError(const std::string& what);

            /**
            * @brief Constructs a new LookupError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            LookupError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the LookupError
            *
            */
           virtual ~LookupError();
        };

        class IndexError : public LookupError
        {
        public:

            /**
            * @brief Constructs a new IndexError
            *
            */
            IndexError();

            /**
            * @brief Constructs a new IndexError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit IndexError(const char* what);

            /**
            * @brief Constructs a new IndexError with an explanatory message
            *
            * @param what explanatory string
            */
            IndexError(const std::string& what);

            /**
            * @brief Constructs a new IndexError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            IndexError(const args&... what)
                : LookupError(what...)
            {}

            /**
            * @brief Destroys the IndexError
            *
            */
           virtual ~IndexError();
        };

        class KeyError : public LookupError
        {
        public:

            /**
            * @brief Constructs a new KeyError
            *
            */
            KeyError();

            /**
            * @brief Constructs a new KeyError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit KeyError(const char* what);

            /**
            * @brief Constructs a new KeyError with an explanatory message
            *
            * @param what explanatory string
            */
            KeyError(const std::string& what);

            /**
            * @brief Constructs a new KeyError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            KeyError(const args&... what)
                : LookupError(what...)
            {}

            /**
            * @brief Destroys the KeyError
            *
            */
           virtual ~KeyError();
        };

        class MemoryError : public Exception
        {
        public:

            /**
            * @brief Constructs a new MemoryError
            *
            */
            MemoryError();

            /**
            * @brief Constructs a new MemoryError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit MemoryError(const char* what);

            /**
            * @brief Constructs a new MemoryError with an explanatory message
            *
            * @param what explanatory string
            */
            MemoryError(const std::string& what);

            /**
            * @brief Constructs a new MemoryError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            MemoryError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the MemoryError
            *
            */
           virtual ~MemoryError();
        };

        class NameError : public Exception
        {
        public:

            /**
            * @brief Constructs a new NameError
            *
            */
            NameError();

            /**
            * @brief Constructs a new NameError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit NameError(const char* what);

            /**
            * @brief Constructs a new NameError with an explanatory message
            *
            * @param what explanatory string
            */
            NameError(const std::string& what);

            /**
            * @brief Constructs a new NameError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            NameError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the NameError
            *
            */
           virtual ~NameError();
        };

        class UnboundLocalError : public NameError
        {
        public:

            /**
            * @brief Constructs a new UnboundLocalError
            *
            */
            UnboundLocalError();

            /**
            * @brief Constructs a new UnboundLocalError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnboundLocalError(const char* what);

            /**
            * @brief Constructs a new UnboundLocalError with an explanatory message
            *
            * @param what explanatory string
            */
            UnboundLocalError(const std::string& what);

            /**
            * @brief Constructs a new UnboundLocalError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnboundLocalError(const args&... what)
                : NameError(what...)
            {}

            /**
            * @brief Destroys the UnboundLocalError
            *
            */
           virtual ~UnboundLocalError();
        };

        class OSError : public Exception
        {
        public:

            /**
            * @brief Constructs a new OSError
            *
            */
            OSError();

            /**
            * @brief Constructs a new OSError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit OSError(const char* what);

            /**
            * @brief Constructs a new OSError with an explanatory message
            *
            * @param what explanatory string
            */
            OSError(const std::string& what);

            /**
            * @brief Constructs a new OSError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            OSError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the OSError
            *
            */
           virtual ~OSError();
        };

        class BlockingIOError : public OSError
        {
        public:

            /**
            * @brief Constructs a new BlockingIOError
            *
            */
            BlockingIOError();

            /**
            * @brief Constructs a new BlockingIOError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit BlockingIOError(const char* what);

            /**
            * @brief Constructs a new BlockingIOError with an explanatory message
            *
            * @param what explanatory string
            */
            BlockingIOError(const std::string& what);

            /**
            * @brief Constructs a new BlockingIOError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            BlockingIOError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the BlockingIOError
            *
            */
           virtual ~BlockingIOError();
        };

        class ChildProcessError : public OSError
        {
        public:

            /**
            * @brief Constructs a new ChildProcessError
            *
            */
            ChildProcessError();

            /**
            * @brief Constructs a new ChildProcessError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ChildProcessError(const char* what);

            /**
            * @brief Constructs a new ChildProcessError with an explanatory message
            *
            * @param what explanatory string
            */
            ChildProcessError(const std::string& what);

            /**
            * @brief Constructs a new ChildProcessError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ChildProcessError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the ChildProcessError
            *
            */
           virtual ~ChildProcessError();
        };

        class ConnectionError : public OSError
        {
        public:

            /**
            * @brief Constructs a new ConnectionError
            *
            */
            ConnectionError();

            /**
            * @brief Constructs a new ConnectionError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ConnectionError(const char* what);

            /**
            * @brief Constructs a new ConnectionError with an explanatory message
            *
            * @param what explanatory string
            */
            ConnectionError(const std::string& what);

            /**
            * @brief Constructs a new ConnectionError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ConnectionError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the ConnectionError
            *
            */
           virtual ~ConnectionError();
        };

        class BrokenPipeError : public ConnectionError
        {
        public:

            /**
            * @brief Constructs a new BrokenPipeError
            *
            */
            BrokenPipeError();

            /**
            * @brief Constructs a new BrokenPipeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit BrokenPipeError(const char* what);

            /**
            * @brief Constructs a new BrokenPipeError with an explanatory message
            *
            * @param what explanatory string
            */
            BrokenPipeError(const std::string& what);

            /**
            * @brief Constructs a new BrokenPipeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            BrokenPipeError(const args&... what)
                : ConnectionError(what...)
            {}

            /**
            * @brief Destroys the BrokenPipeError
            *
            */
           virtual ~BrokenPipeError();
        };

        class ConnectionAbortedError : public ConnectionError
        {
        public:

            /**
            * @brief Constructs a new ConnectionAbortedError
            *
            */
            ConnectionAbortedError();

            /**
            * @brief Constructs a new ConnectionAbortedError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ConnectionAbortedError(const char* what);

            /**
            * @brief Constructs a new ConnectionAbortedError with an explanatory message
            *
            * @param what explanatory string
            */
            ConnectionAbortedError(const std::string& what);

            /**
            * @brief Constructs a new ConnectionAbortedError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ConnectionAbortedError(const args&... what)
                : ConnectionError(what...)
            {}

            /**
            * @brief Destroys the ConnectionAbortedError
            *
            */
           virtual ~ConnectionAbortedError();
        };

        class ConnectionRefusedError : public ConnectionError
        {
        public:

            /**
            * @brief Constructs a new ConnectionRefusedError
            *
            */
            ConnectionRefusedError();

            /**
            * @brief Constructs a new ConnectionRefusedError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ConnectionRefusedError(const char* what);

            /**
            * @brief Constructs a new ConnectionRefusedError with an explanatory message
            *
            * @param what explanatory string
            */
            ConnectionRefusedError(const std::string& what);

            /**
            * @brief Constructs a new ConnectionRefusedError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ConnectionRefusedError(const args&... what)
                : ConnectionError(what...)
            {}

            /**
            * @brief Destroys the ConnectionRefusedError
            *
            */
           virtual ~ConnectionRefusedError();
        };

        class ConnectionResetError : public ConnectionError
        {
        public:

            /**
            * @brief Constructs a new ConnectionResetError
            *
            */
            ConnectionResetError();

            /**
            * @brief Constructs a new ConnectionResetError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ConnectionResetError(const char* what);

            /**
            * @brief Constructs a new ConnectionResetError with an explanatory message
            *
            * @param what explanatory string
            */
            ConnectionResetError(const std::string& what);

            /**
            * @brief Constructs a new ConnectionResetError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ConnectionResetError(const args&... what)
                : ConnectionError(what...)
            {}

            /**
            * @brief Destroys the ConnectionResetError
            *
            */
           virtual ~ConnectionResetError();
        };

        class FileExistsError : public OSError
        {
        public:

            /**
            * @brief Constructs a new FileExistsError
            *
            */
            FileExistsError();

            /**
            * @brief Constructs a new FileExistsError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit FileExistsError(const char* what);

            /**
            * @brief Constructs a new FileExistsError with an explanatory message
            *
            * @param what explanatory string
            */
            FileExistsError(const std::string& what);

            /**
            * @brief Constructs a new FileExistsError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            FileExistsError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the FileExistsError
            *
            */
           virtual ~FileExistsError();
        };

        class FileNotFoundError : public OSError
        {
        public:

            /**
            * @brief Constructs a new FileNotFoundError
            *
            */
            FileNotFoundError();

            /**
            * @brief Constructs a new FileNotFoundError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit FileNotFoundError(const char* what);

            /**
            * @brief Constructs a new FileNotFoundError with an explanatory message
            *
            * @param what explanatory string
            */
            FileNotFoundError(const std::string& what);

            /**
            * @brief Constructs a new FileNotFoundError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            FileNotFoundError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the FileNotFoundError
            *
            */
           virtual ~FileNotFoundError();
        };

        class InterruptedError : public OSError
        {
        public:

            /**
            * @brief Constructs a new InterruptedError
            *
            */
            InterruptedError();

            /**
            * @brief Constructs a new InterruptedError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit InterruptedError(const char* what);

            /**
            * @brief Constructs a new InterruptedError with an explanatory message
            *
            * @param what explanatory string
            */
            InterruptedError(const std::string& what);

            /**
            * @brief Constructs a new InterruptedError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            InterruptedError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the InterruptedError
            *
            */
           virtual ~InterruptedError();
        };

        class IsADirectoryError : public OSError
        {
        public:

            /**
            * @brief Constructs a new IsADirectoryError
            *
            */
            IsADirectoryError();

            /**
            * @brief Constructs a new IsADirectoryError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit IsADirectoryError(const char* what);

            /**
            * @brief Constructs a new IsADirectoryError with an explanatory message
            *
            * @param what explanatory string
            */
            IsADirectoryError(const std::string& what);

            /**
            * @brief Constructs a new IsADirectoryError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            IsADirectoryError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the IsADirectoryError
            *
            */
           virtual ~IsADirectoryError();
        };

        class NotADirectoryError : public OSError
        {
        public:

            /**
            * @brief Constructs a new NotADirectoryError
            *
            */
            NotADirectoryError();

            /**
            * @brief Constructs a new NotADirectoryError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit NotADirectoryError(const char* what);

            /**
            * @brief Constructs a new NotADirectoryError with an explanatory message
            *
            * @param what explanatory string
            */
            NotADirectoryError(const std::string& what);

            /**
            * @brief Constructs a new NotADirectoryError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            NotADirectoryError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the NotADirectoryError
            *
            */
           virtual ~NotADirectoryError();
        };

        class PermissionError : public OSError
        {
        public:

            /**
            * @brief Constructs a new PermissionError
            *
            */
            PermissionError();

            /**
            * @brief Constructs a new PermissionError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit PermissionError(const char* what);

            /**
            * @brief Constructs a new PermissionError with an explanatory message
            *
            * @param what explanatory string
            */
            PermissionError(const std::string& what);

            /**
            * @brief Constructs a new PermissionError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            PermissionError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the PermissionError
            *
            */
           virtual ~PermissionError();
        };

        class ProcessLookupError : public OSError
        {
        public:

            /**
            * @brief Constructs a new ProcessLookupError
            *
            */
            ProcessLookupError();

            /**
            * @brief Constructs a new ProcessLookupError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ProcessLookupError(const char* what);

            /**
            * @brief Constructs a new ProcessLookupError with an explanatory message
            *
            * @param what explanatory string
            */
            ProcessLookupError(const std::string& what);

            /**
            * @brief Constructs a new ProcessLookupError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ProcessLookupError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the ProcessLookupError
            *
            */
           virtual ~ProcessLookupError();
        };

        class TimeoutError : public OSError
        {
        public:

            /**
            * @brief Constructs a new TimeoutError
            *
            */
            TimeoutError();

            /**
            * @brief Constructs a new TimeoutError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit TimeoutError(const char* what);

            /**
            * @brief Constructs a new TimeoutError with an explanatory message
            *
            * @param what explanatory string
            */
            TimeoutError(const std::string& what);

            /**
            * @brief Constructs a new TimeoutError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            TimeoutError(const args&... what)
                : OSError(what...)
            {}

            /**
            * @brief Destroys the TimeoutError
            *
            */
           virtual ~TimeoutError();
        };

        class ReferenceError : public Exception
        {
        public:

            /**
            * @brief Constructs a new ReferenceError
            *
            */
            ReferenceError();

            /**
            * @brief Constructs a new ReferenceError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ReferenceError(const char* what);

            /**
            * @brief Constructs a new ReferenceError with an explanatory message
            *
            * @param what explanatory string
            */
            ReferenceError(const std::string& what);

            /**
            * @brief Constructs a new ReferenceError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ReferenceError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the ReferenceError
            *
            */
           virtual ~ReferenceError();
        };

        class RuntimeError : public Exception
        {
        public:

            /**
            * @brief Constructs a new RuntimeError
            *
            */
            RuntimeError();

            /**
            * @brief Constructs a new RuntimeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit RuntimeError(const char* what);

            /**
            * @brief Constructs a new RuntimeError with an explanatory message
            *
            * @param what explanatory string
            */
            RuntimeError(const std::string& what);

            /**
            * @brief Constructs a new RuntimeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            RuntimeError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the RuntimeError
            *
            */
           virtual ~RuntimeError();
        };

        class NotImplementedError : public RuntimeError
        {
        public:

            /**
            * @brief Constructs a new NotImplementedError
            *
            */
            NotImplementedError();

            /**
            * @brief Constructs a new NotImplementedError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit NotImplementedError(const char* what);

            /**
            * @brief Constructs a new NotImplementedError with an explanatory message
            *
            * @param what explanatory string
            */
            NotImplementedError(const std::string& what);

            /**
            * @brief Constructs a new NotImplementedError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            NotImplementedError(const args&... what)
                : RuntimeError(what...)
            {}

            /**
            * @brief Destroys the NotImplementedError
            *
            */
           virtual ~NotImplementedError();
        };

        class RecursionError : public RuntimeError
        {
        public:

            /**
            * @brief Constructs a new RecursionError
            *
            */
            RecursionError();

            /**
            * @brief Constructs a new RecursionError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit RecursionError(const char* what);

            /**
            * @brief Constructs a new RecursionError with an explanatory message
            *
            * @param what explanatory string
            */
            RecursionError(const std::string& what);

            /**
            * @brief Constructs a new RecursionError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            RecursionError(const args&... what)
                : RuntimeError(what...)
            {}

            /**
            * @brief Destroys the RecursionError
            *
            */
           virtual ~RecursionError();
        };

        class StopIteration : public Exception
        {
        public:

            /**
            * @brief Constructs a new StopIteration
            *
            */
            StopIteration();

            /**
            * @brief Constructs a new StopIteration with an explanatory message
            *
            * @param what explanatory string
            */
            explicit StopIteration(const char* what);

            /**
            * @brief Constructs a new StopIteration with an explanatory message
            *
            * @param what explanatory string
            */
            StopIteration(const std::string& what);

            /**
            * @brief Constructs a new StopIteration with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            StopIteration(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the StopIteration
            *
            */
           virtual ~StopIteration();
        };

        class StopAsyncIteration : public Exception
        {
        public:

            /**
            * @brief Constructs a new StopAsyncIteration
            *
            */
            StopAsyncIteration();

            /**
            * @brief Constructs a new StopAsyncIteration with an explanatory message
            *
            * @param what explanatory string
            */
            explicit StopAsyncIteration(const char* what);

            /**
            * @brief Constructs a new StopAsyncIteration with an explanatory message
            *
            * @param what explanatory string
            */
            StopAsyncIteration(const std::string& what);

            /**
            * @brief Constructs a new StopAsyncIteration with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            StopAsyncIteration(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the StopAsyncIteration
            *
            */
           virtual ~StopAsyncIteration();
        };

        class SyntaxError : public Exception
        {
        public:

            /**
            * @brief Constructs a new SyntaxError
            *
            */
            SyntaxError();

            /**
            * @brief Constructs a new SyntaxError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit SyntaxError(const char* what);

            /**
            * @brief Constructs a new SyntaxError with an explanatory message
            *
            * @param what explanatory string
            */
            SyntaxError(const std::string& what);

            /**
            * @brief Constructs a new SyntaxError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            SyntaxError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the SyntaxError
            *
            */
           virtual ~SyntaxError();
        };

        class IndentationError : public SyntaxError
        {
        public:

            /**
            * @brief Constructs a new IndentationError
            *
            */
            IndentationError();

            /**
            * @brief Constructs a new IndentationError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit IndentationError(const char* what);

            /**
            * @brief Constructs a new IndentationError with an explanatory message
            *
            * @param what explanatory string
            */
            IndentationError(const std::string& what);

            /**
            * @brief Constructs a new IndentationError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            IndentationError(const args&... what)
                : SyntaxError(what...)
            {}

            /**
            * @brief Destroys the IndentationError
            *
            */
           virtual ~IndentationError();
        };

        class TabError : public IndentationError
        {
        public:

            /**
            * @brief Constructs a new TabError
            *
            */
            TabError();

            /**
            * @brief Constructs a new TabError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit TabError(const char* what);

            /**
            * @brief Constructs a new TabError with an explanatory message
            *
            * @param what explanatory string
            */
            TabError(const std::string& what);

            /**
            * @brief Constructs a new TabError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            TabError(const args&... what)
                : IndentationError(what...)
            {}

            /**
            * @brief Destroys the TabError
            *
            */
           virtual ~TabError();
        };

        class SystemError : public Exception
        {
        public:

            /**
            * @brief Constructs a new SystemError
            *
            */
            SystemError();

            /**
            * @brief Constructs a new SystemError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit SystemError(const char* what);

            /**
            * @brief Constructs a new SystemError with an explanatory message
            *
            * @param what explanatory string
            */
            SystemError(const std::string& what);

            /**
            * @brief Constructs a new SystemError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            SystemError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the SystemError
            *
            */
           virtual ~SystemError();
        };

        class TypeError : public Exception
        {
        public:

            /**
            * @brief Constructs a new TypeError
            *
            */
            TypeError();

            /**
            * @brief Constructs a new TypeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit TypeError(const char* what);

            /**
            * @brief Constructs a new TypeError with an explanatory message
            *
            * @param what explanatory string
            */
            TypeError(const std::string& what);

            /**
            * @brief Constructs a new TypeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            TypeError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the TypeError
            *
            */
           virtual ~TypeError();
        };

        class ValueError : public Exception
        {
        public:

            /**
            * @brief Constructs a new ValueError
            *
            */
            ValueError();

            /**
            * @brief Constructs a new ValueError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ValueError(const char* what);

            /**
            * @brief Constructs a new ValueError with an explanatory message
            *
            * @param what explanatory string
            */
            ValueError(const std::string& what);

            /**
            * @brief Constructs a new ValueError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ValueError(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the ValueError
            *
            */
           virtual ~ValueError();
        };

        class UnicodeError : public ValueError
        {
        public:

            /**
            * @brief Constructs a new UnicodeError
            *
            */
            UnicodeError();

            /**
            * @brief Constructs a new UnicodeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnicodeError(const char* what);

            /**
            * @brief Constructs a new UnicodeError with an explanatory message
            *
            * @param what explanatory string
            */
            UnicodeError(const std::string& what);

            /**
            * @brief Constructs a new UnicodeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnicodeError(const args&... what)
                : ValueError(what...)
            {}

            /**
            * @brief Destroys the UnicodeError
            *
            */
           virtual ~UnicodeError();
        };

        class UnicodeDecodeError : public UnicodeError
        {
        public:

            /**
            * @brief Constructs a new UnicodeDecodeError
            *
            */
            UnicodeDecodeError();

            /**
            * @brief Constructs a new UnicodeDecodeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnicodeDecodeError(const char* what);

            /**
            * @brief Constructs a new UnicodeDecodeError with an explanatory message
            *
            * @param what explanatory string
            */
            UnicodeDecodeError(const std::string& what);

            /**
            * @brief Constructs a new UnicodeDecodeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnicodeDecodeError(const args&... what)
                : UnicodeError(what...)
            {}

            /**
            * @brief Destroys the UnicodeDecodeError
            *
            */
           virtual ~UnicodeDecodeError();
        };

        class UnicodeEncodeError : public UnicodeError
        {
        public:

            /**
            * @brief Constructs a new UnicodeEncodeError
            *
            */
            UnicodeEncodeError();

            /**
            * @brief Constructs a new UnicodeEncodeError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnicodeEncodeError(const char* what);

            /**
            * @brief Constructs a new UnicodeEncodeError with an explanatory message
            *
            * @param what explanatory string
            */
            UnicodeEncodeError(const std::string& what);

            /**
            * @brief Constructs a new UnicodeEncodeError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnicodeEncodeError(const args&... what)
                : UnicodeError(what...)
            {}

            /**
            * @brief Destroys the UnicodeEncodeError
            *
            */
           virtual ~UnicodeEncodeError();
        };

        class UnicodeTranslateError : public UnicodeError
        {
        public:

            /**
            * @brief Constructs a new UnicodeTranslateError
            *
            */
            UnicodeTranslateError();

            /**
            * @brief Constructs a new UnicodeTranslateError with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnicodeTranslateError(const char* what);

            /**
            * @brief Constructs a new UnicodeTranslateError with an explanatory message
            *
            * @param what explanatory string
            */
            UnicodeTranslateError(const std::string& what);

            /**
            * @brief Constructs a new UnicodeTranslateError with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnicodeTranslateError(const args&... what)
                : UnicodeError(what...)
            {}

            /**
            * @brief Destroys the UnicodeTranslateError
            *
            */
           virtual ~UnicodeTranslateError();
        };

        class Warning : public Exception
        {
        public:

            /**
            * @brief Constructs a new Warning
            *
            */
            Warning();

            /**
            * @brief Constructs a new Warning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit Warning(const char* what);

            /**
            * @brief Constructs a new Warning with an explanatory message
            *
            * @param what explanatory string
            */
            Warning(const std::string& what);

            /**
            * @brief Constructs a new Warning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            Warning(const args&... what)
                : Exception(what...)
            {}

            /**
            * @brief Destroys the Warning
            *
            */
           virtual ~Warning();
        };

        class BytesWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new BytesWarning
            *
            */
            BytesWarning();

            /**
            * @brief Constructs a new BytesWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit BytesWarning(const char* what);

            /**
            * @brief Constructs a new BytesWarning with an explanatory message
            *
            * @param what explanatory string
            */
            BytesWarning(const std::string& what);

            /**
            * @brief Constructs a new BytesWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            BytesWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the BytesWarning
            *
            */
           virtual ~BytesWarning();
        };

        class DeprecationWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new DeprecationWarning
            *
            */
            DeprecationWarning();

            /**
            * @brief Constructs a new DeprecationWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit DeprecationWarning(const char* what);

            /**
            * @brief Constructs a new DeprecationWarning with an explanatory message
            *
            * @param what explanatory string
            */
            DeprecationWarning(const std::string& what);

            /**
            * @brief Constructs a new DeprecationWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            DeprecationWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the DeprecationWarning
            *
            */
           virtual ~DeprecationWarning();
        };

        class FutureWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new FutureWarning
            *
            */
            FutureWarning();

            /**
            * @brief Constructs a new FutureWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit FutureWarning(const char* what);

            /**
            * @brief Constructs a new FutureWarning with an explanatory message
            *
            * @param what explanatory string
            */
            FutureWarning(const std::string& what);

            /**
            * @brief Constructs a new FutureWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            FutureWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the FutureWarning
            *
            */
           virtual ~FutureWarning();
        };

        class ImportWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new ImportWarning
            *
            */
            ImportWarning();

            /**
            * @brief Constructs a new ImportWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ImportWarning(const char* what);

            /**
            * @brief Constructs a new ImportWarning with an explanatory message
            *
            * @param what explanatory string
            */
            ImportWarning(const std::string& what);

            /**
            * @brief Constructs a new ImportWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ImportWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the ImportWarning
            *
            */
           virtual ~ImportWarning();
        };

        class PendingDeprecationWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new PendingDeprecationWarning
            *
            */
            PendingDeprecationWarning();

            /**
            * @brief Constructs a new PendingDeprecationWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit PendingDeprecationWarning(const char* what);

            /**
            * @brief Constructs a new PendingDeprecationWarning with an explanatory message
            *
            * @param what explanatory string
            */
            PendingDeprecationWarning(const std::string& what);

            /**
            * @brief Constructs a new PendingDeprecationWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            PendingDeprecationWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the PendingDeprecationWarning
            *
            */
           virtual ~PendingDeprecationWarning();
        };

        class ResourceWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new ResourceWarning
            *
            */
            ResourceWarning();

            /**
            * @brief Constructs a new ResourceWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit ResourceWarning(const char* what);

            /**
            * @brief Constructs a new ResourceWarning with an explanatory message
            *
            * @param what explanatory string
            */
            ResourceWarning(const std::string& what);

            /**
            * @brief Constructs a new ResourceWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            ResourceWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the ResourceWarning
            *
            */
           virtual ~ResourceWarning();
        };

        class RuntimeWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new RuntimeWarning
            *
            */
            RuntimeWarning();

            /**
            * @brief Constructs a new RuntimeWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit RuntimeWarning(const char* what);

            /**
            * @brief Constructs a new RuntimeWarning with an explanatory message
            *
            * @param what explanatory string
            */
            RuntimeWarning(const std::string& what);

            /**
            * @brief Constructs a new RuntimeWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            RuntimeWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the RuntimeWarning
            *
            */
           virtual ~RuntimeWarning();
        };

        class SyntaxWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new SyntaxWarning
            *
            */
            SyntaxWarning();

            /**
            * @brief Constructs a new SyntaxWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit SyntaxWarning(const char* what);

            /**
            * @brief Constructs a new SyntaxWarning with an explanatory message
            *
            * @param what explanatory string
            */
            SyntaxWarning(const std::string& what);

            /**
            * @brief Constructs a new SyntaxWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            SyntaxWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the SyntaxWarning
            *
            */
           virtual ~SyntaxWarning();
        };

        class UnicodeWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new UnicodeWarning
            *
            */
            UnicodeWarning();

            /**
            * @brief Constructs a new UnicodeWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UnicodeWarning(const char* what);

            /**
            * @brief Constructs a new UnicodeWarning with an explanatory message
            *
            * @param what explanatory string
            */
            UnicodeWarning(const std::string& what);

            /**
            * @brief Constructs a new UnicodeWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UnicodeWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the UnicodeWarning
            *
            */
           virtual ~UnicodeWarning();
        };

        class UserWarning : public Warning
        {
        public:

            /**
            * @brief Constructs a new UserWarning
            *
            */
            UserWarning();

            /**
            * @brief Constructs a new UserWarning with an explanatory message
            *
            * @param what explanatory string
            */
            explicit UserWarning(const char* what);

            /**
            * @brief Constructs a new UserWarning with an explanatory message
            *
            * @param what explanatory string
            */
            UserWarning(const std::string& what);

            /**
            * @brief Constructs a new UserWarning with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            UserWarning(const args&... what)
                : Warning(what...)
            {}

            /**
            * @brief Destroys the UserWarning
            *
            */
           virtual ~UserWarning();
        };

        class GeneratorExit : public BaseException
        {
        public:

            /**
            * @brief Constructs a new GeneratorExit
            *
            */
            GeneratorExit();

            /**
            * @brief Constructs a new GeneratorExit with an explanatory message
            *
            * @param what explanatory string
            */
            explicit GeneratorExit(const char* what);

            /**
            * @brief Constructs a new GeneratorExit with an explanatory message
            *
            * @param what explanatory string
            */
            GeneratorExit(const std::string& what);

            /**
            * @brief Constructs a new GeneratorExit with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            GeneratorExit(const args&... what)
                : BaseException(what...)
            {}

            /**
            * @brief Destroys the GeneratorExit
            *
            */
           virtual ~GeneratorExit();
        };

        class KeyboardInterrupt : public BaseException
        {
        public:

            /**
            * @brief Constructs a new KeyboardInterrupt
            *
            */
            KeyboardInterrupt();

            /**
            * @brief Constructs a new KeyboardInterrupt with an explanatory message
            *
            * @param what explanatory string
            */
            explicit KeyboardInterrupt(const char* what);

            /**
            * @brief Constructs a new KeyboardInterrupt with an explanatory message
            *
            * @param what explanatory string
            */
            KeyboardInterrupt(const std::string& what);

            /**
            * @brief Constructs a new KeyboardInterrupt with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            KeyboardInterrupt(const args&... what)
                : BaseException(what...)
            {}

            /**
            * @brief Destroys the KeyboardInterrupt
            *
            */
           virtual ~KeyboardInterrupt();
        };

        class SystemExit : public BaseException
        {
        public:

            /**
            * @brief Constructs a new SystemExit
            *
            */
            SystemExit();

            /**
            * @brief Constructs a new SystemExit with an explanatory message
            *
            * @param what explanatory string
            */
            explicit SystemExit(const char* what);

            /**
            * @brief Constructs a new SystemExit with an explanatory message
            *
            * @param what explanatory string
            */
            SystemExit(const std::string& what);

            /**
            * @brief Constructs a new SystemExit with an explanatory tuple
            *
            * @param what explanatory values
            */
            template <class... args>
            SystemExit(const args&... what)
                : BaseException(what...)
            {}

            /**
            * @brief Destroys the SystemExit
            *
            */
           virtual ~SystemExit();
        };
    }
}

#endif
