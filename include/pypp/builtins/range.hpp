/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_RANGE_HPP
#define PYPP_BUILTINS_RANGE_HPP

#include <pypp/builtins/exceptions.hpp>
#include <pypp/builtins/raise.hpp>
#include <pypp/builtins/abs.hpp>
#include <pypp/builtins/slice.tcc>
#include <pypp/builtins/iterator.tcc>
#include <pypp/builtins/in.hpp>

#include <utility>

namespace py
{
    inline namespace builtins
    {

        /**
         * @brief Template class for holding a range of values
         *
         * @details @rst
         *
         * .. index:: py::builtins::range
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-range:
         *
         * *py::builtins::*\ range
         * =======================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      class range;
         *
         * Class for holding a range of values.
         *
         * ``py::builtins::range`` holds a range of values. The range is
         * specified by three value:
         *
         * *    :ref:`start <py-builtins-range-start>`: The start value of the
         *      range.
         *
         * *    :ref:`stop <py-builtins-range-stop>`: The end value of the
         *      range (exclusive).
         *
         * *    :ref:`step <py-builtins-range-step>`: The difference between
         *      values.
         *
         * .. warning::
         *
         *      Although ``range`` support iterators, it's iterators don't
         *      support ``operator ->`` as objects are constructed on demand.
         *      It's iterators also don't return any references to object, they
         *      return the object.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: The type of values.
         *
         *
         * Members types
         * -------------
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`value_type <py-builtins-range-value_type>`
         *          -   The type of values
         *
         *      *   -   :ref:`iterator <py-builtins-range-iterator>`
         *          -   The type of iterators
         *
         *      *   -   :ref:`const_iterator <py-builtins-range-const_iterator>`
         *          -   The type of constant iterators
         *
         *      *   -   :ref:`reverse_iterator <py-builtins-range-reverse_iterator>`
         *          -   The type of reverse iterators
         *
         *      *   -   :ref:`const_reverse_iterator <py-builtins-range-const_reverse_iterator>`
         *          -   The type of constant reverse iterators
         *
         *
         * Members functions
         * -----------------
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`(constructor) <py-builtins-range-range>`
         *          -   Constructs a range
         *
         *      *   -   :ref:`operator = <py-builtins-range-operator-assign>`
         *          -   Assigns a range to the range
         *
         * **Value access**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`operator [] <py-builtins-range-operator-subscript>`
         *          -   Accesses value at specified index
         *
         *      *   -   :ref:`operator [] <py-builtins-range-operator-subscript>`
         *          -   Returns a slice of the range
         *
         *      *   -   :ref:`front <py-builtins-range-front>`
         *          -   Acceses the first value
         *
         *      *   -   :ref:`back <py-builtins-range-back>`
         *          -   Acceses the last value
         *
         * **Capacity**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`size <py-builtins-range-size>`
         *          -   Returns the size of the range
         *
         * **Iterators**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`begin <py-builtins-range-begin>`
         *
         *              :ref:`cbegin <py-builtins-range-cbegin>`
         *          -   Returns an iterator to the beginning
         *
         *      *   -   :ref:`end <py-builtins-range-end>`
         *
         *              :ref:`cend <py-builtins-range-cend>`
         *          -   Returns an iterator to the end
         *
         *      *   -   :ref:`rbegin <py-builtins-range-rbegin>`
         *
         *              :ref:`crbegin <py-builtins-range-crbegin>`
         *          -   Returns a reverse iterator to the beginning
         *
         *      *   -   :ref:`rend <py-builtins-range-rend>`
         *
         *              :ref:`crend <py-builtins-range-crend>`
         *          -   Returns a reverse iterator to the end
         *
         * **Observers**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`start <py-builtins-range-start>`
         *          -   Returns the start value of the range
         *
         *      *   -   :ref:`stop <py-builtins-range-stop>`
         *          -   Returns the stop value of the range
         *
         *      *   -   :ref:`step <py-builtins-range-step>`
         *          -   Returns the step value of the range
         * 
         *
         * **Lookup**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`count <py-builtins-range-count>`
         *          -   Returns the count of a value in the range
         *
         *      *   -   :ref:`index <py-builtins-range-index>`
         *          -   Returns the index of a value in the range
         *
         *      *   -   :ref:`find <py-builtins-range-find>`
         *          -   Finds and returns an iterator to a value in the range
         *
         * **Modifiers**
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`swap <py-builtins-range-swap>`
         *          -   Swaps the range with another
         *
         *
         * Non-member functions
         * --------------------
         *
         * .. list-table::
         *      :class: width-full
         *      :widths: 25 75
         *
         *      *   -   :ref:`operator \< <py-builtins-operator-less_than-range>`
         *
         *              :ref:`operator > <py-builtins-operator-more_than-range>`
         *
         *              :ref:`operator == <py-builtins-operator-equals-range>`
         *
         *              :ref:`operator != <py-builtins-operator-not_equals-range>`
         *
         *              :ref:`operator \<= <py-builtins-operator-less_than_or_equal-range>`
         *
         *              :ref:`operator >= <py-builtins-operator-more_than_or_equal-range>`
         *
         *          -   Compares two ranges lexicographically
         *
         *      *   -   :ref:`operator ->* <py-builtins-operator-pointer_to_member-range>`
         *          -   Specializes the
         *              :ref:`py-builtins-operator-pointer_to_member-in`
         *              algorithm
         *
         *      *   -   :ref:`::std::swap <std-swap-py-builtins-range>`
         *          -   Specializes the ``std::swap`` algorithm
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/list.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> nums(10);
         *          py::print(py::list(nums));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
         *
         *
         * Member hierarchy
         * ----------------
         *
         * [[[writer_eval: rst_member_tree(node)]]]
         *
         * .. toctree::
         *      :hidden:
         *
         *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
         *
         * @endrst
         *
         * @tparam type type of values
         */
        template <class type>
        class range
        {
        private:

            /**
             * @brief Start value of range
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             */
            type start_;

            /**
             * @brief Stop value of range
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             */
            type stop_;

            /**
             * @brief Step value of range
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             */
            long step_;

        public:

            /**
             * @brief Defines type of values as value_type
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::value_type
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-value_type:
             *
             * *py::builtins::range::*\ value_type
             * ==================================
             *
             * .. code-block:: cpp
             *
             *      typedef type value_type;
             *
             * The type of values.
             *
             * @endrst
             *
             */
            typedef type value_type;

            /**
             * @brief Defines the type of iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-iterator:
             *
             * *py::builtins::range::*\ iterator
             * ================================
             *
             * .. code-block:: cpp
             *
             *      typedef /\* ... *\/ iterator;
             *
             * The type of iterator to values.
             *
             * @endrst
             *
             */
            typedef internal__::iterator<
                range,
                value_type,
                value_type*,
                value_type
            > iterator;

            /**
             * @brief Defines the type of constant iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::const_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-const_iterator:
             *
             * *py::builtins::range::*\ const_iterator
             * ======================================
             *
             * .. code-block:: cpp
             *
             *      typedef /\* ... *\/ const_iterator;
             *
             * Type of constant iterator to values.
             *
             * @endrst
             *
             */
            typedef internal__::iterator<
                const range,
                value_type,
                value_type*,
                value_type
            > const_iterator;

            /**
             * @brief Defines the type of reverse iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::reverse_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-reverse_iterator:
             *
             * *py::builtins::range::*\ reverse_iterator
             * =========================================
             *
             * .. code-block:: cpp
             *
             *      typedef std::reverse_iterator<iterator> reverse_iterator;
             *
             * The type of reverse iterator to values.
             *
             * @endrst
             *
             */
            typedef std::reverse_iterator<iterator> reverse_iterator;

            /**
             * @brief Defines the type of constant reverse iterators
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::const_reverse_iterator
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-const_reverse_iterator:
             *
             * *py::builtins::range::*\ const_reverse_iterator
             * ===============================================
             *
             * .. code-block:: cpp
             *
             *      typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
             *
             * The type of constant reverse iterator to values.
             *
             * @endrst
             *
             */
            typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

            /**
             * @brief Constructs a new range from stop
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::range
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-range:
             *
             * *py::builtins::range::*\ range
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr range(const type& stop);                                      // 1
             *
             *      constexpr range(type&& stop);                                           // 2
             *
             *      constexpr range(const type& start, const type& stop, long step = 1);    // 3
             *
             *      constexpr range(type&& start, type&& stop, long step = 1);              // 4
             *
             * 1.   Constructs an range with given stop.
             *
             * 2.   Same as (1).
             *
             * 3.   Constructs a range from given start, stop and step.
             *
             * 4.   Same as (3).
             *
             *
             * Parameters
             * ----------
             *
             * ``start``: The start of range. See :ref:`py-builtins-range-stop`.
             *
             * ``stop``: The stop of range (exclusive). See
             * :ref:`py-builtins-range-stop`.
             *
             * ``step``: The step of range. See :ref:`py-builtins-range-stop`.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/list.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(7);
             *          py::print(py::list<int>(range));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      {0, 1, 2, 3, 4, 5, 6}
             *
             * @endrst
             *
             * @param stop stop value
             */
            constexpr range(const type& stop);

            /**
             * @brief Constructs a new range from stop
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param stop stop value
             */
            constexpr range(type&& stop);

            /**
             * @brief Constructs a new range from start, stop and step
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param start start value
             * @param stop stop value
             * @param step step value
             */
            constexpr range(const type& start, const type& stop, long step = 1);

            /**
             * @brief Constructs a new range from start, stop and step
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param start start value
             * @param stop stop value
             * @param step step value
             */
            constexpr range(type&& start, type&& stop, long step = 1);

            /**
             * @brief Returns the count of values hold by the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::size
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-size:
             *
             * *py::builtins::range::*\ size
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr std::size_t size() const;
             *
             * Returns the count of values hold by the range.
             *
             *
             * Return value
             * ------------
             *
             * The count of values hold by the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block::
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::print(py::range<int>(1, 8).size());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      7
             *
             * @endrst
             *
             * @return count of values in the range
             */
            constexpr std::size_t size() const;

            /**
             * @brief Returns the count of values with given value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::count
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-count:
             *
             * *py::builtins::range::*\ count
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr std::size_t count(value_type value) const;
             *
             * Returns the count of values with given value.
             *
             *
             * Parameters
             * ----------
             *
             * ``value``: The value to look for.
             *
             *
             * Return value
             * ------------
             *
             * The count of values with given value, always either 0 or 1.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::print(py::range(10).count(9));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      1
             *
             * @endrst
             *
             * @param value the value to find
             * @return count of values with given value, either 0 or 1
             */
            constexpr std::size_t count(value_type value) const;

            /**
             * @brief Returns index of given value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::index
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-index:
             *
             * *py::builtins::range::*\ index
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr std::size_t index(const value_type& value) const;
             *
             * Returns the index of given value.
             *
             *
             * Parameter
             * ---------
             *
             * ``value``: The value to look for.
             *
             *
             * Return value
             * ------------
             *
             * The index of given element.
             *
             *
             * Exceptions
             * ----------
             *
             * :ref:`py-builtins-ValueError` if the element can't be found.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums(1, 10);
             *          py::print(nums.index(5));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      4
             *
             * @endrst
             *
             * @param value the element whose index to return
             * @throw py::ValueError if there is no element with given
             * ``element``
             * @return index of the value
             */
            constexpr std::size_t index(const value_type& value) const;

            /**
             * @brief Finds and returns iterator to given value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::find
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-find:
             *
             * *py::builtins::range::*\ find
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_iterator find(const type& value) const;
             *
             * Finds the given value and returns an iterator to it, otherwise
             * returns ``this->end()``.
             *
             *
             * Parameter
             * ---------
             *
             * ``value``: The value to look for.
             *
             *
             * Return value
             * ------------
             *
             * Returns an iterator to the given value if found, otherwise
             * ``this->end()``.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums(1, 10);
             *          py::print(*nums.find(5));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      5
             *
             * @endrst
             *
             * @param value the element to find
             * @return index of the value
             */
            constexpr const_iterator find(const type& value) const;

            /**
             * @brief Start value of the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::start
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-start:
             *
             * *py::builtins::range::*\ start
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const value_type& start() const;
             *
             * Returns the start value of the range.
             *
             *
             * Return value
             * ------------
             *
             * The start value of the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums(10, 20, 2);
             *          py::print(nums.start()));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      10
             *
             * @endrst
             */
            constexpr const value_type& start() const;

            /**
             * @brief Stop value of the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::stop
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-stop:
             *
             * *py::builtins::range::*\ stop
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const value_type& stop() const;
             *
             * Returns the stop value of the range (exclusive).
             *
             *
             * Return value
             * ------------
             *
             * The stop value of the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums(10, 20, 2);
             *          py::print(nums.stop()));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      20
             *
             * @endrst
             */
            constexpr const value_type& stop() const;

            /**
             * @brief Step value of the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::step
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-step:
             *
             * *py::builtins::range::*\ step
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const long& step() const;
             *
             * Returns the step value of the range.
             *
             * The step value defines the difference between two values. This
             * defines how much to add to a value to get the next one.
             *
             * .. note::
             *
             *      The value of this variable is always a non-zero integer.
             *
             *
             * Return value
             * ------------
             *
             * The step value of the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums(10, 20, 2);
             *          py::print(nums.step()));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      2
             *
             * @endrst
             */
            constexpr const long& step() const;

            /**
             * @brief Returns the first value of the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::front
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-front:
             *
             * *py::builtins::range::*\ front
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr value_type front() const;
             *
             * Returns the first value of the range.
             *
             *
             * Return value
             * ------------
             *
             * The first value of the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(2, 5);
             *          py::print(range.front());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      2
             *
             * @endrst
             *
             * @return the first value of the range
             */
            constexpr value_type front() const;

            /**
             * @brief Returns the last value of the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::back
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-back:
             *
             * *py::builtins::range::*\ back
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr value_type back() const;
             *
             * Returns the last value of the range.
             *
             *
             * Return value
             * ------------
             *
             * The last value of the range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(2, 5);
             *          py::print(range.back());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      4
             *
             * @endrst
             *
             * @return the last value of the range
             */
            constexpr value_type back() const;

            /**
             * @brief Swaps the range with another range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::swap
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-swap:
             *
             * *py::builtins::range::*\ swap
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr void swap(range& other);
             *
             * Swaps the range with another range.
             *
             *
             * Parameter
             * ---------
             *
             * ``other``: Another range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> nums1(1, 10);
             *          py::range<int> nums2(5);
             *          nums1.swap(nums2);
             *          py::print(nums1.stop());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      5
             *
             * @endrst
             *
             * @param other another range
             */
            constexpr void swap(range& other);

            /**
             * @brief Returns a constant iterator to the first value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::begin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-begin:
             *
             * *py::builtins::range::*\ begin
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_iterator begin() const;
             *
             * Returns a constant iterator to the first value of the range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-cbegin`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to the first value.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*range.begin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant iterator to the first value
             */
            constexpr const_iterator begin() const;

            /**
             * @brief Returns a constant iterator to the first value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::cbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-cbegin:
             *
             * *py::builtins::range::*\ cbegin
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_iterator cbegin() const;
             *
             * Returns a constant iterator to the first value of the range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-begin`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to the first value.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*range.cbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant iterator to the first value
             */
            constexpr const_iterator cbegin() const;

            /**
             * @brief Returns a constant reverse iterator to the first value of
             * the reversed range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::rbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-rbegin:
             *
             * *py::builtins::range::*\ rbegin
             * ===============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_reverse_iterator rbegin() const;
             *
             * Returns a constant reverse iterator to the first value of the
             * reversed range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-crbegin`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to the first value of the reversed
             * range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*range.rbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant reverse iterator to the first value of the
             * reversed range
             */
            constexpr const_reverse_iterator rbegin() const;

            /**
             * @brief Returns a constant reverse iterator to the first value of
             * the reversed range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::crbegin
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-crbegin:
             *
             * *py::builtins::range::*\ crbegin
             * ================================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_reverse_iterator crbegin() const;
             *
             * Returns a constant reverse iterator to the first value of the
             * reversed range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-rbegin`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to the first value of the reversed
             * range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*range.crbegin());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant reverse iterator to the first value of the
             * reversed range
             */
            constexpr const_reverse_iterator crbegin() const;

            /**
             * @brief Returns a constant iterator to one past the last value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::end
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-end:
             *
             * *py::builtins::range::*\ end
             * ============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_iterator end() const;
             *
             * Returns a constant iterator to one past the last value of the
             * range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-cend`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to one past the last value.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*(range.end() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant iterator to one past the last value
             */
            constexpr const_iterator end() const;

            /**
             * @brief Returns a constant iterator to one past the last value
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::cend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-cend:
             *
             * *py::builtins::range::*\ cend
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_iterator cend() const;
             *
             * Returns a constant iterator to one past the last value of the
             * range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-end`.
             *
             * .. graphviz:: ../assets/range-begin-end.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant iterator to one past the last value.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*(range.cend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      3
             *
             * @endrst
             *
             * @return constant iterator to one past the last value
             */
            constexpr const_iterator cend() const;

            /**
             * @brief Returns a constant reverse iterator to one past the last
             * value of the reversed range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::rend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-rend:
             *
             * *py::builtins::range::*\ rend
             * =============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_reverse_iterator rend() const;
             *
             * Returns a constant reverse iterator to one past the last value of
             * the reversed range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-crend`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to one past the last value of the
             * reversed range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*(range.rend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant reverse iterator to one past the last value of
             * the reversed range
             */
            constexpr const_reverse_iterator rend() const;

            /**
             * @brief Returns a constant reverse iterator to one past the last
             * value of the reversed range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::crend
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-crend:
             *
             * *py::builtins::range::*\ crend
             * ==============================
             *
             * .. code-block:: cpp
             *
             *      constexpr const_reverse_iterator crend() const;
             *
             * Returns a constant reverse iterator to one past the last value of
             * the reversed range.
             *
             * .. warning::
             *
             *      Iterators of ``range`` don't support ``operator ->`` as they
             *      are constructed on demand.
             *
             * .. tip::
             *
             *      Equivalent to :ref:`py-builtins-range-rend`.
             *
             * .. graphviz:: ../assets/range-rbegin-rend.dot
             *
             *
             * Return value
             * ------------
             *
             * A constant reverse iterator to one past the last value of the
             * reversed range.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(4);
             *          py::print(*(range.crend() - 1));
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *
             * @endrst
             *
             * @return constant reverse iterator to one past the last value of
             * the reversed range
             */
            constexpr const_reverse_iterator crend() const;

            /**
             * @brief Returns value at given index
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::operator []
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-operator-subscript:
             *
             * *py::builtins::range::*\ operator []
             * ====================================
             *
             * .. code-block:: cpp
             *
             *      constexpr type operator [] (long index) const;          // 1
             *
             *      range operator [] (std::string properties) const;       // 2
             *
             * 1.   Returns the value at given index.
             *
             * 2.   Returns a slice of the range with given start, stop and
             *      step.
             *
             *
             * Parameters
             * ----------
             *
             * ``index``: The index of the element.
             *
             * ``properties``: The properties of slice.
             *
             * .. admonition:: Format of argument ``properties``
             *
             *      The value of ``properties`` must have the format::
             *
             *          "[start]:[stop][:step]"
             *
             *
             * Return value
             * ------------
             *
             * 1.   The value at given index.
             *
             * 2.   A slice of the range with given start, stop and step.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(3);
             *          py::print(range[0]);
             *          py::print(range["1:2"]); // Slice
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      0
             *      py::builtins::range<...>(1, 2)
             *
             * @endrst
             *
             * @attr
             *
             *      toctree_label operator []
             *      member_tree_label operator []
             *
             * @endattr
             *
             * @param index index of element
             * @return reference to element
             */
            constexpr type operator [] (long index) const;

            /**
             * @brief Returns a slice of the range
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param properties properties of slice
             * @return slice of range
             */
            range operator [] (std::string properties) const;

            /**
             * @brief Assigns another range to the range
             *
             * @details @rst
             *
             * .. index:: py::builtins::range::operator =
             *
             * [[[writer_eval: ref_label]]]
             *
             * .. _py-builtins-range-operator-assign:
             *
             * *py::builtins::range::*\ operator =
             * ===================================
             *
             * .. code-block:: cpp
             *
             *      constexpr range& operator = (const range& other);       // 1
             *
             *      constexpr range& operator = (range&& other);            // 2
             *
             * Assigns another range to the range.
             *
             *
             * Parameters
             * ----------
             *
             * ``other``: Another range.
             *
             *
             * Return value
             * ------------
             *
             * ``*this``.
             *
             *
             * Complexity
             * ----------
             *
             * Constant.
             *
             *
             * Example
             * -------
             *
             * .. code-block:: cpp
             *
             *      #include <pypp/builtins/range.hpp>
             *      #include <pypp/builtins/print.hpp>
             *
             *      int main()
             *      {
             *          py::range<int> range(10);
             *          range = py::range<int>(20);
             *          py::print(range.stop());
             *      }
             *
             * **Output**:
             *
             * .. code-block:: text
             *
             *      20
             *
             * @endrst
             *
             * @attr
             *
             *      toctree_label operator =
             *      member_tree_label operator =
             *
             * @endattr
             *
             * @param other another range
             * @return the range
             */
            constexpr range& operator = (const range& other);

            /**
             * @brief Assigns another range to the range
             *
             * @details @attr
             *
             *      generate false
             *
             * @endattr
             *
             * @param other another range
             * @return the range
             */
            constexpr range& operator = (range&& other);
        };

        /**
         * @brief Checks whether a range is less than another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator < [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-less_than-range:
         *
         * *py::builtins::*\ operator < (:ref:`py::builtins::range <py-builtins-range>`)
         * =============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator < (const range<type>& lhs, const range<type>& rhs);
         *
         * Checks whether a range is less than another range.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is less than ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(9);
         *          py::range<int> rhs(10);
         *
         *          py::print(lhs < rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator < (py::builtins::range)
         *      member_tree_label operator \\< (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is less than ``rhs``
         */
        template <class type>
        bool operator < (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Checks whether a range is more than another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator > [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-more_than-range:
         *
         * *py::builtins::*\ operator > (:ref:`py::builtins::range <py-builtins-range>`)
         * =============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator > (const range<type>& lhs, const range<type>& rhs);
         *
         * Checks whether a range is more than another range.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is more than ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(9);
         *          py::range<int> rhs(9);
         *
         *          py::print(lhs > rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      false
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator > (py::builtins::range)
         *      member_tree_label operator > (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is more than ``rhs``
         */
        template <class type>
        bool operator > (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Checks whether a range is equal to another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator == [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-equals-range:
         *
         * *py::builtins::*\ operator == (:ref:`py::builtins::range <py-builtins-range>`)
         * ==============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator == (const range<type>& lhs, const range<type>& rhs);
         *
         * Compares two ranges for equality.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them, for example
         *      ``range(0, 4, 2) == range(0, 3, 2)`` returns ``true``.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(9);
         *          py::range<int> rhs(9);
         *
         *          py::print(lhs == rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator == (py::builtins::range)
         *      member_tree_label operator == (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is equal to ``rhs``
         */
        template <class type>
        constexpr bool operator == (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Checks whether a range is not equal to another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator != [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-not_equals-range:
         *
         * *py::builtins::*\ operator != (:ref:`py::builtins::range <py-builtins-range>`)
         * ==============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator != (const range<type>& lhs, const range<type>& rhs);
         *
         * Compares two ranges for inequality.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them for example
         *      ``range(0, 4, 2) != range(0, 3, 2)`` returns ``false``.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is not equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(0);
         *          py::range<int> rhs(4, 0);
         *
         *          py::print(lhs != rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      false
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator != (py::builtins::range)
         *      member_tree_label operator != (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is not equal to ``rhs``
         */
        template <class type>
        constexpr bool operator != (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Checks whether a range is less than or equal to another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator <= [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-less_than_or_equal-range:
         *
         * *py::builtins::*\ operator <= (:ref:`py::builtins::range <py-builtins-range>`)
         * ==============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator <= (const range<type>& lhs, const range<type>& rhs);
         *
         * Checks whether a range is less than or equal to another range.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is less than or equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(9);
         *          py::range<int> rhs(10);
         *
         *          py::print(lhs <= rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator <= (py::builtins::range)
         *      member_tree_label operator \\<= (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is less than or equal to ``rhs``
         */
        template <class type>
        constexpr bool operator <= (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Checks whether a range is more than or equal to another range
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator >= [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-more_than_or_equal-range:
         *
         * *py::builtins::*\ operator >= (:ref:`py::builtins::range <py-builtins-range>`)
         * ==============================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      constexpr bool operator >= (const range<type>& lhs, const range<type>& rhs);
         *
         * Checks whether a range is more than or equal to another range.
         *
         * .. note::
         *
         *      Ranges are compared by the values hold by range, not by
         *      specifications of them.
         *
         *
         * Parameters
         * ----------
         *
         * ``lhs``: A range.
         *
         * ``rhs``: Another range.
         *
         *
         * Return value
         * ------------
         *
         * Whether ``lhs`` is more than or equal to ``rhs``.
         *
         *
         * Complexity
         * ----------
         *
         * :math:`O(n)`, where ``n = py::min(lhs.size(), rhs.size())``.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> lhs(9);
         *          py::range<int> rhs(9);
         *
         *          py::print(lhs >= rhs);
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator >= (py::builtins::range)
         *      member_tree_label operator >= (py::builtins::range)
         *
         * @endattr
         *
         * @tparam type type of elements
         * @param lhs first range
         * @param rhs second range
         * @return whether ``lhs`` is more than or equal to ``rhs``
         */
        template <class type>
        constexpr bool operator >= (const range<type>& lhs, const range<type>& rhs);

        /**
         * @brief Returns whether the iterable array contains given ``element``
         *
         * @details @rst
         *
         * .. index:: py::builtins::operator ->* [py::builtins::range]
         *
         * [[[writer_eval: ref_label]]]
         *
         * .. _py-builtins-operator-pointer_to_member-range:
         *
         * *py::builtins::*\ operator ->* (:ref:`py::builtins::range <py-builtins-range>`) (specialization)
         * ================================================================================================
         *
         * *Defined in* ``<pypp/builtins/range.hpp>``
         *
         * .. code-block:: cpp
         *
         *      template <class type>
         *      bool operator ->* (const type& element, const in<range<type>>& range);
         *
         * Returns whether a range contains a value.
         *
         *
         * Template parameters
         * -------------------
         *
         * ``type``: Type of the value to find.
         *
         *
         * Parameters
         * ----------
         *
         * ``value``: The value to find.
         *
         * ``range``: An ``py::builtins::in`` object referencing range.
         *
         *
         * Return value
         * ------------
         *
         * Whether the range contains a value.
         *
         *
         * Complexity
         * ----------
         *
         * Constant.
         *
         *
         * Example
         * -------
         *
         * .. code-block:: cpp
         *
         *      #include <pypp/builtins/range.hpp>
         *      #include <pypp/builtins/in.hpp>
         *      #include <pypp/builtins/print.hpp>
         *
         *      int main()
         *      {
         *          py::range<int> nums(6);
         *          py::print(4 ->* py::in(nums));
         *      }
         *
         * **Output**:
         *
         * .. code-block:: text
         *
         *      true
         *
         * @endrst
         *
         * @attr
         *
         *      toctree_label operator ->* (py::builtins::range) (specialization)
         *      member_tree_label operator ->* (py::builtins::range) (specialization)
         *
         * @endattr
         *
         * @tparam type type of value
         * @param value the value to look for
         * @param range ``in`` object which wrapped the range
         * @return whether the array contains given ``value``
         */
        template <class type>
        bool operator ->* (const type& value, const in<range<type>>& range);
    }
}

namespace std
{

    /**
     * @brief Swaps the range with another range
     *
     * @details @rst
     *
     * .. index:: std::swap [py:builtins:range]
     *
     * [[[writer_eval: ref_label]]]
     *
     * .. _std-swap-py-builtins-range:
     *
     * *std::*\ swap (:ref:`py::builtins::range <py-builtins-range>`) (specialization)
     * ===============================================================================
     *
     * .. code-block:: cpp
     *
     *      template <class type>
     *      constexpr void swap(
     *          py::builtins::range<type>& lhs,
     *          py::builtins::range<type>& rhs
     *      ) noexcept(noexcept(lhs.swap(rhs)));
     *
     * Swaps a range with another range.
     *
     * .. tip::
     *
     *      Equivalent to::
     *
     *          lhs.swap(rhs);
     *
     *
     * Parameter
     * ---------
     *
     * ``lhs``: A range.
     *
     * ``rhs``: Another range.
     *
     *
     * Complexity
     * ----------
     *
     * Constant.
     *
     *
     * Example
     * -------
     *
     * .. code-block:: cpp
     *
     *      #include <pypp/builtins/range.hpp>
     *      #include <pypp/builtins/print.hpp>
     *
     *      int main()
     *      {
     *          py::range<int> nums1(1, 10);
     *          py::range<int> nums2(5);
     *
     *          std::swap(nums1, nums2);
     *          py::print(nums1.stop());
     *      }
     *
     * **Output**:
     *
     * .. code-block:: text
     *
     *      5
     *
     * @endrst
     *
     * @attr
     *
     *      toctree_label ::std::swap (py:builtins:range) (specialization)
     *      member_tree_label swap (py:builtins:range) (specialization)
     *
     * @endattr
     *
     * @param lhs a range
     * @param rhs another range
     */
    template <class type>
    constexpr void swap(
        py::builtins::range<type>& lhs,
        py::builtins::range<type>& rhs
    ) noexcept(noexcept(lhs.swap(rhs)));
}

#include <pypp/builtins/range.tcc>

#endif
