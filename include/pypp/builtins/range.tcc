/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_RANGE_TCC
#define PYPP_BUILTINS_RANGE_TCC

#include <pypp/builtins/range.hpp>

#include <utility>

namespace py
{
    inline namespace builtins
    {
        template <class type>
        constexpr range<type>::range(const type& stop)
            : start_(type()), stop_(stop), step_(1)
        {}

        template <class type>
        constexpr range<type>::range(type&& stop)
            : start_(), stop_(std::move(stop)), step_(1)
        {}

        template <class type>
        constexpr range<type>::range(const type& start, const type& stop, long step)
            : start_(start), stop_(stop), step_(step)
        {
            if (step == 0) raise(ValueError("range step can't be zero"));
        }

        template <class type>
        constexpr range<type>::range(type&& start, type&& stop, long step)
            : start_(std::move(start)), stop_(std::move(stop)), step_(std::move(step))
        {
            if (step == 0) raise(ValueError("range step can't be zero"));
        }

        template <class type>
        constexpr std::size_t range<type>::size() const
        {
            std::size_t size = abs(stop_ - start_) / abs(step_);
            if (type(start_ + (step_ * size)) < stop_)
            {
                size++;
            }

            return size;
        }

        template <class type>
        constexpr std::size_t range<type>::count(type value) const
        {
            if (value < start() || !(value < stop()))
                return 0;

            if ((value - start()) % step() == 0)
                return 1;

            return 0;
        }

        template <class type>
        constexpr std::size_t range<type>::index(const type& value) const
        {
            if (count(value))
            {
                return std::size_t(value - start()) / step();
            }

            raise(ValueError("value not in range"));
        }

        template <class type>
        constexpr typename range<type>::const_iterator range<type>::find(const type& value) const
        {
            if (count(value))
            {
                return begin() + index(value);
            }
            else
            {
                return end();
            }
        }

        template <class type>
        constexpr const type& range<type>::start() const
        {
            return start_;
        }

        template <class type>
        constexpr const type& range<type>::stop() const
        {
            return stop_;
        }

        template <class type>
        constexpr const long& range<type>::step() const
        {
            return step_;
        }

        template <class type>
        constexpr type range<type>::front() const
        {
            return (*this)[0];
        }

        template <class type>
        constexpr type range<type>::back() const
        {
            return (*this)[-1];
        }

        template <class type>
        constexpr void range<type>::swap(range<type>& other)
        {
            value_type start = std::move(start_);
            value_type stop = std::move(stop_);
            long step = std::move(step_);

            start_ = std::move(other.start_);
            stop_ = std::move(other.stop_);
            step_ = std::move(other.step_);

            other.start_ = std::move(start);
            other.stop_ = std::move(stop);
            other.step_ = std::move(step);
        }

        template <class type>
        constexpr typename range<type>::const_iterator range<type>::begin() const
        {
            return cbegin();
        }

        template <class type>
        constexpr typename range<type>::const_iterator range<type>::cbegin() const
        {
            return const_iterator(this, 0);
        }

        template <class type>
        constexpr typename range<type>::const_reverse_iterator range<type>::rbegin() const
        {
            return crbegin();
        }

        template <class type>
        constexpr typename range<type>::const_reverse_iterator range<type>::crbegin() const
        {
            return const_reverse_iterator(end());
        }

        template <class type>
        constexpr typename range<type>::const_iterator range<type>::end() const
        {
            return cend();
        }

        template <class type>
        constexpr typename range<type>::const_iterator range<type>::cend() const
        {
            return const_iterator(this, size());
        }

        template <class type>
        constexpr typename range<type>::const_reverse_iterator range<type>::rend() const
        {
            return crend();
        }

        template <class type>
        constexpr typename range<type>::const_reverse_iterator range<type>::crend() const
        {
            return const_reverse_iterator(begin());
        }

        template <class type>
        constexpr type range<type>::operator [] (long index) const
        {
            if (index < 0)
                index += size();

            if (index < 0 || static_cast<std::size_t>(index) >= size())
                raise(IndexError("range object index out of range"));

            return start_ + (step_ * index);
        }

        template <class type>
        range<type> range<type>::operator [] (std::string properties) const
        {
            auto data = internal__::slice::from_str(properties);

            data.step *= step_;
            if (data.stop == std::numeric_limits<long>::min())
                data.stop = start_ - step_;

            return range((*this)[data.start], (*this)[data.stop - data.step] - step_, data.step);
        }

        template <class type>
        constexpr range<type>& range<type>::operator = (const range<type>& other)
        {
            start_ = other.start_;
            stop_ = other.stop_;
            step_ = other.step_;

            return *this;
        }

        template <class type>
        constexpr range<type>& range<type>::operator = (range<type>&& other)
        {
            start_ = std::move_if_noexcept(other.start_);
            stop_ = std::move_if_noexcept(other.stop_);
            step_ = other.step_;

            return *this;
        }

        template <class type>
        bool operator < (const range<type>& lhs, const range<type>& rhs)
        {
            for (std::size_t i = 0; i < lhs.size() && i < rhs.size(); i++)
            {
                if (lhs[i] < rhs[i])
                {
                    return true;
                }
                else if (lhs[i] > rhs[i])
                {
                    return false;
                }
            }

            return lhs.size() < rhs.size();
        }

        template <class type>
        bool operator > (const range<type>& lhs, const range<type>& rhs)
        {
            for (std::size_t i = 0; i < lhs.size() && i < rhs.size(); i++)
            {
                if (lhs[i] > rhs[i])
                {
                    return true;
                }
                else if (lhs[i] < rhs[i])
                {
                    return false;
                }
            }

            return lhs.size() > rhs.size();
        }

        template <class type>
        constexpr bool operator == (const range<type>& lhs, const range<type>& rhs)
        {
            return !(lhs < rhs || lhs > rhs);
        }

        template <class type>
        constexpr bool operator != (const range<type>& lhs, const range<type>& rhs)
        {
            return !(lhs == rhs);
        }

        template <class type>
        constexpr bool operator <= (const range<type>& lhs, const range<type>& rhs)
        {
            return !(lhs > rhs);
        }

        template <class type>
        constexpr bool operator >= (const range<type>& lhs, const range<type>& rhs)
        {
            return !(lhs < rhs);
        }

        template <class type>
        bool operator ->* (const type& value, const in<range<type>>& range)
        {
            return range.container().count(value);
        }
    }
}

namespace std
{
    template <class type>
    constexpr void swap(
        py::builtins::range<type>& lhs,
        py::builtins::range<type>& rhs
    ) noexcept(noexcept(lhs.swap(rhs)))
    {
        lhs.swap(rhs);
    }
}

#endif
