/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PYPP_BUILTINS_HPP
#define PYPP_BUILTINS_HPP

#include <pypp/builtins/abs.hpp>
#include <pypp/builtins/all.hpp>
#include <pypp/builtins/any.hpp>
#include <pypp/builtins/assert.hpp>
#include <pypp/builtins/bool.hpp>
#include <pypp/builtins/exceptions.hpp>
#include <pypp/builtins/in.hpp>
#include <pypp/builtins/list.hpp>
#include <pypp/builtins/len.hpp>
#include <pypp/builtins/max.hpp>
#include <pypp/builtins/min.hpp>
#include <pypp/builtins/raise.hpp>
#include <pypp/builtins/range.hpp>
#include <pypp/builtins/str.hpp>

/**
 * @brief Namespace containing everything provided by PyPP
 *
 * @details @rst
 *
 * Namespace ``py`` contains everything provided by PyPP. It contains very basic
 * functions like :ref:`py::all <py-builtins-all>` to very complex classes and
 * data structures.
 *
 * @endrst
 *
 * @attr
 *
 *      generate false
 *
 * @endattr
 *
 */
namespace py
{

    /**
     * @brief Namespace containing core functions and classes
     *
     * @details @rst
     *
     * .. index:: py::builtins
     *
     * [[[writer_eval: ref_label]]]
     *
     * .. _py-builtins:
     *
     * *py::*\ builtins
     * ================
     *
     * .. code-block:: cpp
     *
     *      inline namespace builtins {}
     *
     * This namespace contains core functions, classes and data structures
     * (available in Python module builtins) defined by PyPP.
     *
     *
     * Members
     * -------
     *
     * [[[writer_eval: rst_member_tree(node)]]]
     *
     * .. toctree::
     *      :hidden:
     *
     *      [[[writer_eval: textwrap.indent(toctree, " ")]]]
     *
     * @endrst
     *
     */
    inline namespace builtins {}
}

#ifdef DOXYGEN_DOCUMENTATION_BUILD
/**
 * @brief Dummy namespace to make Doxygen think this is documented
 *
 * @details @attr
 *
 *      generate false
 *
 * @endattr
 */
namespace std {}
#endif

#endif
