/*
    Copyright (C) 2020-2021 Akib Azmain.

    This file is part of PyPP.

    PyPP is free software: you can redistribute it and/or modify it under the
    terms of the GNU Lesser General Public License as published by the Free
    Software Foundation, either version 3 of the License, or (at your option)
    any later version.

    PyPP is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
    more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PyPP.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @mainpage
 *
 * @details @rst
 *
 * [[[writer_eval: ref_label]]]
 *
 * .. _library-references:
 *
 * Library References
 * ==================
 *
 * This reference manual describes the API provided by PyPP. This attempts to be
 * exact and complete, so it may be hard to read. If you are new to PyPP, it is
 * recommended that you read the :ref:`tutorials <tutorial>`, it will give you a
 * solid conception of what PyPP is and also introduce you with many useful
 * constructs provided by PyPP.
 *
 *
 * .. index:: py
 *
 * .. _py:
 *
 * py
 * ---
 *
 * .. code-block:: cpp
 *
 *      namespace py {}
 *
 * Everything provided by PyPP is under the namespace PyPP. Namespace PyPP
 * contains several other namespaces, each one containing a modules. Functions
 * like :ref:`py-builtins-all` and :ref:`py-builtins-print` are defined in
 * namespace :ref:`py-builtins`, but it is unnecessary to specify ``builtins``
 * when refering them (no need to write ``py::builtins::print``, ``py::print``
 * is enough), as :ref:`py-builtins` is an inline namespace.
 *
 *
 * Symbol hierarchy
 * ++++++++++++++++
 *
 * [[[writer_eval: rst_member_tree(node.child("namespace""py"), force=True)]]]
 *
 * .. toctree::
 *      :hidden:
 *
 *      [[[writer_eval: textwrap.indent(generate_toctree(node.child("namespace""py")), " ")]]]
 *
 *
 * std
 * ---
 *
 * This namespace contains specializations and overloads for several constructs
 * provided by the C++ standard library for PyPP.
 *
 * [[[writer_eval: rst_member_tree(node.child("namespace""std"), force=True)]]]
 *
 * .. toctree::
 *      :hidden:
 *
 *      [[[writer_eval: textwrap.indent(generate_toctree(node.child("namespace""std")), " ")]]]
 *
 * @endrst
 *
 */

#ifndef PYPP_PYPP_HPP
#define PYPP_PYPP_HPP

#include <pypp/builtins.hpp>

#endif
