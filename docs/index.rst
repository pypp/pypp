PyPP: Pythonic Programming in C++
=================================

.. only:: latex

    Introduction
    ------------

PyPP is a C++ library that allows you to write **pythonic** code in C++. Think
of it as a **re-implementation** of the Python standard library in C++.

Cross-platform
    PyPP is a cross-platform library, everything in PyPP works on every platform
    unless otherwise specified. So no matter whatever platform you are
    targeting, PyPP will be out of your way.

Easy to use
    PyPP is an easy to use, powerful library. It provides an extensive set of
    functionalities, allowing you to increase your productivity. See
    :ref:`tutorials <tutorial>` for getting started.

Open source
    PyPP is an open source project, so you can modify it as you like. As it is
    open source, you can also fix bugs and :ref:`contribute <contributing>` them
    to PyPP. You can also propose a new feature, new proposals are welcome.

You can find out more about :ref:`all features of PyPP <features>` by exploring
the documentation and reading the :ref:`tutorials <tutorial>`.


.. toctree::
    :hidden:
    :caption: Quick Start

    setup/installation
    tutorials/index

.. toctree::
    :hidden:
    :caption: References

    api/index

.. toctree::
    :hidden:
    :caption: About

    features

.. toctree::
    :hidden:
    :caption: Contributing

    contributing/guide
    Code of Conduct <contributing/code-of-conduct>

.. toctree::
    :hidden:
    :caption: License

    license


.. only:: not latex

    .. toctree::
        :hidden:
        :caption: Appendix

        genindex
