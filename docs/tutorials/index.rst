.. _tutorial:

PyPP Tutorial
=============

PyPP is an easy to learn and very useful C++ library. It attempts to port the
Python standard library (a very useful library) to C++, enabling you to write
more efficient code in C++ with less time and effort. PyPP has many useful
function, classes, and both high-level and low-level data structures, making it
ideal for rapid application development and problem-solving.

This tutorial introduces the reader to PyPP, its basic concepts, and its
features. It is recommended to install PyPP before continuing (see
:ref:`installation` for instructions on installation) so the reader can compile
see results himself (and possibly play with them), but not required.

This tutorial doesn't attempt to cover everything in the library, even not the
most used ones. For a complete reference of every class and function (and
everything else) see the :ref:`library-references`. After reading this tutorial,
you'll be able to write and compile software that uses PyPP, and explore other
parts of the library yourself.
