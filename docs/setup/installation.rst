.. _installation:

Installing PyPP
===============

PyPP is an cross-platform library. It is intended to work with a variety of
platforms. The installation process of PyPP is more or less easy depending on
the platform, as the process is different in every platform. The installation
process is specified below for every platforms supported currently:


Linux
-----

Installation on Linux is pretty straight-forward. Download a release from the
releases and extract them in a dedicated folder. Then execute the following
commands:

.. code-block:: bash

    mkdir build
    cd build

    ../configure

    make
    make install

.. warning::

    Executing ``../configure`` without ``--prefix`` argument set will cause
    ``make install`` to install PyPP to system directories, which may conflict
    with your package manager.

.. note::

    Depending on where you are installing, you may need to use ``sudo make
    install`` instead of ``make install``.


MacOS and other Unix-likes
--------------------------

The installation process should be same as Linux_, some additional configuration
may be needed.


Windows
-------

.. // TODO: Added instructions for windows
