# PyPP license

```{eval-rst}
.. include ../README.rst
    :start-after: license-note-start
```

## License text

A copy of the GNU Lesser General Public License version 3 and GNU General Public
License version 3 is provided below (source
<https://www.gnu.org/licenses/lgpl-3.0.html> and
<https://www.gnu.org/licenses/gpl-3.0.html>):

```{include} ../LICENSE.md
```

```{include} ../LICENSE.GPL.md
```
