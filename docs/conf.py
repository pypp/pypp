# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath("."))


# -- Required modules --------------------------------------------------------

import os
import sys
sys.path.insert(0, os.path.abspath("."))
import textwrap
import datetime
import writer


# -- Project information -----------------------------------------------------

project = "PyPP"
author = "Akib Azmain"
copyright = f"2020-{datetime.datetime.now().year}, {author}"

# The full version, including alpha/beta/rc tags
release = "0.0.1-alpha"


# -- General configuration ---------------------------------------------------

source_suffix = [
    ".rst",
    ".md"
]

primary_domain = "cpp"

highlight_language = "cpp"

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named "sphinx.ext.*") or your custom
# ones.
extensions = [
    "sphinx.ext.graphviz",
    "sphinx.ext.imgconverter",
    "sphinx.ext.mathjax",
    "myst_parser",
    "notfound.extension",
    "docscov",
    "doxycast"
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["build", "doxygen", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_book_theme"

html_theme_options = {
    "repository_url": "https://gitlab.com/pypp/pypp",
    "path_to_docs": "docs/",
    "use_repository_button": True,
    "use_issues_button": True,
}

html_title = project

html_baseurl = "https://pypp.readthedocs.io/en/latest/"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["static"]

html_css_files = [
    "css/full-width-table.css"
]


# -- Graphviz configurations -------------------------------------------------

graphviz_output_format = "svg"


# -- MathJax configurations --------------------------------------------------

mathjax_path = \
    "https://cdn.jsdelivr.net/npm/mathjax@2/MathJax.js?config=TeX-AMS-MML_HTMLorMML"


# -- MyST Parser configurations ----------------------------------------------

myst_url_schemes = [
    "http",
    "https",
    "mailto"
]


# -- DoxyCast configuration --------------------------------------------------

doxycast_config = {
    "projects": {
        "pypp": {
            "xml_dir": "doxygen",
            "output_dir": "api",
            "execute_doxygen": True,
            "doxygen_config": textwrap.dedent("""
                INPUT                        =      ../include
                STRIP_FROM_PATH              =      ..

                EXCLUDE_PATTERNS             =      /include/pypp/*/*.tcc
            """),
            "writer": writer
        }
    }
}


# -- Docscov configurations --------------------------------------------------

docscov_config = {
    "xml_dir": os.path.join(doxycast_config["projects"]["pypp"]["xml_dir"], "xml"),
    "root_dir": "..",
    "scope": [
        "public",
        "protected",
        # "private"
    ],
    "kind": [
        "class",
        "define",
        "enum",
        "enumvalue",
        # "file",
        "friend",
        "function",
        "namespace",
        "page",
        "signal",
        "slot",
        "struct",
        "typedef",
        "union",
        "variable"
    ]
}
