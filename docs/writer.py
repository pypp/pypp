import doxycast.node
import doxycast.utils
from doxycast.utils import ansi_colors

import os
import re
import textwrap

class return_trigger(BaseException): pass

def Return(*args):
    raise return_trigger(*args)

def replace(local_vars, contents):
    class replacer:
        def __call__(self, match):
            if match.group(1) == "eval":
                return eval(match.group(2), globals(), self.local_vars)
            elif match.group(1) == "exec":
                try:
                    return exec(textwrap.dedent(match.group(2)), globals(), self.local_vars)
                except return_trigger as ret:
                    return str(ret)
                except BaseException as error:
                    raise RuntimeError(
                        "\nexception occurred in embedded python, exception is: " +
                        type(error).__name__ + ", exception message: " + str(error) +
                        ", script:\n" + textwrap.dedent(match.group(0))
                    )
            else:
                raise RuntimeError(f"invalid expression: \"{match.group(0)}\"")

        def __init__(self, local_vars):
            self.local_vars = local_vars

    return (
        re.sub(r"\[\[\[writer_([a-z]+):\s((.|\n)*?)\]\]\]", replacer(local_vars), contents)
        .replace("/\\*", "/*")
        .replace("*\\/", "*/")
        .replace("/\\\\*", "/\\*")
        .replace("*\\\\/", "*\\/")
    )

def output_writing(project: str, file: str) -> None:
    print(
        "\x1b[K\r", # Clean the line
        doxycast.utils.color(ansi_colors.bold) + "doxycast: writing rst: ",
        doxycast.utils.color(ansi_colors.magenta) + project +
        doxycast.utils.color(ansi_colors.bold) + ": " + doxycast.utils.color(ansi_colors.magenta),
        file + doxycast.utils.color(ansi_colors.default),
        sep="",
        end="\r"
    )

def dispatch(project: str, output_dir: str, node: doxycast.node.node, config: dict, xml_dir: str):
    if node.type == "namespace":
        write_namespace(project, output_dir, node, config, xml_dir)
    elif node.type == "class":
        write_class(project, output_dir, node, config, xml_dir)
    elif node.type == "struct":
        write_struct(project, output_dir, node, config, xml_dir)
    elif node.type == "union":
        write_union(project, output_dir, node, config, xml_dir)
    elif node.type == "enum":
        write_enum(project, output_dir, node, config, xml_dir)
    elif node.type == "function":
        write_function(project, output_dir, node, config, xml_dir)
    elif node.type == "variable":
        write_variable(project, output_dir, node, config, xml_dir)
    elif node.type == "typedef":
        write_typedef(project, output_dir, node, config, xml_dir)
    elif node.type == "macro":
        write_macro(project, output_dir, node, config, xml_dir)
    else:
        write_node(project, output_dir, node, config, xml_dir)

def write_node(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    pass

def write_namespace(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    if node.attributes.get("generate", "true") == "true":
        output_writing(project, os.path.join(output_dir, f"{node.doxygen_id}.rst"))

        contents: str = node.details

        toctree = str()

        toctree = generate_toctree(node)

        ref_label = f".. _doxycast_{project}_{node.doxygen_id}:"
        open(os.path.join(output_dir, f"{node.doxygen_id}.rst"), "w").write(replace(locals(), contents))

    for child in node.children:

        # Make sure symbols from inline namespace don't get rewritten
        continue_ = False
        for node_child in node.children:
            if node_child.type == "namespace":
                try:
                    node_child.child(child.doxygen_id)
                    continue_ = True
                except IndexError:
                    pass
        if continue_: continue

        dispatch(project, output_dir, child, config, xml_dir)

def write_class(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    if node.attributes.get("generate", "true") == "true":
        output_writing(project, os.path.join(output_dir, f"{node.doxygen_id}.rst"))

        contents: str = node.details

        toctree = str()

        toctree = generate_toctree(node)

        inheritance_graph = str()

        try:
            for graph_node_id in node.inheritance_graph:
                inheritance_graph += (
                    f"node{graph_node_id} ["
                    f"""label=\"{node.inheritance_graph[graph_node_id].label}\"{
                        f' href="../{output_dir}/{node.inheritance_graph[graph_node_id].refid}.html#'
                        f'{node.inheritance_graph[graph_node_id].refid}", target="_top"'
                            if node.inheritance_graph[graph_node_id].refid else str()
                    }]\n"""
                )

            inheritance_graph += "\n"

            for graph_node_id in node.inheritance_graph:
                for child_graph_node_id in [
                    child[0] for child in node.inheritance_graph[graph_node_id].children
                ]:
                    inheritance_graph += f"node{graph_node_id} -> node{child_graph_node_id}\n"

            inheritance_graph = textwrap.indent(textwrap.dedent("""
                digraph "inheritance-graph" {{
                    graph[rankdir=RL, ranksep=0.2]
                    node[shape=Mrecord, fontname="helvetica", height=0.1]
                    edge[arrowsize=0.6, arrowhead=vee]

                    {}
                }}
            """).format(
                textwrap.indent(inheritance_graph, " " * 4)[4:]
            ), " " * 4)

        except AttributeError:
            pass

        ref_label = f".. _doxycast_{project}_{node.doxygen_id}:"
        open(os.path.join(output_dir, f"{node.doxygen_id}.rst"), "w").write(replace(locals(), contents))

    for child in node.children:
        dispatch(project, output_dir, child, config, xml_dir)

def write_struct(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    write_class(project, output_dir, node, config, xml_dir)

def write_union(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    if node.attributes.get("generate", "true") == "true":
        output_writing(project, os.path.join(output_dir, f"{node.doxygen_id}.rst"))

        contents: str = node.details

        toctree = generate_toctree(node)

        ref_label = f".. _doxycast_{project}_{node.doxygen_id}:"
        open(os.path.join(output_dir, f"{node.doxygen_id}.rst"), "w").write(replace(locals(), contents))

    for child in node.children:
        dispatch(project, output_dir, child, config, xml_dir)

def write_function(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    write_variable(project, output_dir, node, config, xml_dir)

def write_variable(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    if node.attributes.get("generate", "true") == "true":
        output_writing(project, os.path.join(output_dir, f"{node.doxygen_id}.rst"))

        contents: str = node.details

        ref_label = f".. _doxycast_{project}_{node.doxygen_id}:"
        open(os.path.join(output_dir, f"{node.doxygen_id}.rst"), "w").write(replace(locals(), contents))

def write_enum(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    write_variable(project, output_dir, node, config, xml_dir)

def write_typedef(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    write_variable(project, output_dir, node, config, xml_dir)

def write_macro(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    write_variable(project, output_dir, node, config, xml_dir)

def write_tree(
    project: str,
    output_dir: str,
    node: doxycast.node.node,
    config: dict,
    xml_dir: str
) -> None:
    output_writing(project, os.path.join(output_dir, "index.rst"))

    index_contents: str = node.child("indexpage").details

    toctree = generate_toctree(node.child("global-namespace"))

    ref_label = f".. _doxycast_{project}_index:"
    open(os.path.join(output_dir, f"index.rst"), "w").write(replace(locals(), index_contents))

    for child in node.child("global-namespace").children:
        dispatch(project, output_dir, child, config, xml_dir)

    print()

def generate_toctree(node) -> str:
    result = "\n"

    for child in node.children:
        if child.attributes.get("generate", "true") == "true":

            # Make sure symbols from inline namespace don't get into the toctree
            continue_ = False
            for node_child in node.children:
                if node_child.type == "namespace":
                    try:
                        node_child.child(child.doxygen_id)
                        continue_ = True
                    except IndexError:
                        pass
            if continue_: continue

            label = child.attributes.get("toctree_label", child.name.rsplit('::')[-1])
            result += f"{label} <{child.doxygen_id}>\n"

    return textwrap.indent(result, " " * 4)

def setup(app) -> None:
    pass


def rst_member_tree(node, *, with_given=False, force=False, recursive_force=False) -> str:
    output = str()

    if force or (
        node.attributes.get("generate", "true") == "true" and
        node.attributes.get("tree_link", "true") == "true"
    ):
        for child in node.children:
            if child.attributes.get("generate", "true") == "true":
                label = child.attributes.get("member_tree_label", child.name.rsplit('::')[-1])
                output += f"*   :ref:`{label} <doxycast_pypp_{child.doxygen_id}>`\n\n"

            output += textwrap.indent(rst_member_tree(
                child,
                force=recursive_force,
                recursive_force=recursive_force
            ), " " * 4)

        if with_given:
            output = textwrap.dedent(f"""
                *   :ref:`{node.name.rsplit('::')[-1]} <doxycast_pypp_{node.doxygen_id}>`

                {{}}
            """).format(textwrap.indent(output, " " * 4))

    return output

